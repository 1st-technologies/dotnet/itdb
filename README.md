# ITDB - A Custom IT-centric Work Flow and Project Management Web-Based Application

![Projects View](sample.png)

ITDB is an application designed to track project and operational activities with minimal overhead. Activities in ITDB are grouped into projects, tasks and issues.

## Features

* Linked fields
* AJAX
* Data Validation
* Data Masking
* In-Line Editing
* Minimal Design

## Automation

ITDB works to minimize data-entry by performing several background calculations to dynamically calculate project [BRAG](https://en.wikipedia.org/wiki/Traffic_light_rating_system#Performance_monitoring) status from the status of all linked tasks and issues. Likewise, **completed date**, **status** and **percent** are programatically linked meaning that a *user need enter only one value, not all three*, thus reducing time spent on data-entry. 

## Designed for Speed

ITDB employs [AJAX](https://en.wikipedia.org/wiki/Ajax_\(programming\)) technologies and in-line editing to minimize page reloads and a minimal design along with cache tuning to make page reloads, when necessary, as fast as possible. The final result is an application that gets out of your way so you can get out of your way so you can get back to work.

## Open Access

While the ITDB platform can support full role based access, at present, ITDB is designed as an open-access system. This means that all users can view all projects, tasks and issues.
