﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Data

Partial Class Tasks
    Inherits System.Web.UI.Page
    Dim intProjectID As Int32

    Protected Sub imbhome_Click(sender As Object, e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub imbback_Click(sender As Object, e As ImageClickEventArgs) Handles imbback.Click
        Response.Redirect("ProjectDetails.aspx?ProjectID=" & intProjectID)
    End Sub

    Protected Sub imbMyView_Click(sender As Object, e As ImageClickEventArgs) Handles imbMyView.Click
        If Len(Request.ServerVariables("LOGON_USER")) > 0 Then
            Dim intUserID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & Request.ServerVariables("LOGON_USER") & "'")
            Response.Redirect("/MyView.aspx?PersonID=" & intUserID, False)
        End If
    End Sub

    Protected Sub btnSaveTaskLog_Click(sender As Object, e As EventArgs) Handles btnSaveTaskLog.Click
        If Not Page.IsValid Then Exit Sub

        Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
            "INSERT INTO Task_History (Log_Entry, Date, Task_ID) VALUES ('" & ceNewTaskLogEntry.Text.Replace("'", "''").Trim() & "', '" & Now() & "', '" & Request.QueryString("TaskID") & "'); SELECT SCOPE_IDENTITY();")
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("TASKHISTORYOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            "" _
        )

        upTaskLog.Update()
        TaskLog_Grid.DataBind()
        ceNewTaskLogEntry.Text = ""
    End Sub

    Protected Sub btnSaveNewTaskAttachment_Click(sender As Object, e As EventArgs) Handles btnSaveNewTaskAttachment.Click
        ' This section was built using the method found here: http://www.dotnettutorials.com/tutorials/database/upload-files-sql-database-cs/
        ' DEVELOPER'S NOTE: web.config may need edit is file uploads are timing out in prod, see http://docs.telerik.com/devtools/aspnet-ajax/controls/upload/uploading-files/uploading-large-files
        If Me.Page.IsPostBack Then
            If Not Page.IsValid Then Exit Sub

            If fuTaskAttachment.HasFile Then
                If fuTaskAttachment.PostedFile.ContentLength() <= Convert.ToInt64(ConfigurationManager.AppSettings("MAXFILESIZE")) Then
                    Using Conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString)
                        Try
                            Const SQL As String = "INSERT INTO [Task_Attachments] (Task_ID, File_Name, Description, MIMEType, FileSize, FileData) VALUES (@Task_ID, @File_Name, @Description, @MIMEType, @FileSize, @FileData)"
                            Dim cmd As New SqlCommand(SQL, Conn)

                            cmd.Parameters.AddWithValue("@Task_ID", Request.QueryString("TaskID"))
                            cmd.Parameters.AddWithValue("@File_Name", fuTaskAttachment.FileName.ToString())
                            cmd.Parameters.AddWithValue("@MIMEType", fuTaskAttachment.PostedFile.ContentType.ToString())
                            cmd.Parameters.AddWithValue("@Description", txtTaskattachmentDescription.Text)
                            cmd.Parameters.AddWithValue("@FileSize", fuTaskAttachment.PostedFile.ContentLength())

                            Dim bytes As Byte() = New Byte(fuTaskAttachment.PostedFile.InputStream.Length) {}
                            fuTaskAttachment.PostedFile.InputStream.Read(bytes, 0, bytes.Length)
                            cmd.Parameters.AddWithValue("@FileData", bytes)

                            Conn.Open()
                            cmd.ExecuteNonQuery()
                            Conn.Close()
                        Catch sqlEx As SqlException
                            ' bad upload
                            Conn.Close()
                        Finally
                            upAttachments.Update()
                            Task_Attachments_Grid.DataBind()
                        End Try
                    End Using
                Else
                    ' file too big
                    common.fLogActivity( _
                        0, _
                        0, _
                        Convert.ToInt32(ConfigurationManager.AppSettings("ERRORACTIONID")), _
                        "File size exceeded maximum allowed (" & ConfigurationManager.AppSettings("MAXSIZESTRING") & ")" _
                    )
                End If
                fuTaskAttachment = New FileUpload
                txtTaskattachmentDescription.Text = ""
            End If
        End If
    End Sub

    Protected Sub Task_Attachments_Grid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles Task_Attachments_Grid.RowCommand
        If e.CommandName = "downloadtaskattachment" Then
            Dim objSQLConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString)
            Dim objSQLCommand As New SqlCommand("SELECT FileSize, File_Name, FileData, MIMEType FROM [Task_Attachments] WHERE ID = " & e.CommandArgument)
            objSQLCommand.Connection = objSQLConnection
            objSQLConnection.Open()
            Dim objSQLDataReader As SqlDataReader = objSQLCommand.ExecuteReader()
            objSQLDataReader.Read()
            Dim FileData As Byte() = DirectCast(objSQLDataReader("FileData"), [Byte]())
            Dim MIMEType As String = objSQLDataReader("MIMEType").ToString()
            Dim File_Name As String = objSQLDataReader("File_Name").ToString()
            Dim FileSize As String = objSQLDataReader("FileSize").ToString()
            objSQLConnection.Close()
            Response.Clear()
            Response.Buffer = False
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = MIMEType
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + File_Name)
            Response.AppendHeader("Content-Length", FileSize.ToString())
            Response.OutputStream.Write(FileData, 0, FileData.Length)
            Response.Flush()
            Response.End()
        End If
    End Sub

    Protected Sub Task_Attachments_Grid_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles Task_Attachments_Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ib As ImageButton = DirectCast(e.Row.Cells(2).Controls(0), ImageButton)
            ib.OnClientClick = "if (!confirm('Are you sure you want to delete this file?')) return;"
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        intProjectID = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT Project_ID FROM Tasks WHERE ID = " & Request.QueryString("TaskID"))
    End Sub

    Protected Sub txtPercentComplete_TextChanged(sender As Object, e As EventArgs)
        Dim ddlStatus As DropDownList = DirectCast(DetailsView1.FindControl("ddlStatus"), DropDownList)
        Dim txtPercent As TextBox = DirectCast(DetailsView1.FindControl("txtPercentComplete"), TextBox)
        Dim txtDateComplete As TextBox = DirectCast(DetailsView1.FindControl("txtCompletedDate"), TextBox)
        Dim intCompleteIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSCOMPLETEINDEX"))
        Dim intInProgressIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSINPROGRESSINDEX"))
        Dim intNotStartedIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSNOTSTARTEDINDEX"))
        Dim intOnHoldIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSONHOLDINDEX"))
        If txtPercent.Text = 100 Then
            ddlStatus.SelectedValue = intCompleteIndex
            txtDateComplete.Text = Now().ToString()
        ElseIf txtPercent.Text < 100 And txtPercent.Text > 0 And (ddlStatus.SelectedValue = intCompleteIndex Or ddlStatus.SelectedValue = intNotStartedIndex) Then
            ddlStatus.SelectedValue = intInProgressIndex
            txtDateComplete.Text = ""
        ElseIf txtPercent.Text = 0 Then
            ddlStatus.SelectedValue = intNotStartedIndex
            txtDateComplete.Text = ""
        End If
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim ddlStatus As DropDownList = DirectCast(DetailsView1.FindControl("ddlStatus"), DropDownList)
        Dim txtPercent As TextBox = DirectCast(DetailsView1.FindControl("txtPercentComplete"), TextBox)
        Dim txtDateComplete As TextBox = DirectCast(DetailsView1.FindControl("txtCompletedDate"), TextBox)
        Dim intCompleteIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSCOMPLETEINDEX"))
        Dim intInProgressIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSINPROGRESSINDEX"))
        Dim intNotStartedIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSNOTSTARTEDINDEX"))
        Dim intOnHoldIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSONHOLDINDEX"))
        If ddlStatus.SelectedValue = intCompleteIndex Then
            txtPercent.Text = 100
            txtDateComplete.Text = Now().ToString()
        ElseIf ddlStatus.SelectedValue = intOnHoldIndex And txtPercent.Text = 100 Then
            txtPercent.Text = 99
            txtDateComplete.Text = ""
        ElseIf ddlStatus.SelectedValue = intInProgressIndex And (txtPercent.Text = 0 Or txtPercent.Text = 100) Then
            txtPercent.Text = 50
            txtDateComplete.Text = ""
        ElseIf ddlStatus.SelectedValue = intNotStartedIndex Then
            txtPercent.Text = 0
            txtDateComplete.Text = ""
        End If
    End Sub

    Protected Sub txtCompletedDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddlStatus As DropDownList = DirectCast(DetailsView1.FindControl("ddlStatus"), DropDownList)
        Dim txtPercent As TextBox = DirectCast(DetailsView1.FindControl("txtPercentComplete"), TextBox)
        Dim txtDateComplete As TextBox = DirectCast(DetailsView1.FindControl("txtCompletedDate"), TextBox)
        Dim intCompleteIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSCOMPLETEINDEX"))
        Dim intInProgressIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSINPROGRESSINDEX"))
        Dim intNotStartedIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSNOTSTARTEDINDEX"))
        Dim intOnHoldIndex As Int32 = Convert.ToInt32(ConfigurationManager.AppSettings("STATUSONHOLDINDEX"))
        If Len(txtDateComplete.Text) > 0 Then
            ddlStatus.SelectedValue = intCompleteIndex
            txtPercent.Text = 100
        ElseIf Len(txtDateComplete.Text) = 0 Then
            If txtPercent.Text = 100 Then txtPercent.Text = 50
            If ddlStatus.SelectedValue = intCompleteIndex Then ddlStatus.SelectedValue = intInProgressIndex
        End If
    End Sub

    Protected Sub DetailsView1_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles DetailsView1.ItemUpdated
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("TASKOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
            e.OldValues("Task").ToString() _
        )
    End Sub

    Protected Sub Task_Attachments_Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Task_Attachments_Grid.RowDeleted
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("TASKATTACHMENTOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("DELETEACTIONID")), _
            e.Values("File_Name").ToString() _
        )
    End Sub
End Class
