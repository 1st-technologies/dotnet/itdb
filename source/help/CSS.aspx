﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="CSS.aspx.vb" Inherits="help_CSS" title="CSS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>CSS</h1>
    <p>ITDB will detect the browser and apply the appropriate CSS file. All browsers first recive
    css\styles.css followed by the browser specific CSS file.</p>
    <p>Supported browsers and the css files are:</p>
    <table style="border-collapse: collapse;" border="1" cellpadding="3">
        <thead>
            <tr><th>Browser</th><th>CSS file</th></tr>
        </thead>
        <tbody>
            <tr><td>Chrome/Edge</td><td>css\chrome.css</td></tr>
            <tr><td>Firefox</td><td>css\firefox.css</td></tr>
            <tr><td>Internet Explorer 7</td><td>css\ie7.css</td></tr>
            <tr><td>Internet Explorer 8</td><td>css\ie8.css</td></tr>
            <tr><td>Internet Explorer 9</td><td>css\ie9.css</td></tr>
            <tr><td>Internet Explorer 10</td><td>css\ie10.css</td></tr>
            <tr><td>Internet Explorer 11</td><td>css\ie11.css</td></tr>
            <tr><td>Safari</td><td>css\safari.css</td></tr>
        </tbody>
    </table>
    <h2>Internet Explorer</h2>
    <p>ITDB uses Internet Explorer 10 emulation meta tags in the head section of all the 
    master templates. This is required to enable the CKEditor code.
    The appropriate meta tag to enable IE10 emulation is:</p>
    <pre>&lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=EmulateIE10&quot; /&gt;</pre>
</asp:Content>