﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="AdminAlerts.aspx.vb" Inherits="help_AdminAlerts" title="Alert Administration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Alerts Administration</h1>
    <p>ITDB can send email alerts to users based on the result of a SQL query.
    Alerts are evaluated every week-day at 11:00 AM.</p>
    <h2>How to Create an Alert</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" alt="Settings" class="inlineicon" />&nbsp;<b>Settings</b>
        icon</li>
        <li>Select the <b>Alerts</b> tab.</li>
        <li>Click the <img src="/images/mail2_(add)_16x16.gif" alt="New Alert" class="inlineicon" />&nbsp;<b>New&nbsp;Alert</b>
        icon.<br />
        <img src="images/newalert.png" alt="New Alert dialog" /></li>
        <li>Enter an <b>Alert</b> name.</li>
        <li>Enter a SQL <b>Query</b>, the query must return a single column of email addresses.<br />
        <div>Some Examples:</div>
        <div style="margin: 15px 0px -5px 0px; font-size: smaller;">An Alert Query to Return the Email Adresses of all
        users with tasks have not been updated within the last 20 days.</div>
        <pre>WITH OrderedTaskLogs AS(
	SELECT
		ROW_NUMBER() Over (
			PARTITION BY t.ID
			ORDER BY th.Date DESC
		) AS Log_Number,
		CONVERT(varchar(10), th.Date, 101) AS [Log Date],
		t.Status_ID As Status_ID,
		t.Start_Date As [Start Date],
		pr.archive AS [Archive],
		pl.email AS [email]
	FROM Tasks t
	INNER JOIN Projects pr ON t.Project_ID = pr.ID
	INNER JOIN Task_History th ON t.ID = th.Task_ID
	INNER JOIN People pl ON t.Person_ID = pl.ID
) SELECT email
FROM OrderedTaskLogs
WHERE
	Log_Number = 1 AND
	datediff(day, [Log Date], GETDATE()) > 20 AND
	Archive = 0 AND
	Status_ID <> 3 AND (
		[Start Date] IS NULL OR
		datediff(day, [Start Date],
		GETDATE()) > -1
	)
GROUP BY email</pre>
        <div style="margin: 15px 0px -5px 0px; font-size: smaller;">An Alert Query to Return a single email if duplicate
        users are found.</div>
        <pre>WITH DuplicateUsers AS(
	SELECT Count(*) AS CountOfPeople, full_name, email FROM People GROUP BY full_name, email
	UNION ALL
	SELECT Count(*) AS CountOfPeople, [login], email FROM People GROUP BY [login], email
)
SELECT DISTINCT 'alfredo.blanco@tradeweb.com' AS email FROM DuplicateUsers WHERE CountOfPeople &gt; 1</pre></li>
        <li>Enter alert message <b>Body</b>. The body may contain HTML markup.<br />
        <div>Example Message Body:</div>
        <pre>&lt;p style=&quot;font-family: sans-serif&quot;&gt;A duplicate User Full Name or Email was Detected in the ITDB Database.
        Please review the project and task assignments and disable the duplicate account&lt;/p&gt;</pre></li>
        <li>Click <input type="button" value="Save" class="tradewebbutton" /></li>
    </ol>
    <h2>How to Edit an Alert</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b>
        icon</li>
        <li>Select the <b>Alerts</b> tab.</li>
        <li>Click the <img src="/images/mail2_(edit)_16x16.gif" alt="Edit" class="inlineicon" />&nbsp;<b>Edit</b> icon next
        to the alert you wish to modify.<br />
        <img src="images/editalert.png" alt="Alert record in edit mode" class="inlineicon" /></li>
        <li>Make the required modifications and click the
        <img src="/images/save_16x16.gif" class="inlineicon" alt="Save" />&nbsp;<b>Save</b> icon to save changes
        or click <img src="/images/undo1_16x16.gif" class="inlineicon" alt="Cancel" />&nbsp;<b>Cancel</b>
        to revert changes.</li>
    </ol>
    <h2>How to Delete an Alert</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b>
        icon</li>
        <li>Select the <b>Alerts</b> tab.</li>
        <li>Click the <img src="/images/mail2_(delete)_16x16.gif" alt="Edit" class="inlineicon" />&nbsp;<b>Delete</b> icon next
        to the alert you wish to delete.</li>
        <li>Click <input type="button" value="OK" style="width: 80px;" /> to confirm.<br />
        <img src="images/deletealert.png" alt="Delete alert confirmation" style="margin: 5px 0px 0px 0px;" /></li>
    </ol>
</asp:Content>