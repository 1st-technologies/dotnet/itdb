﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="Utilities.aspx.vb" Inherits="help_Utilities" title="Utilities" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Utilities</h1>
    <p>ITDB Administration view provides several tools to enact bulk changes on the database.</p>
    <p>you can...</p>
    <ol>
        <li>Reassign all projects, tasks and issues from one user to another.</li>
        <li>Reassign specific tasks or issues from one user to another.</li>
        <li>Move tasks and issues between projects.</li>
    </ol>
    <p>The Utilities page provides full data validation to prevent accidental bulks changes.</p>
    <p><img src="images/utilities.png" alt="Utilities page" /></p>
</asp:Content>

