﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="ReportsHelp.aspx.vb" Inherits="help_ReportsHelp" title="Reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Reports</h1>
    <p>The Reports view presents a list of editable reports to examine ITDB data.
    All reports can be modified via the <a href="AdminReport.aspx">Report Administration Page</a>.
    The selected report can be exported to Excel by selecting the
    <img src="../images/excel_16x16.gif" class="inlineicon" alt="Export to Excel" />&nbsp;<b>Export</b> button.</p>
    <p><img src="images/reportsview.png" alt="Reports View" /></p>
</asp:Content>

