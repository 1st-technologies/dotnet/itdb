﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="AdminPeople.aspx.vb" Inherits="help_AdminPeople" title="People Administration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>People Administration</h1>
    <p>ITDB users must be a member of one of two Active Directory groups, the user must also be added
    to the ITDB People administration page and assigned a role to designate permissions.</p>
    <h2>Active Directory</h2>
    <p>ITDB users must be a member of <b>TWNY\ITDB_Users</b>, ITDB administrators must be a member of
    <b>TWNY\ITDB_Admins</b>.</p>
    <h2>&#39;People&#39; Settings</h2>
    <p>While the ITDB platform can support full role based access, at present, it is configured as an
    open platform. This means that all users can view and modify all projects, tasks and issues.</p>
    <div class="callout">A person record cannot be deleted, only disabled. Disabled users cannot be
    added to new Projects, Tasks or Issues.</div>
    <h2>How to Add a Person</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b>
        icon</li>
        <li>Select the <b>People</b> tab.</li>
        <li>Click the <img src="/images/administrator1_(add)_16x16.gif" alt="New Person" class="inlineicon" />&nbsp;<b>New&nbsp;Person</b>
        icon.</li>
        <li>Enter the user's <b>Full Name</b>, <b>Email</b> address, <b>Phone</b> number and Active Directory <b>Login</b>
        ID (including domain).<br />
        <img src="images/newperson.png" alt="New Person" /></li>
        <li>Select a user role.</li>
        <li>Click <input type="button" value="Save" class="tradewebbutton" /></li>
    </ol>
    <h2>How to Edit a Person</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b> icon</li>
        <li>Select the <b>People</b> tab.</li>
        <li>Click the <img src="/images/administrator1_(edit)_16x16.gif" alt="Edit" class="inlineicon" />&nbsp;<b>Edit</b> icon next to the user
        you wish to modify.<br />
        <img src="images/editperson.png" alt="Person record in edit mode" /></li>
        <li>Make the required modifications and click the
        <img src="/images/save_16x16.gif" class="inlineicon" alt="Save" />&nbsp;<b>Save</b> icon to save changes
        or click <img src="/images/undo1_16x16.gif" class="inlineicon" alt="Cancel" />&nbsp;<b>Cancel</b>
        to revert changes.</li>
    </ol>
</asp:Content>