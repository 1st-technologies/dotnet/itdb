﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="GettingStarted.aspx.vb" Inherits="help_GettingStarted" title="Getting Started" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Getting Started</h1>
    <p>The first consideration when documenting and tracking work activity is to determine if
    the effort constitutes:</p>
    <ol class="upper-alpha">
        <li>An operational task or break-fix</li>
        <li>A project oriented task</li>
        <li>An issue encountered in the course of a project</li>
    </ol>
    <p>The second question you must ask is, given an operational or project task, does this task constitute an
    &quot;accomplishment&quot;? Specifically, do you want it to appear in the monthly accomplishment reports.</p>
    <h2>Projects</h2>
    <p>According to the <a href="http://www.pmi.org">Project Management Institute</a>, a project is
    &quot;a temporary endeavour undertaken to create a unique product, service or result.&quot;<a href="#fn1" id="ref1" class="footnote">1</a>.
    However, while a traditional project must have a definitive goals and end date. ITDB dynamically
    sets start and end dates from the dates of the enclosed tasks, therefore a ITDB project can also
    be comprised of what are normally operational tasks as well.</p>
    <div class="callout">When creating tasks, keep in mind that accomplishments are simply tasks with the
    accomplishment bit set.</div>
    <h2>Tasks</h2>
    <p>A Task is any given activity intended to further a project that <i>does not represent an issue encountered
    during the execution of that project</i>. While an end date is not required, <b>all tasks must have a specific
    goal</b>. A given task should be the largest unit of work that can be <i>accomplished by a single individual</i>.
    Tasks can be reassigned but should never be shared.</p>
    <h3>Task Scope</h3>
    <p>The scope of a task can vary depending on the reporting requirements. For instance, if a task will take
    two months to complete, but management requires weekly accomplishment reporting, consider splitting the task
    (i.e one task to build 40 servers -> 8 tasks to build 5 servers). Otherwise, if the reporting of a task can
    wait for the full duration, it is optimal to create the fewest tasks possible.</p>
    <h3>Accomplishments</h3>
    <p>Part of the function of ITDB is to facilitate the reporting of monthly accomplishments. In ITDB, all Accomplishments
    are Tasks but not all Tasks need be Accomplishments.  When creating tasks, you must decide is the task represents
    an activity that you wish to be reported to upper management or if it is mearly part of a larger effort best
    represented by another task in the project. Keep in mind that Accomplishments are simply tasks with the
    accomplishment bit set.</p>
    <h2>Issues</h2>
    <p>An Issue is an occurrence that presents an impediment to a project. An Issue cannot be an
    accomplishment, only the task that the issue is preventing the completion of can be an accomplishment.
    However, when an Issue is found to be systemic in nature, (a &quot;problem&quot; in ITIL terminology)
    it should be created as a task and therefore can be counted as an accomplishment.</p>
    <hr />
    <sup id="fn1">1. See <a href="http://www.pmi.org/About-Us/About-Us-What-is-Project-Management.aspx" target="_blank">http://www.pmi.org/About-Us/About-Us-What-is-Project-Management.aspx</a>
    <a href="#ref1" title="Jump back to footnote 1 in the text.">↩</a></sup>
</asp:Content>