﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="AdminPriorities.aspx.vb" Inherits="help_AdminPriorities" title="Priority Administration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Priority Administration</h1>
    <p>Priorities are the display names given to the internal Task and Issue Priority levels.</p>
    <div class="callout">A priority cannot be deleted. The &#39Normal&#39; priority is a special internal priority level.
    Any changes to this priority must be coordinated with changes to <a href="IIS.aspx">IIS</a>.</div>
    <h2>How to Add a Priority</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b>
        icon</li>
        <li>Select the <b>Priority</b> tab.</li>
        <li>Click the <img src="/images/flag2_(add)_16x16.gif" alt="New Priority" class="inlineicon" />&nbsp;<b>New&nbsp;Priority</b> icon.</li>
        <li>Enter the <b>Priority</b> name and click the <input type="button" value="Save" class="tradewebbutton" /> button.<br />
        <img src="images/newpriority.png" alt="New Status" /></li>
    </ol>
    <h2>How to Edit a Priority</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b>
        icon</li>
        <li>Select the <b>Priorities</b> tab.</li>
        <li>Click the <img src="/images/flag2_(edit)_16x16.gif" alt="Edit" class="inlineicon" />&nbsp;<b>Edit</b> icon next
        to the priority you wish to modify.<br />
        <img src="images/editpriority.png" alt="Priority record in edit mode" /></li>
        <li>Make the required modifications and click the
        <img src="/images/save_16x16.gif" class="inlineicon" alt="Save" />&nbsp;<b>Save</b> icon to save changes
        or click <img src="/images/undo1_16x16.gif" class="inlineicon" alt="Cancel" />&nbsp;<b>Cancel</b>
        to revert changes.</li>
    </ol>
</asp:Content>

