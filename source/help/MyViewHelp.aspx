﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="MyViewHelp.aspx.vb" Inherits="help_MyViewHelp" title="'My' View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>&#39;My&#39;&nbsp;View</h1>
    <p>The <b>&#39;My&#39; View</b> shows all currently pending tasks and issues assigned to you.
    This view can be filtered by <b>Type</b> (task or issue), <b>Priority</b> or <b>Status</b>.
    The task list, with current filter states, can be exported to Excel by selecting the
    <img src="../images/excel_16x16.gif" class="inlineicon" alt="Export to Excel" />&nbsp;<b>Export</b> button.
    Selecting the task or issue name will link back to the task or issue.</p>
    <p><img src="images/myview.png" alt="'My' View" /></p>
</asp:Content>