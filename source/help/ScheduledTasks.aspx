﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="ScheduledTasks.aspx.vb" Inherits="help_ScheduledTasks" title="Scheduled Tasks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Scheduled Tasks</h1>
    <h2>Send Alerts</h2>
    <p>The Web server is configured to run the following powershell script in d:\jobs every weekday at 11:00AM.</p>
    <pre>&lt;#
.SYNOPSIS
    Enumerates email form a SQL table and ends an alert email.
.DESCRIPTION
    Enumerates email form a SQL table and ends an alert email. The query must return the following three columns:
    [Alert] which is the name of the alert and subject line in the resulting email
    [Query] The query to use to get recipient emails. This query must return a single column of emails and the field title must br &quot;email&quot;
    [Body] The body of the alert email
.EXAMPLE
    powershell -file sendalerts.ps1
.LINK
    http://ITDB/
.NOTES
    Author: Alfredo Blanco
    Date:   2015/09/15
    (c) 2015 Eden Technologies, Inc.
#&gt;


# Define and validate parameters
[CmdletBinding()]
param()

$smtpserver = &quot;smtp.tradeweb.com&quot;
$smtpport = 25
$from = &quot;ITDB_noreply@tradeweb.com&quot;
$bodyheader = '&lt;div style=&quot;background-color: #1A5DA8; height: 90px; white-space: normal; padding: 10px;&quot;&gt;' + `
    '&lt;div&gt;&nbsp;&lt;/div&gt;' + `
    '&lt;span&gt;&nbsp;&lt;/span&gt;' + `
    '&lt;img src=&quot;http://ITDB/images/tradeweb-logo.png&quot; alt=&quot;Tradeweb Logo&quot; /&gt;' + `
    '&lt;span&gt;&nbsp;&lt;/span&gt;' + `
    '&lt;span style=&quot;font-weight: bold; color: white; font-size: 28px; font-family: sans-serif&quot;&gt;Distributed Systems Database&lt;/span&gt;' + `
    '&lt;div&gt;&nbsp;&lt;/div&gt;' + `
    '&lt;/div&gt;'
# NOTE: Outlook client ignores many common HTML tags: See http://www.outlook-apps.com/html-ignored-by-outlook



Function fncSQLQuery{
    param ([string]$query)
    $SqlConnection = New-Object System.Data.SqlClient.SqlConnection
    $SqlConnection.ConnectionString = &quot;Data Source=DNYSQLGEN3;Initial Catalog=ITDB;Persist Security Info=True;User ID=ITDB_RW;Password=[PASSWORD]&quot;
    $SqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $SqlCmd.CommandText = $query
    $SqlCmd.Connection = $SqlConnection
    $SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
    $SqlAdapter.SelectCommand = $SqlCmd
    $DataSet = New-Object System.Data.DataSet
    Try{
        [void] $SqlAdapter.Fill($DataSet)
    } Catch {
        throw (&quot;Error connecting to database: {0}&quot; -f $($_.exception.message))
    }
    $SqlConnection.Close()
    return $DataSet
}

write-verbose &quot;Getting Fortune...&quot;
$fortune = [System.IO.File]::ReadAllText('fortune.txt') -replace &quot;`r`n&quot;, &quot;`n&quot; -split &quot;`n%`n&quot; | Get-Random
Write-Verbose &quot;Querying ITDB for defined alerts...&quot;
$ds1 = fncSQLQuery(&quot;SELECT Query, Body, Alert FROM Alerts WHERE Enabled = 1&quot;)
Foreach ($row1 in $ds1.Tables[0].Rows) {
    $query = $row1[&quot;query&quot;]
    $body = $bodyheader + $row1[&quot;body&quot;] + &quot;`r`n`r`n&quot; + '&lt;hr /&gt;&lt;div style=&quot;font-family: monospace; font-size: x-small;&quot;&gt;' + $fortune + '&lt;/div&gt;'
    $subject = $row1[&quot;Alert&quot;]
    write-verbose &quot;Enumerating email adresses for the '$subject' alert...&quot;
    $ds2 = fncSQLQuery($query)
    Foreach ($row2 in $ds2.Tables[0].Rows) {
        $email = $row2[&quot;email&quot;]
        Write-Verbose &quot;Sending Email to $email...&quot;
        Try{
            Send-MailMessage -From &quot;ITDB_noreply@tradeweb.com&quot; -Subject $subject -To $email -Body $body -BodyAsHtml -Port $smtpport -Priority High -SmtpServer $smtpserver -DeliveryNotificationOption OnFailure
        } catch {
            throw (&quot;Error sending email: {0}&quot; -f $($_.exception.message))
        }
    }
}</pre>
</asp:Content>

