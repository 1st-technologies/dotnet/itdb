﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="IssueHelp.aspx.vb" Inherits="help_IssueHelp" title="Issues" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Issues</h1>
    <h2>How to Create an Issue</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select a project name from the Projects list.<br />
        <img src="images/projectgrid.png" alt="Projects List" /></li>
        <li>Click the <img src="/images/support1_(add)_16x16.gif" class="inlineicon" alt="Create a New Issue" />&nbsp<b>Create a New Issue</b> icon.<br />
        <img src="images/newissue.png" alt="New Issue" /></li>
        <li>Enter an <b>Issue Name</b> and <b>Description</b>.</li>
        <li>Select an <b>Assignee</b> and <b>Priority</b>.</li>
        <li>Click <input type="button" value="Save" class="tradewebbutton" /></li>
    </ol>
    <h2>How to Edit an Issue</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select a project from the Projects list.<br />
        <img src="images/projectgrid.png" alt="Projects List" /></li>
        <li>Select an Issue from the Issues list.<br />
        <img src="images/issuegrid.png" alt="Issue List" /></li>
        <li>Click the <a href="">Edit</a> link.<br />
        <img src="images/viewissue.png" alt="View Issue" /></li>
        <li>Edit each field as required and select the <a href="">Update</a>
        link to save changes.<br />
        <img src="images/editissue.png" alt="Edit Issue"/><br />
    </ol>
    <h2>How to Delete an Issue</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select a project from the Projects list.<br />
        <img src="images/projectgrid.png" alt="Projects List" /></li>
        <li>Select the <img src="/images/support1_(delete)_16x16.gif" alt="Delete" class="inlineicon" />&nbsp;<b>Delete</b> icon.</li>
        <li>Click <input type="button" value="OK" style="width: 80px;" /> to confirm.<br />
        <img src="images/deleteissue.png" alt="Delete issue confirmation" style="margin: 5px 0px 0px 0px;" /></li>
    </ol>
    <h2>How to Add a Log Entry</h2>
    <p>Issues support the creation of log entries. Log entries cannot be deleted.</p>
    <p>See <a href="LogEntryHelp.aspx">How to Create a Log Entry</a></p>
</asp:Content>

