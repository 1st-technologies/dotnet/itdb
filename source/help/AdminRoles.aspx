﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="AdminRoles.aspx.vb" Inherits="help_AdminRoles" title="Roles Administration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Roles Administration</h1>
    <p>While the ITDB platform can support full role based access, at present, it is configured as an
    open platform. This means that all users can view and modify all projects, tasks and issues.</p>
    <p>At present, only Managers can be assigned to or delete projects.</p>
    <div class="callout">A role cannot be deleted. The &#39;Manager&#39; role is a special internal role,
    any changes to this role must be coordinated with changes to <a href="IIS.aspx">IIS</a>.</div>
    <h2>How to Add a Role</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b> icon</li>
        <li>Select the <b>Roles</b> tab.</li>
        <li>Click the <img src="/images/access_(add)_16x16.gif" alt="New Person" class="inlineicon" />&nbsp;<b>New&nbsp;Person</b> icon.</li>
        <li>Enter the <b>Role</b> name and click the <input type="button" value="Save" class="tradewebbutton" /> button.<br />
        <img src="images/newrole.png" alt="New Role" /></li>
    </ol>
    <h2>How to Edit a Role</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b> icon</li>
        <li>Select the <b>Roles</b> tab.</li>
        <li>Click the <img src="/images/access_(edit)_16x16.gif" alt="Edit" class="inlineicon" />&nbsp;<b>Edit</b> icon next to the role
        you wish to modify.<br />
        <img src="images/editrole.png" alt="Role record in edit mode" /></li>
        <li>Make the required modifications and click the
        <img src="/images/save_16x16.gif" class="inlineicon" alt="Save" />&nbsp;<b>Save</b> icon to save changes
        or click <img src="/images/undo1_16x16.gif" class="inlineicon" alt="Cancel" />&nbsp;<b>Cancel</b>
        to revert changes.</li>
    </ol>
</asp:Content>

