﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="ProjectHelp.aspx.vb" Inherits="help_ProjectHelp" title="Projects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Projects</h1>
    <h2>Viewing Projects</h2>
    <p>The default homepage of <b>ITDB</b> presents a list of all projects.
    The Projects view can be filtered by <b>Owner</b>, <b>BRAG</b> status and <b>Platform</b>.
    Completed projects can be viewed by selecting the <b>Completed</b> checkbox.
    The project list with current filter states can be exported to Excel by selecting the
    <img src="../images/excel_16x16.gif" class="inlineicon" alt="Export to Excel" />&nbsp;<b>Export</b> button</p>
    <p><img src="images/projectsview.png" alt="Projects View" /></p>
    <p>From here you can navigate to the
        <img src="../images/administrator1_16x16.gif" class="inlineicon" alt="'My' View" />&nbsp;<b>&#39;My&#39;&nbsp;View</b>,
        the <img src="../images/report3_16x16.gif" class="inlineicon" alt="Reports View" />&nbsp;<b>Reports&nbsp;View</b>,
        the <img src="../images/graph2_16x16.gif" class="inlineicon" alt="Dashboard" />&nbsp;<b>Dashboard</b> and
        the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b> view.
    </p>
    <h2>BRAG Status</h2>
    <p>ITDB uses the <a href="https://en.wikipedia.org/wiki/Traffic_light_rating_system#Performance_monitoring" target="_blank">Traffic Light Rating System</a>.
    Project status is indicated by the following color codes:</p>
    <table style="border-collapse: collapse" border="1" cellpadding="3">
        <thead>
            <tr>
                <th>Color Code</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="background-color: #0066ff; color:White; font-weight: bold">BLUE</td><td>All Tasks Completed/No Open Issues</td>
            </tr>
            <tr>
                <td style="background-color: red; color:White; font-weight: bold">RED</td><td>Overdues Task(s)</td>
            </tr>
            <tr>
                <td style="background-color: #ffb547; color:White; font-weight: bold">AMBER</td><td>Open Issue(s)</td>
            </tr>
            <tr>
                <td style="background-color: #00b050; color:White; font-weight: bold">GREEN</td><td>On Time, No Open Issues</td>
            </tr>
        </tbody>
    </table>
    <h2>How to Create a New Project</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select the <img src="/images/record_(add)_16x16.gif" alt="Create a new project" class="inlineicon" />&nbsp;<b>Create a new project</b> icon.<br />
        <img src="images/newproject.png" alt="New project Dialog" /></li>
        <li>Enter a <b>Project Name</b> and <b>Description</b>.</li>
        <li>Select a <b>Project Owner</b> and <b>Platform</b>. Only &#39;Managers&#39; can be made project owners<a href="#fn1" id="ref1" class="footnote">1</a>.</li>
        <li>Select the <b>Business as Usual</b> option if the project
        will constitute an operational effort.</li>
        <li>Click <input type="button" value="Save" class="tradewebbutton" /></li>
    </ol>
    <h2>How to Edit a Project</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select a project name from the Projects list.<br />
        <img src="images/projectgrid.png" alt="Projects List" /></li>
        <li>Click the <a href="">Edit</a> link.<br />
        <img src="images/viewproject.png" alt="View Project" /></li>
        <li>Edit each field as required and select the <a href="">Update</a>
        link to save changes.<br />
        <img src="images/editproject.png" alt="Edit Project"/></li>
    </ol>
    <h2>How to Delete a Project</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select the <img src="/images/record_(delete)_16x16.gif" alt="Delete" class="inlineicon" />&nbsp;<b>Delete</b> icon.</li>
        <li>Click <input type="button" value="OK" style="width: 80px;" /> to confirm.<br />
        <img src="images/deleteproject.png" alt="Delete project confirmation" style="margin: 5px 0px 0px 0px;" /></li>
    </ol>
    <h2>How to Add a Log Entry</h2>
    <p>Projects support the creation of log entries. Log entries cannot be deleted.</p>
    <p>See <a href="LogEntryHelp.aspx">How to Create a Log Entry</a></p>
    <h2>How to Attach Files to a Project</h2>
    <p>You can upload and attach files to a project. NOTE: the maximum files size is 50MB.</p>
    <p>See <a href="Attachments.aspx">How to Upload a File Attachment</a></p>
    <hr />
    <sup id="fn1">1. See <a href="AdminRoles.aspx">Roles Administration</a>
    <a href="#ref1" title="Jump back to footnote 1 in the text.">↩</a></sup>
</asp:Content>
