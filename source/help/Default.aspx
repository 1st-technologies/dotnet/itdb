﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="help_Default" title="Help Overview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Tradeweb IT Database Help</h1>
    <h2>Overview</h2>
    <p>Tradeweb IT Database (ITDB) is an application designed to track project and operational
    activities with minimal overhead. Activities in ITDB are grouped into projects, tasks and issues.</p>
    <div class="callout">
        <div style="font-weight: bold;">Features</div>
        <ul>
            <li>Linked fields</li>
            <li>AJAX</li>
            <li>Data Validation</li>
            <li>Data Masking</li>
            <li>In-Line Editing</li>
            <li>Minimal Design</li>
        </ul>
    </div>
    <h2>Automation</h2>
    <p>ITDB works to minimize data-entry by performing several background calculations to dynamically
    calculate project
    &quot;<a href="https://en.wikipedia.org/wiki/Traffic_light_rating_system#Performance_monitoring" target="_blank"><b><span style="color:#0066ff">B</span><span style="color:red">R</span><span style="color:#ffb547">A</span><span style="color:#00b050">G</span></b></a>&quot;
    status from the status of all enclosed tasks and issues. Likewise, <b>completed date</b>, <b>status</b>
    and <b>percent</b> are programatically linked meaning that a <i>user need enter only one value, not all
    three</i>, thus reducing necessary data-entry. Data entry is further accelerated by the use of data validation
    and data masking.</p>
    <h2>Designed for Speed</h2>
    <p>ITDB employs <a href="https://en.wikipedia.org/wiki/Ajax_(programming)">AJAX</a> technologies and
    in-line editing to minimize page reloads and a minimal design along with cache tuning to make page
    reloads, when necessary, as fast as possible. The final result is an application that gets out of
    your way so you can get back to work.</p>
    <h2>Open Access</h2>
    <p>While the ITDB platform can support full role based access, at present, ITDB is configured as an
    open platform. This means that all users can view and modify all projects, tasks and issues.
    Administrative tasks still require manager access.</p>
</asp:Content>