﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="AdminReport.aspx.vb" Inherits="help_AdminReport" title="Report Administration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Report Administration</h1>
    <p>ITDB supports through-the-web report editing with an integrated menu system.</p>
    <h2>How to Create a Report or Report Node</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b>
        icon</li>
        <li>Select the <b>Reports</b> tab.</li>
        <li>Click the <img src="/images/report3_(add)_16x16.gif" class="inlineicon" alt="New Report" />&nbsp;<b>New&nbsp;Report</b> icon.<br />
        <img src="images/newreport.png" alt="New Report dialog" /></li>
        <li>Enter a report <b>Title</b>.</li>
        <li>If you are creating an empty report node leave the <b>Query</b> field blank. Else,
        Enter a valid query using the database schema outlined in the <a href="SQL.aspx">Adavance - SQL</a> manual.
        SQL column selections can return HTML code to format the report as desired.
        <div>Some Examples:</div>
        <div style="margin: 15px 0px -5px 0px;">A Report Query to Return all projects with a link to the project details in the first column.</div>
        <pre>SELECT
	'&lt;a href=&quot;ProjectDetails.aspx?ProjectID=' + Convert(varchar(16), pr.ID) + '&quot;&gt;' + pr.Project + '&lt;/a&gt;' AS Project,
	c.Platform,
	pl.full_name AS [Owner]
FROM Projects pr
INNER JOIN Platforms c ON pr.Platform_ID = c.ID
INNER JOIN People pl ON pr.Person_ID = pl.ID
WHERE pr.archive = 0</pre>
    <div style="margin: 15px 0px -5px 0px;">A Report Query to Return all incomplete tasks that have not been updatded in the last 20 days.</div>
    <pre>WITH OrderedTaskLogs AS(
	SELECT
		ROW_NUMBER() Over (
			PARTITION BY t.ID ORDER BY th.Date DESC
		) AS Log_Number,
		'&lt;a href="ProjectDetails.aspx?ProjectID=' + Convert(varchar(16), pr.ID) + '"$gt;' + pr.Project + '&lt;/a&gt;' AS Project,
		'Task' AS [Type],
		'&lt;a href="TaskDetails.aspx?TaskID=' + Convert(varchar(16), t.ID) + '"&gt;' + t.Task + '&lt;/a&gt;' AS Item,
		th.Log_Entry AS [Log],
		CONVERT(varchar(10), th.Date, 101) AS [Log Date],
		pl.full_name AS [Owner],
		pr.archive AS [Archive],
		t.Status_ID As Status_ID,
		t.Start_Date As [Start Date]
	FROM Tasks t
	INNER JOIN Projects pr ON t.Project_ID = pr.ID
	INNER JOIN Task_History th ON t.ID = th.Task_ID
	INNER JOIN People pl ON t.Person_ID = pl.ID
	UNION ALL
	SELECT
		ROW_NUMBER() Over (
			PARTITION BY i.ID ORDER BY ih.Date DESC
		) AS Log_Number,
		'&lt;a href=&quot;ProjectDetails.aspx?ProjectID=' + Convert(varchar(16), pr.ID) + '&quot;&gt;' + pr.Project + '&lt;/a&gt;' AS Project,
		'Issue' AS [Type],
		'&lt;a href=&quot;IssueDetails.aspx?IssueID=' + Convert(varchar(16), i.ID) + '&quot;&gt;' + i.Issue + '&lt;/a&gt;' AS Item,
		ih.Log_Entry AS [Log],
		CONVERT(varchar(10), ih.Date, 101) AS [Log Date],
		pl.full_name AS [Owner],
		pr.archive AS [Archive],
		i.Status_ID AS Status_ID,
		NULL AS [Start Date]
	FROM Issues i
	INNER JOIN Projects pr ON i.Project_ID = pr.ID
	INNER JOIN Issue_History ih ON i.ID = ih.Issue_ID
	INNER JOIN People pl ON i.Person_ID = pl.ID
)
SELECT
	Project,
	[Type],
	Item,
	[Log],
	[Log Date],
	[Owner],
	datediff(day, [Log Date], GETDATE()) As Age
FROM OrderedTaskLogs
WHERE
	Log_Number = 1 AND
	datediff(day, [Log Date], GETDATE()) &gt; 20 AND
	Archive = 0 AND
	Status_ID &lt;&gt; 3 AND (
		[Start Date] IS NULL OR
		datediff(day, [Start Date], GETDATE()) &gt; -1
	)
ORDER BY
	Age DESC,
	Project,
	Item</pre></li>
	    <li>Select a menu node to place the new reports or select &quot;Root&quot; to place the report in the root of
	    the report menu.</li>
	    <li>Click <input type="button" value="Save" class="tradewebbutton" /></li>
    </ol>
    <h2>How to Edit a Report or Node</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b>
        icon</li>
        <li>Select the <b>Alerts</b> tab.</li>
        <li>Click the <img src="/images/report3_(edit)_16x16.gif" alt="Edit" class="inlineicon" />&nbsp;<b>Edit</b> icon next
        to the report you wish to modify.<br />
        <img src="images/editreport.png" alt="Report record in edit mode" class="inlineicon" /></li>
        <li>Make the required modifications and click the
        <img src="/images/save_16x16.gif" class="inlineicon" alt="Save" />&nbsp;<b>Save</b> icon to save changes
        or click <img src="/images/undo1_16x16.gif" class="inlineicon" alt="Cancel" />&nbsp;<b>Cancel</b>
        to revert changes.</li>
    </ol>
    <h2>How to Delete a Report or Node</h2>
    <div class="callout">You can only a delete a report or node that does not have sub-reports or nodes. Move or delete all subnodes to enable the report delete button.</div>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b>
        icon</li>
        <li>Select the <b>Reports</b> tab.</li>
        <li>Click the <img src="/images/report3_(delete)_16x16.gif" alt="Edit" class="inlineicon" />&nbsp;<b>Delete</b> icon next
        to the report you wish to delete.</li>
        <li>Click <input type="button" value="OK" style="width: 80px;" /> to confirm.<br />
        <img src="images/deletereport.png" alt="Delete report confirmation" style="margin: 5px 0px 0px 0px;" /></li>
    </ol>
</asp:Content>

