﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="AdminPlatforms.aspx.vb" Inherits="help_AdminPlatforms" title="Platform Administration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Platform Administration</h1>
    <p>Platforms are the platforms to which a project can be assigned.</p>
    <div class="callout">A platform cannot be deleted.</div>
    <h2>How to Add a Platform</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b>
        icon</li>
        <li>Select the <b>Priority</b> tab.</li>
        <li>Click the <img src="/images/server1_(add)_16x16.gif" alt="New Platform" class="inlineicon" />&nbsp;<b>New&nbsp;Platform</b> icon.</li>
        <li>Enter the <b>Platform</b> name and click the <input type="button" value="Save" class="tradewebbutton" /> button.<br />
        <img src="images/newplatform.png" alt="New Status" /></li>
    </ol>
    <h2>How to Edit a Platform</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b>
        icon</li>
        <li>Select the <b>Platforms</b> tab.</li>
        <li>Click the <img src="/images/server1_(edit)_16x16.gif" alt="Edit" class="inlineicon" />&nbsp;<b>Edit</b> icon
        next to the platform you wish to modify.<br />
        <img src="images/editplatforms.png" alt="Platform record in edit mode" /></li>
        <li>Make the required modifications and click the
        <img src="/images/save_16x16.gif" class="inlineicon" alt="Save" />&nbsp;<b>Save</b> icon to save changes
        or click <img src="/images/undo1_16x16.gif" class="inlineicon" alt="Cancel" />&nbsp;<b>Cancel</b>
        to revert changes.</li>
    </ol>
</asp:Content>

