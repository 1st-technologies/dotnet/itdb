﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="ITIL.aspx.vb" Inherits="help_ITIL" title="Relationship to ITIL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Relationship to ITIL</h1>
    <img src="/images/white_head_eagle-1920x1080-transparent1.png" align="right" title="Vietnam was a tie" />
    <p>There is none, ITIL is a <a href="https://en.wikipedia.org/wiki/ITIL#History" target="_blank">British import</a>
    and no red-blooded American should have anything to do with it!</p>
    <p>With that, the following table shows the nearest analogues to ITDB objects.</p>
    <table style="border-collapse: collapse; border: solid black 1px" border="1">
        <thead>
            <tr>
                <th>ITIL</th>
                <th>ITDB</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Event</td>
                <td>Issue</td>
            </tr>
            <tr>
                <td>Incident</td>
                <td>Issue</td>
            </tr>
            <tr>
                <td>Problem</td>
                <td>Project</td>
            </tr>
            <tr>
                <td>Change</td>
                <td>Project</td>
            </tr>
            <tr>
                <td>Release</td>
                <td>Task</td>
            </tr>
            <tr>
                <td>Configuration</td>
                <td>Task</td>
            </tr>
        </tbody>
    </table>
</asp:Content>

