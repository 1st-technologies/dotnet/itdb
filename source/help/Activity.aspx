﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="Activity.aspx.vb" Inherits="help_Activity" title="Activity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Activity Log</h1>
    <p>ITDB logs all user activity in the Activity page.</p>
    <p><img src="images/activities.png" alt="Activity log page" /></p>
</asp:Content>

