﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="Attachments.aspx.vb" Inherits="help_Attachments" title="Attachments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Attachments</h1>
    <p>ITDB allows users to attach Files to Projects and Tasks.</p>
    <h2>How to Attach a File</h2>
    <ol>
        <li>Navigate to the Project or Task</li>
        <li>Select the <img src="/images/file_(add)_16x16.gif" alt="Upload Attachment" class="inlineicon" />&nbsp;<b>Upload Attachment</b> icon.<br />
        <img src="images/newattachment.png" alt="Upload Attachment" /></li>
        <li>Click the <input type="button" value="Browse" /> button.<br />
        <img src="images/browse.png" alt="Browse Dialog" /></li>
        <li>Browse for and select a file and click <input type="button" value="Open" />.</li>
        <li>Enter a file description.</li>
        <li>Click <input type="button" value="Save" class="tradewebbutton" />.</li>
    </ol>
    <h2>How to Delete an Attachment</h2>
    <ol>
        <li>Navigate to the Project or Task</li>
        <li>Select the <img src="/images/file_(delete)_16x16.gif" alt="Delete" class="inlineicon" />&nbsp;<b>Delete</b> icon
        next to the file you wish to delete.</li>
        <li>Click <input type="button" value="OK" style="width: 80px;" /> to confirm.<br />
        <img src="images/deleteattachmentconfirm.png" alt="Delete attachment confirmation" style="margin: 5px 0px 0px 0px;" /></li>
    </ol>
</asp:Content>

