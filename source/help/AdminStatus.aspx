﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="AdminStatus.aspx.vb" Inherits="help_AdminStatus" title="Status Administration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Status Administration</h1>
    <p>Status type are the display names given to the internal Task and Issue Status types.</p>
    <div class="callout">A status cannot be deleted. Any changes to status types
    must be coordinated with changes to <a href="IIS.aspx">IIS</a>.</div>
    <h2>How to Add a Status Type</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b> icon</li>
        <li>Select the <b>Status</b> tab.</li>
        <li>Click the <img src="/images/label_(add)_16x16.gif" alt="New Status" class="inlineicon" />&nbsp;<b>New&nbsp;Status</b> icon.</li>
        <li>Enter the <b>Role</b> name and click the <input type="button" value="Save" class="tradewebbutton" /> button.<br />
        <img src="images/newstatustype.png" alt="New Status" /></li>
    </ol>
    <h2>How to Edit a Status Type</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b> icon</li>
        <li>Select the <b>Status</b> tab.</li>
        <li>Click the <img src="/images/label_(edit)_16x16.gif" alt="Edit" class="inlineicon" />&nbsp;<b>Edit</b> icon next to the status
        you wish to modify.<br />
        <img src="images/editstatustypes.png" alt="Status record in edit mode" /></li>
        <li>Make the required modifications and click the
        <img src="/images/save_16x16.gif" class="inlineicon" alt="Save" />&nbsp;<b>Save</b> icon to save changes
        or click <img src="/images/undo1_16x16.gif" class="inlineicon" alt="Cancel" />&nbsp;<b>Cancel</b>
        to revert changes.</li>
    </ol>
</asp:Content>

