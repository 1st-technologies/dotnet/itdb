﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="IIS.aspx.vb" Inherits="help_IIS" title="IIS Configuration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>IIS Administration</h1>
    <h2>DNS Alias and Bindings</h2>
    <p>ITDB requires the DNS Alias (&quot;C&quot; record) &quot;ITDB&quot; and the appropriate host header
    bindings to &quot;itdb&quot; and &quot;itdb.nyoffice.tradeweb.com&quot;.</p>
    <h2>Service Principle Names (SPNs)</h2>
    <p>ITDB requires SPNs configured as follows</p>
    <ul>
        <li>setspn –S HTTP/itdb hostname</li>
        <li>setspn –S HTTP/itdb.nyoffice.tradeweb.com hostname</li>
    </ul>
    <p>Where &#39;hostname&#39; is the netbios name of the server hosting ITDB.</p>
    <h2>Application Pool</h2>
    <p>The ITDB application pool must be configured as .NET CLS 2.0.50727 with &#39;classic&#39; managed pipeline mode.</p>
    <p style="color: red; font-weight: bold;">NOTE: Even though ITDB is developed in .NET 3.5 the application pool uses .NET 2
    as NET 3 and 3.5 did not change the core engine from version 2.</p>
    <h2>Authentication</h2>
    <p>ITDB uses Windows authentication configured with the &#39;Negotiate&#39; and NTLM authentication
    providers. All other authentication providers should be diabled.</p>
    <div class="callout">The &#39;Negotiate&#39; provider uses kerberos then fails over to NTLM.</div>
    <p><img src="images/iisauthentication.png" alt="IIS Authentication configuration" /></p>
    <h2>Temp Folder</h2>
    <p>The charting feature requires that Everyone be given full access to d:\temp as configred by the <a href="#ChartImageHandler">ChartImageHandler</a> setting in the WEB.Config file.</p>
    <h2>Logging</h2>
    <p>IIS Logging for ITDB has been redirected to D:\inetpub\logs\LogFiles.</p>
    <h2>WEB.config</h2>
    <p>ITDB requires the following additions to the web.config file.</p>
    <pre>&lt;configuration&gt;
	&lt;configSections&gt;
		&lt;add key=&quot;PROJECTOWNERMENUDEFAULT&quot; value=&quot;Select a Project Owner&quot; /&gt;
		&lt;add key=&quot;TASKOWNERMENUDEFAULT&quot; value=&quot;Select a Task Owner&quot; /&gt;
		&lt;add key=&quot;ISSUEOWNERMENUDEFAULT&quot; value=&quot;Select an Issue Owner&quot; /&gt;
		&lt;add key=&quot;PRIORITYMENUDEFAULT&quot; value=&quot;Select a Priority&quot; /&gt;
		&lt;add key=&quot;PLATFORMMENUDEFAULT&quot; value=&quot;Select a Platform&quot; /&gt;
		&lt;add key=&quot;USERMENUDEFAULT&quot; value=&quot;Select a User&quot; /&gt;
		&lt;add key=&quot;PROJECTMENUDEFAULT&quot; value=&quot;Select a Project&quot; /&gt;
		&lt;add key=&quot;NODEMENUEFAULT&quot; value=&quot;Select a Node&quot; /&gt;

		&lt;add key=&quot;NORMALPRIORITYINDEX&quot; value=&quot;2&quot; /&gt;

		&lt;add key=&quot;MAXFILESIZE&quot; value=&quot;53687091200&quot; /&gt;
		&lt;add key=&quot;MAXSIZESTRING&quot; value=&quot;50MB&quot; /&gt;

		&lt;add key=&quot;STATUSCOMPLETEINDEX&quot; value=&quot;3&quot; /&gt;
		&lt;add key=&quot;STATUSINPROGRESSINDEX&quot; value=&quot;2&quot; /&gt;
		&lt;add key=&quot;STATUSNOTSTARTEDINDEX&quot; value=&quot;1&quot; /&gt;
		&lt;add key=&quot;STATUSONHOLDINDEX&quot; value=&quot;4&quot; /&gt;
		&lt;add key=&quot;MANAGERROLEINDEX&quot; value=&quot;1&quot; /&gt;

		&lt;add key=&quot;CATEGOROBJECTID&quot; value=&quot;1&quot; /&gt;
		&lt;add key=&quot;ISSUEHISTORYOBJECTID&quot; value=&quot;2&quot; /&gt;
		&lt;add key=&quot;ISSUEOBJECTID&quot; value=&quot;3&quot; /&gt;
		&lt;add key=&quot;PERSONOBJECTID&quot; value=&quot;4&quot; /&gt;
		&lt;add key=&quot;PRIORITYOBJECTID&quot; value=&quot;5&quot; /&gt;
		&lt;add key=&quot;PROJECTATTACHMENTOBJECTID&quot; value=&quot;6&quot; /&gt;
		&lt;add key=&quot;PROJECTHISTORYOBJECTID&quot; value=&quot;7&quot; /&gt;
		&lt;add key=&quot;PROJECTOBJECTID&quot; value=&quot;8&quot; /&gt;
		&lt;add key=&quot;REPORTTYPEOBJECTID&quot; value=&quot;9&quot; /&gt;
		&lt;add key=&quot;STATUSTYPEOBJECTID&quot; value=&quot;10&quot; /&gt;
		&lt;add key=&quot;TASKATTACHMENTOBJECTID&quot; value=&quot;11&quot; /&gt;
		&lt;add key=&quot;TASKHISTORYOBJECTID&quot; value=&quot;12&quot; /&gt;
		&lt;add key=&quot;TASKOBJECTID&quot; value=&quot;13&quot; /&gt;
		&lt;add key=&quot;ALERTOBJECTID&quot; value=&quot;14&quot; /&gt;
		&lt;add key=&quot;ROLEOBJECTID&quot; value=&quot;15&quot; /&gt;
		&lt;add key=&quot;REGIONOBJECTID&quot; value=&quot;16&quot; /&gt;

		&lt;add key=&quot;INSERTACTIONID&quot; value=&quot;1&quot; /&gt;
		&lt;add key=&quot;DELETEACTIONID&quot; value=&quot;2&quot; /&gt;
		&lt;add key=&quot;UPDATEACTIONID&quot; value=&quot;3&quot; /&gt;
		&lt;add key=&quot;ARCHIVEACTIONID&quot; value=&quot;4&quot; /&gt;
		&lt;add key=&quot;RESTOREACTIONID&quot; value=&quot;5&quot; /&gt;
		&lt;add key=&quot;ERRORACTIONID&quot; value=&quot;6&quot; /&gt;

		<a id="ChartImageHandler" />&lt;add key=&quot;ChartImageHandler&quot; value=&quot;storage=file;timeout=20;dir=d:\Temp\;&quot; /&gt;
	&lt;/appSettings&gt;
	&lt;connectionStrings&gt;
		&lt;add name=&quot;ITDBConnectionString&quot; connectionString=&quot;Data Source=DNYSQLGEN3;Initial Catalog=ITDB;Persist Security Info=True;User ID=ITDB_RW;Password=[DataBasePassword]&quot; providerName=&quot;System.Data.SqlClient&quot; /&gt;
	&lt;/connectionStrings&gt;
	&lt;system.web&gt;
		&lt;compilation debug=&quot;true&quot;&gt;
			&lt;assemblies&gt;
				&lt;add assembly=&quot;System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35&quot; /&gt;
			&lt;/assemblies&gt;
		&lt;/compilation&gt;
		&lt;httpHandlers&gt;
			&lt;add path=&quot;ChartImg.axd&quot; verb=&quot;GET,HEAD&quot; type=&quot;System.Web.UI.DataVisualization.Charting.ChartHttpHandler, System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35&quot; validate=&quot;false&quot; /&gt;
		&lt;/httpHandlers&gt;
		&lt;authorization&gt;
			&lt;allow roles=&quot;TWNY\itdb_users&quot; /&gt;
			&lt;allow roles=&quot;TWNY\itdb_admins&quot; /&gt;
			&lt;deny users=&quot;*&quot; /&gt;
		&lt;/authorization&gt;
		&lt;identity impersonate=&quot;false&quot; /&gt;
		&lt;httpRuntime maxRequestLength=&quot;51200&quot; /&gt;
	&lt;/system.web&gt;
	&lt;location path=&quot;admin&quot;&gt;
		&lt;system.web&gt;
			&lt;authorization&gt;
				&lt;allow roles=&quot;TWNY\itdb_admins&quot; /&gt;
				&lt;deny users=&quot;*&quot; /&gt;
			&lt;/authorization&gt;
		&lt;/system.web&gt;
	&lt;/location&gt;
	&lt;location path=&quot;accessdenied.html&quot;&gt;
		&lt;system.web&gt;
			&lt;authorization&gt;
				&lt;allow users=&quot;*&quot; /&gt;
			&lt;/authorization&gt;
		&lt;/system.web&gt;
	&lt;/location&gt;
	&lt;location path=&quot;css&quot;&gt;
		&lt;system.web&gt;
			&lt;authorization&gt;
				&lt;allow users=&quot;*&quot; /&gt;
			&lt;/authorization&gt;
		&lt;/system.web&gt;
	&lt;/location&gt;
	&lt;location path=&quot;images&quot;&gt;
		&lt;system.web&gt;
			&lt;authorization&gt;
				&lt;allow users=&quot;*&quot; /&gt;
			&lt;/authorization&gt;
		&lt;/system.web&gt;
	&lt;/location&gt;
	&lt;location path=&quot;fonts&quot;&gt;
		&lt;system.web&gt;
			&lt;authorization&gt;
				&lt;allow users=&quot;*&quot; /&gt;
			&lt;/authorization&gt;
		&lt;/system.web&gt;
	&lt;/location&gt;
	&lt;location path=&quot;help&quot;&gt;
		&lt;system.web&gt;
			&lt;authorization&gt;
				&lt;allow users=&quot;*&quot; /&gt;
			&lt;/authorization&gt;
		&lt;/system.web&gt;
	&lt;/location&gt;
	&lt;system.webServer&gt;
		&lt;handlers&gt;
			&lt;remove name=&quot;ChartImageHandler&quot; /&gt;
			&lt;add name=&quot;ChartImageHandler&quot; preCondition=&quot;integratedMode&quot; verb=&quot;GET,HEAD&quot; path=&quot;ChartImg.axd&quot; type=&quot;System.Web.UI.DataVisualization.Charting.ChartHttpHandler, System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35&quot; /&gt;
		&lt;/handlers&gt;
		&lt;httpErrors&gt;
			&lt;remove statusCode=&quot;401&quot; subStatusCode=&quot;-1&quot; /&gt;
			&lt;error statusCode=&quot;401&quot; prefixLanguageFilePath=&quot;&quot; path=&quot;/accessdenied.html&quot; responseMode=&quot;ExecuteURL&quot; /&gt;
		&lt;/httpErrors&gt;
	&lt;/system.webServer&gt;
&lt;/configuration&gt;</pre>
    <h2>Proxy</h2>
    <p>To ensure connecttivity, the ITDB alias names should be excluded from the proxy.</p>
</asp:Content>