﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="LogEntryHelp.aspx.vb" Inherits="help_LogEntryHelp" title="Log Entries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Log Entries</h1>
    <p>ITDB allows users to attach Log Entries to Projects, Tasks and Issues.</p>
    <div class="callout" title="Don't log angry">Log Entries cannot be deleted or modified.</div>
    <h2>How to Create a Log Entry</h2>
    <ol>
        <li>Navigate to the Project, Task or Issue</li>
        <li>Select the <img src="/images/comment3_(add)_16x16.gif" alt="Create a new Log Entry" class="inlineicon" />&nbsp;<b>New Log</b> icon.<br />
        <img src="images/newlog.png" alt="New Log Dialog" /></li>
        <li>Enter the log in the field using the formatting tools provided<a href="#fn1" id="ref1" class="footnote">1</a>.</li>
        <li>Click <input type="button" value="Save" class="tradewebbutton" /></li>
    </ol>
    <hr />
    <sup id="fn1">1. The Log Entry editor uses the CKEditor ASP.NET Rich Text Editor.
    See
    <a href="http://docs.cksource.com/CKEditor_3.x/Users_Guide" target="_blank">http://docs.cksource.com/CKEditor_3.x/Users_Guide</a>
    for more information.
    <a href="#ref1" title="Jump back to footnote 1 in the text.">↩</a></sup>
</asp:Content>

