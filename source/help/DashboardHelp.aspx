﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="DashboardHelp.aspx.vb" Inherits="help_DashboardHelp" title="Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Dashboard</h1>
    <p>The Dashboard view presents a series of graphs to provide a quick overview of ITDB data.</p>
    <p><img src="images/dashboardview.png" alt="Dashboard View" /></p>
</asp:Content>
