﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="SQL.aspx.vb" Inherits="help_SQLHelp" title="SQL Structure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>SQL</h1>
    <h2>SQL Account</h2>
    <p>ITDB requires a SQL account &quot;ITDB_RW&quot; with dbowner permissions on the ITDB database.</p>
    <h2>SQL Configuration</h2>
    <p>The database must be configured to cache incidences between reboots.</p>
    <h2>SQL Structure</h2>
    <p>Below is an overview of the ITDB database schema</p>
    <asp:Image ID="Image1" runat="server" ImageUrl="~/help/images/ITDB.png" />
</asp:Content>

