﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="TaskHelp.aspx.vb" Inherits="help_TaskHelp" title="Tasks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Tasks</h1>
    <h2>How to Create a New Task</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select a project name from the Projects list.<br />
        <img src="images/projectgrid.png" alt="Projects List" /></li>
        <li>Click the <img src="/images/flag1_(add)_16x16.gif" class="inlineicon" alt="Create a New Task" />&nbsp<b>Create a New Task</b> icon.<br />
        <img src="images/newtask.png" alt="New Task" /></li>
        <li>Enter a <b>Task Name</b> and <b>Description</b>.</li>
        <li>Select an <b>Assignee</b> and <b>Priority</b>.</li>
        <li>Optionally specify a <b>Due Date</b> if required.</li>
        <li>Select the <b>Accomplishment</b> option if you wish that the task appear in the monthly accomplishment reports.
            <div class="callout" style="z-index: 10; position: relative; right: 280x">For a task to be an accomplishment it should uniquely represent
            a given activity and not overlap with any other efforts.</div>
        </li>
        <li>Click <input type="button" value="Save" class="tradewebbutton" /></li>
    </ol>
    <h2>How to Edit a Task</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select a project from the Projects list.<br />
        <img src="images/projectgrid.png" alt="Projects List" /></li>
        <li>Select a task from the Tasks list.<br />
        <img src="images/taskgrid.png" alt="Task List" /></li>
        <li>Click the <a href="">Edit</a> link.<br />
        <img src="images/viewtask.png" alt="View Task" /></li>
        <li>Edit each field as required and select the <a href="">Update</a>
        link to save changes.<br />
        <img src="images/edittask.png" alt="Edit Task"/><br />
        <p>The <b>Status</b>, <b>Percent Complete</b> and <b>Completed Date</b> fields
        are programatically linked. If you set <b>Status</b> to the value listed in the first column, the
        <b>Percent Complete</b> and <b>Completed Date</b> will be updated as follows...</p>
        <table border="1" style="border-collapse: collapse;">
            <thead>
                <tr>
                    <th>Status</th>
                    <th>Percent Complete</th>
                    <th>Completed Date</th>
                </tr>
            </thead>
            <tr>
                <td>Not Started</td>
                <td>0%</td>
                <td>Clears Value</td>
            </tr>
            <tr>
                <td>In Progress</td>
                <td>If the Value is 0% or 100%, sets the value to 50%, otherwise leave as is</td>
                <td>Clears Value</td>
            </tr>
            <tr>
                <td>Complete</td>
                <td>100%</td>
                <td>Date of Edit</td>
            </tr>
        </table>
        &nbsp;
        <div>If you set <b>Percent Complete</b> value, the <b>Status</b> and <b>Completed Date</b> are
        set as follows...</div>
        <table border="1" style="border-collapse: collapse;">
            <thead>
                <tr>
                    <th>Percent Complete</th>
                    <th>Status</th>
                    <th>Completed Date</th>
                </tr>
            </thead>
            <tr>
                <td>0%</td>
                <td>Not Started</td>
                <td>Clears Value</td>
            </tr>
            <tr>
                <td>1% - 99%</td>
                <td>In Progress</td>
                <td>Clears Value</td>
            </tr>
            <tr>
                <td>100%</td>
                <td>Complete</td>
                <td>Date of Edit</td>
            </tr>
        </table>
        &nbsp;
        <div>If you set <b>Completed Date</b> value, the <b>Status</b> and <b>Percent Complete</b> are
        set as follows...</div>
        <table border="1" style="border-collapse: collapse;">
            <thead>
                <tr>
                    <th>Completed Date</th>
                    <th>Status</th>
                    <th>Percent Complete</th>
                </tr>
            </thead>
            <tr>
                <td>Any Value</td>
                <td>Complete</td>
                <td>100%</td>
            </tr>
            <tr>
                <td>No Value</td>
                <td>If the value is &quot;<b>Complete</b>&quot; set to &quot;<b>In Progress</b>&quot;, otherwise leave as is</td>
                <td>If the value is 100%, set to 50%, otherwise leave as is</td>
            </tr>
        </table></li>
    </ol>
    <h2>How to Delete a Task</h2>
    <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB/</a>.</li>
        <li>Select a project from the Projects list.<br />
        <img src="images/projectgrid.png" alt="Projects List" /></li>
        <li>Select the <img src="/images/flag1_(delete)_16x16.gif" alt="Delete" class="inlineicon" />&nbsp;<b>Delete</b> icon.</li>
        <li>Click <input type="button" value="OK" style="width: 80px;" /> to confirm.<br />
        <img src="images/deletetask.png" alt="Delete task confirmation" style="margin: 5px 0px 0px 0px;" /></li>
    </ol>
    <h2>How to Add a Log Entry</h2>
    <p>Tasks support the creation of log entries. Log entries cannot be deleted.</p>
    <p>See <a href="LogEntryHelp.aspx">How to Create a Log Entry</a></p>
    <h2>How to Attach Files to a Task</h2>
    <p>You can upload and attach files to a task. NOTE: the maximum files size is 50MB.</p>
    <p>See <a href="Attachments.aspx">How to Upload a File Attachment</a></p>
</asp:Content>

