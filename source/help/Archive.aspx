﻿<%@ Page Language="VB" MasterPageFile="~/help/help.master" AutoEventWireup="false" CodeFile="Archive.aspx.vb" Inherits="help_Archive" title="Archive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Archive</h1>
    <p>Projects in ITDB are never deleted, rather they are hidden from all views and reports. &#39;Deleted&#39
    projects are viewable from the Administration Archive view.</p>
    <p><img src="images/archive.png" alt="Archived Projects view" /></p>
    <h2>How To Restore a Deleted Project</h2>
   <ol>
        <li>Navigate to <a href="http://ITDB/">http://ITDB</a>.</li>
        <li>Select the <img src="../images/settings1_16x16.gif" class="inlineicon" alt="Settings" />&nbsp;<b>Settings</b> icon</li>
        <li>Select the <b>Archive</b> tab.</li>
        <li>Click the <img src="/images/trash_(restore)_16x16.gif" alt="Restore" class="inlineicon" />&nbsp;<b>Restore</b>
        icon next to the project you with to restore.</li>
        <li>Click <input type="button" value="OK" style="width: 80px;" /> to confirm.<br />
        <img src="images/restoreconfirm.png" alt="Restore project confirmation" style="margin: 5px 0px 0px 0px;" /></li>
    </ol>
</asp:Content>