﻿
Partial Class help
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        ' Code to handle browser specific CSS.
        ' NOTE: IE 10+ no longer support CSS rules <!--[if IE x]>
        Dim strBrowser = Request.Browser.Browser.ToString()
        Dim strBrowserVersion = Request.Browser.MajorVersion.ToString()
        Dim css As New HtmlLink()

        Select Case strBrowser
            Case "Chrome" ' NOTE: Also applies to Opera Browser
                css.Href = ResolveClientUrl("/css/chrome.css")
                css.Attributes("rel") = "stylesheet"
                css.Attributes("type") = "text/css"
                css.Attributes("media") = "all"
                Page.Header.Controls.Add(css)
            Case "Firefox"
                css.Href = ResolveClientUrl("/css/firefox.css")
                css.Attributes("rel") = "stylesheet"
                css.Attributes("type") = "text/css"
                css.Attributes("media") = "all"
                Page.Header.Controls.Add(css)
            Case "Safari"
                css.Href = ResolveClientUrl("/css/safari.css")
                css.Attributes("rel") = "stylesheet"
                css.Attributes("type") = "text/css"
                css.Attributes("media") = "all"
                Page.Header.Controls.Add(css)
            Case "IE", "InternetExplorer"
                Select Case strBrowserVersion
                    Case "7"
                        css.Href = ResolveClientUrl("/css/ie7.css")
                        css.Attributes("rel") = "stylesheet"
                        css.Attributes("type") = "text/css"
                        css.Attributes("media") = "all"
                        Page.Header.Controls.Add(css)
                    Case "8"
                        css.Href = ResolveClientUrl("/css/ie8.css")
                        css.Attributes("rel") = "stylesheet"
                        css.Attributes("type") = "text/css"
                        css.Attributes("media") = "all"
                        Page.Header.Controls.Add(css)
                    Case "9"
                        css.Href = ResolveClientUrl("/css/ie9.css")
                        css.Attributes("rel") = "stylesheet"
                        css.Attributes("type") = "text/css"
                        css.Attributes("media") = "all"
                        Page.Header.Controls.Add(css)
                    Case "10"
                        css.Href = ResolveClientUrl("/css/ie10.css")
                        css.Attributes("rel") = "stylesheet"
                        css.Attributes("type") = "text/css"
                        css.Attributes("media") = "all"
                        Page.Header.Controls.Add(css)
                    Case "11"
                        css.Href = ResolveClientUrl("/css/ie11.css")
                        css.Attributes("rel") = "stylesheet"
                        css.Attributes("type") = "text/css"
                        css.Attributes("media") = "all"
                        Page.Header.Controls.Add(css)
                End Select
        End Select
    End Sub

    Protected Sub TreeView1_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeView1.SelectedNodeChanged
        Response.Redirect(TreeView1.SelectedValue, False)
    End Sub
End Class

