﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="IssueDetails.aspx.vb"
    Inherits="Issues" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar" style="padding: 3px 0px;">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
        <span class="menubarbutton">
            <asp:ImageButton ID="imbback" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/BranchRelationshipParent_16x.png" AlternateText="Back to parent project" CausesValidation="false"/>
            <asp:Label ID="lblback" runat="server" Text="Parent" AssociatedControlID="imbback" CssClass="menutext"
                ToolTip="Back to parent project" />
        </span>
		<span class="menubarbutton">
			<asp:ImageButton ID="imbMyView" runat="server" ImageAlign="absMiddle"
				ImageUrl="~/images/TaskList_16x.png" AlternateText="View all your tasks and issues" ToolTip="View all your tasks and issues"
				CausesValidation="false" />
			<asp:Label ID="lblMyView" runat="server" Text="My Tasks" AssociatedControlID="imbMyView" CssClass="menutext"
                ToolTip="View all your tasks and issues" />
		</span>
        <span class="menubarbutton">
            <asp:ImageButton ID="imbnewIssuelog" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Comment_16x.png" AlternateText="Create a new log entry" CausesValidation="false" Height="16px"/>
            <asp:Label ID="lblnewIssuelog" runat="server" Text="New Log" AssociatedControlID="imbnewIssuelog" CssClass="menutext"
                ToolTip="Create a new log entry" />
        </span>
    </div>
    <div class="subTitle">Issue Details</div>

    <asp:DetailsView ID="DetailsView1" runat="server"
        DataKeyNames="ID"
        DataSourceID="Issue_Data"
        AutoGenerateRows="False"
        AutoGenerateEditButton="True"
        CssClass="detailsView" RowStyle-Height="23">
        <FieldHeaderStyle CssClass="detailsView_header" />
        <AlternatingRowStyle CssClass="gridView_text_alternate" />
        <Fields>
            <asp:TemplateField HeaderText="Project">
                <ItemTemplate>
                    <div style="width:820px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;">
                        <asp:Label ID="lblProject" runat="server" Text='<%# Bind("Project") %>' />
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Issue">
                <EditItemTemplate>
                    <asp:TextBox ID="txtIssue" runat="server" Text='<%# Bind("Issue")%>' CssClass="gridView_TextBox" MaxLength="128" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblIssue" runat="server" Text='<%# Bind("Issue")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="txtIssueDescription" runat="server" TextMode="MultiLine" Text='<%# Bind("Description") %>'
                        Height="100" CssClass="gridView_TextBox" MaxLength="4000" />
                </EditItemTemplate>
                <ItemTemplate>
                    <div style="word-wrap: normal; overflow-y: auto; height: 100px;">
                        <%#Eval("Description")%>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Priority">
                <EditItemTemplate>
                    <asp:DropDownList id="ddlPriority" runat="server"
                        CSSClass="gridView_TextBox"
                        DataTextField="Priority"
                        DataValueField="ID"
                        DataSourceID="Priority_Data"
                        SelectedValue='<%#Eval("Priority_ID")%>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPriority" runat="server" Text='<%# Bind("Priority")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status">
                <EditItemTemplate>
                    <asp:DropDownList id="ddlStatus" runat="server"
                        CSSClass="gridView_TextBox"
                        DataTextField="Status"
                        DataValueField="ID"
                        DataSourceID="Status_Data"
                        SelectedValue='<%#Eval("Status_ID")%>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Owner">
                <EditItemTemplate>
                    <asp:DropDownList id="ddlEditOwner" runat="server"
                        CSSClass="gridView_TextBox"
                        DataTextField="full_name"
                        DataValueField="ID"
                        DataSourceID="People_Data"
                        SelectedValue='<%#Eval("Person_ID")%>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblOwner" runat="server" Text='<%# Bind("full_name")%>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>
    </asp:DetailsView>
    
    <div style="line-height: 22px;">&nbsp;</div>
    <div class="subTitle">History</div>

    <asp:UpdatePanel ID="upIssueLog" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="IssueLog_Grid" runat="server"
                AutoGenerateColumns="False"
                DataKeyNames="ID"
                DataSourceID="Issue_History_Data"
                AllowPaging="true" CssClass="gridView" EmptyDataText="There are no log entries associated with this Issue."
                PageSize="3" PagerSettings-Mode="Numeric">
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <Columns>
                    <asp:BoundField DataField="Date" HeaderText="Date" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="190" />
                    <asp:TemplateField HeaderText="Entry">
                        <ItemTemplate>
                            <div style="width: 1403px; max-height: 300px; word-wrap: normal; overflow: auto;">
								<%#Eval("log_entry")%>
							</div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel id="pnlNewIssueLog" runat="server" CssClass="modalpopup"
        Height="418px" Width="760px" style="display: none;" DefaultButton="btnSaveIssueLog">
        <div id="newpIssuelogtitle" class="popuptitle">New Log</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td>
                    <CKEditor:CKEditorControl ID="ceNewIssueLogEntry" runat="server"
                        Height="235" ResizeEnabled="false" BorderWidth="0"
                        Toolbar="Source|-|Cut|Copy|Paste|PasteText|PasteFromWord|-|Undo|Redo|-|Bold|Italic|Underline|Strike|Subscript|Superscript|-|NumberedList|BulletedList|-|Outdent|Indent|-|JustifyLeft|justifyCenter|JustifyRight|-|Link|Unlink
                        /
                    	Format|Font|FontSize" Skin="office2003"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align:center">
                    <asp:Button ID="btnSaveIssueLog" runat="server" Text="Save" CssClass="tradewebbutton" ValidationGroup="newIssuelog" />
                    &nbsp;&nbsp;
                    <asp:button ID="btnCancelIssueLog" runat="server" Text="Cancel" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:SqlDataSource ID="Issue_History_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Issue_History WHERE Issue_ID = @IssueID ORDER BY [Date] DESC">
        <SelectParameters>
            <asp:QueryStringParameter Name="IssueID" QueryStringField="IssueID" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Issue_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM IssueDetails WHERE ID = @IssueID"
        UpdateCommand="UPDATE Issues SET Issue = @Issue, Description = @Description, Person_ID = @PersonID, Status_ID = @StatusID, Priority_ID = @PriorityID WHERE ID = @IssueID">
        <SelectParameters>
            <asp:QueryStringParameter Name="IssueID" QueryStringField="IssueID" />
        </SelectParameters>
        <UpdateParameters>
            <asp:QueryStringParameter Name="IssueID" QueryStringField="IssueID" />
            <asp:Parameter Name="Issue" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Percent_Complete" Type="int32" />
            <asp:ControlParameter Name="PersonID" Type="Int32" ControlID="ctl00$ContentPlaceHolder1$DetailsView1$ddlEditOwner"
                PropertyName="SelectedValue" />
            <asp:ControlParameter Name="StatusID" Type="Int32" ControlID="ctl00$ContentPlaceHolder1$DetailsView1$ddlStatus"
                PropertyName="SelectedValue" />
            <asp:ControlParameter Name="PriorityID" Type="Int32" ControlID="ctl00$ContentPlaceHolder1$DetailsView1$ddlPriority"
                PropertyName="SelectedValue" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="People_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM People ORDER BY full_name">
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Priority_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Priority_Types">
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Status_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Status_Types">
    </asp:SqlDataSource>

    <cc1:ModalPopupExtender ID="mpeNewIssueLog" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewIssuelog"
        PopupControlID="pnlNewIssueLog"
        CancelControlID="btnCancelIssueLog"
        DropShadow="true"
        PopupDragHandleControlID="newIssuelogtitle" />

    <asp:ValidationSummary ID="vsIssueLog" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newIssuelog" />
</asp:Content>

