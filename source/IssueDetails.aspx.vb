﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Data

Partial Class Issues
    Inherits System.Web.UI.Page
    Dim intProjectID As Int32

    Protected Sub imbhome_Click(sender As Object, e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub imbback_Click(sender As Object, e As ImageClickEventArgs) Handles imbback.Click
        Response.Redirect("ProjectDetails.aspx?ProjectID=" & intProjectID)
    End Sub

    Protected Sub imbMyView_Click(sender As Object, e As ImageClickEventArgs) Handles imbMyView.Click
        If Len(Request.ServerVariables("LOGON_USER")) > 0 Then
            Dim intUserID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & Request.ServerVariables("LOGON_USER") & "'")
            Response.Redirect("/MyView.aspx?PersonID=" & intUserID, False)
        End If
    End Sub

    Protected Sub btnSaveIssueLog_Click(sender As Object, e As EventArgs) Handles btnSaveIssueLog.Click
        If Not Page.IsValid Then Exit Sub

        Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
            "INSERT INTO Issue_History (Log_Entry, Date, Issue_ID) VALUES ('" & ceNewIssueLogEntry.Text.Replace("'", "''").Trim() & "', '" & Now() & "', '" & Request.QueryString("IssueID") & "'); SELECT SCOPE_IDENTITY();")
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("ISSUEHISTORYOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            "" _
        )

        upIssueLog.Update()
        IssueLog_Grid.DataBind()
        ceNewIssueLogEntry.Text = ""
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        intProjectID = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT Project_ID FROM Issues WHERE ID = " & Request.QueryString("IssueID"))
    End Sub

    Protected Sub DetailsView1_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles DetailsView1.ItemUpdated
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("ISSUEOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
            e.OldValues("Issue").ToString() _
        )
    End Sub
End Class
