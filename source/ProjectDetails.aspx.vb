﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Data

Partial Class Tasks
    Inherits System.Web.UI.Page

    Protected Sub imbhome_Click(sender As Object, e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub imbMyView_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbMyView.Click
        If Len(Request.ServerVariables("LOGON_USER")) > 0 Then
            Dim intUserID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & Request.ServerVariables("LOGON_USER") & "'")
            Response.Redirect("/MyView.aspx?PersonID=" & intUserID, False)
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Me.Page.IsPostBack Then
            Dim intUserID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & Request.ServerVariables("LOGON_USER") & "'")
            ' Disable controls if project is archived
            Dim bolProjectArchived As Boolean = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT archive FROM Projects WHERE ID = " & Request.QueryString("ProjectID"))
            If bolProjectArchived = True Then
                archivewarning.Visible = True
                DetailsView1.Enabled = False
                imbnewtask.Enabled = False
                imbnewissue.Enabled = False
                imbnewprojectlog.Enabled = False
                imbnewprojectattachment.Enabled = False
                Tasks_Grid.Enabled = False
                Issues_Grid.Enabled = False
                ProjectLog_Grid.Enabled = False
                Project_Attachments_Grid.Enabled = False
            End If

            'populate dropdownlists
            ddlTaskAssignedTo.DataSource = common.fGetPeopleEnabled()
            ddlTaskAssignedTo.DataTextField = "full_name"
            ddlTaskAssignedTo.DataValueField = "ID"
            ddlTaskAssignedTo.DataBind()
            ddlTaskAssignedTo.Items.Insert(0, ConfigurationManager.AppSettings("TASKOWNERMENUDEFAULT"))
            ddlTaskAssignedTo.SelectedValue = intUserID

            ddlIssueAssignedTo.DataSource = common.fGetPeopleEnabled()
            ddlIssueAssignedTo.DataTextField = "full_name"
            ddlIssueAssignedTo.DataValueField = "ID"
            ddlIssueAssignedTo.DataBind()
            ddlIssueAssignedTo.Items.Insert(0, ConfigurationManager.AppSettings("ISSUEOWNERMENUDEFAULT"))
            ddlIssueAssignedTo.SelectedValue = intUserID

            ddlTaskPriority.DataSource = common.fGetPriorities()
            ddlTaskPriority.DataTextField = "Priority"
            ddlTaskPriority.DataValueField = "ID"
            ddlTaskPriority.DataBind()
            ddlTaskPriority.Items.Insert(0, ConfigurationManager.AppSettings("PRIORITYMENUDEFAULT"))
            ddlTaskPriority.SelectedValue = ConfigurationManager.AppSettings("NORMALPRIORITYINDEX")

            ddlIssuePriority.DataSource = common.fGetPriorities()
            ddlIssuePriority.DataTextField = "Priority"
            ddlIssuePriority.DataValueField = "ID"
            ddlIssuePriority.DataBind()
            ddlIssuePriority.Items.Insert(0, ConfigurationManager.AppSettings("PRIORITYMENUDEFAULT"))
            ddlIssuePriority.SelectedValue = ConfigurationManager.AppSettings("NORMALPRIORITYINDEX")
        End If
    End Sub

    Protected Sub btnSaveTask_Click(sender As Object, e As EventArgs) Handles btnSaveTask.Click
        If Not Page.IsValid Then Exit Sub
        'Dim intUserID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & Request.ServerVariables("LOGON_USER") & "'")

        Dim intID As Int32
        If Len(txtDueDate.Text) > 0 Then
            intID = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
                "INSERT INTO Tasks (Task, Description, Person_ID, Priority_ID, Start_Date, Due_Date, Project_ID, Status_ID, achievement) VALUES ('" & txtTaskName.Text.Replace("'", "''").Trim() & "', '" & txtTaskDescription.Text.Replace("'", "''").Trim() & "', '" & ddlTaskAssignedTo.SelectedItem.Value & "', '" & ddlTaskPriority.SelectedItem.Value & "', '" & Now() & "', '" & txtDueDate.Text & "', '" & Request.QueryString("ProjectID") & "', '1', '" & chkAccomplishment.Checked & "'); SELECT SCOPE_IDENTITY();")
        Else
            intID = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
                "INSERT INTO Tasks (Task, Description, Person_ID, Priority_ID, Start_Date, Project_ID, Status_ID, achievement) VALUES ('" & txtTaskName.Text.Replace("'", "''").Trim() & "', '" & txtTaskDescription.Text.Replace("'", "''").Trim() & "', '" & ddlTaskAssignedTo.SelectedItem.Value & "', '" & ddlTaskPriority.SelectedItem.Value & "', '" & Now() & "', '" & Request.QueryString("ProjectID") & "', '1', '" & chkAccomplishment.Checked & "'); SELECT SCOPE_IDENTITY();")
        End If

        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("TASKOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            txtTaskName.Text.Replace("'", "''").Trim() _
        )

        upTasks.Update()
        Tasks_Grid.DataBind()
        txtTaskName.Text = ""
        txtTaskDescription.Text = ""
        'ddlTaskAssignedTo.SelectedIndex = intUserID
        'ddlTaskPriority.SelectedIndex = ConfigurationManager.AppSettings("NORMALPRIORITYINDEX")
        txtDueDate.Text = ""

        mpeNewTask.Show()
    End Sub

    Protected Sub btnSaveIssue_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveIssue.Click
        If Not Page.IsValid Then Exit Sub
        'Dim intUserID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & Request.ServerVariables("LOGON_USER") & "'")

        Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
            "INSERT INTO Issues (Issue, Description, Person_ID, Priority_ID, Project_ID, Status_ID) VALUES ('" & txtIssueName.Text.Replace("'", "''").Trim() & "', '" & txtIssueDescription.Text.Replace("'", "''").Trim() & "', '" & ddlIssueAssignedTo.SelectedItem.Value & "', '" & ddlIssuePriority.SelectedItem.Value & "', '" & Request.QueryString("ProjectID") & "', '1'); SELECT SCOPE_IDENTITY();")
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("ISSUEOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            txtIssueName.Text.Replace("'", "''").Trim() _
        )

        upIssues.Update()
        Issues_Grid.DataBind()
        txtIssueName.Text = ""
        txtIssueDescription.Text = ""
        'ddlIssuePriority.SelectedValue = ConfigurationManager.AppSettings("NORMALPRIORITYINDEX")
        'ddlIssueAssignedTo.SelectedIndex = intUserID

        mpeNewIssue.Show()
    End Sub

    Protected Sub btnSaveProjectLog_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveProjectLog.Click
        If Not Page.IsValid Then Exit Sub

        Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
            "INSERT INTO Project_History (Log_Entry, Date, Project_ID) VALUES ('" & ceNewProjectLogEntry.Text.Replace("'", "''").Trim() & "', '" & Now() & "', '" & Request.QueryString("ProjectID") & "'); SELECT SCOPE_IDENTITY();")
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("PROJECTHISTORYOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            "" _
        )

        upProjectLog.Update()
        ProjectLog_Grid.DataBind()
        ceNewProjectLogEntry.Text = ""
    End Sub

    Protected Sub btnSaveNewProjectAttachment_Click(sender As Object, e As EventArgs) Handles btnSaveNewProjectAttachment.Click
        ' This section was built using the method found here: http://www.dotnettutorials.com/tutorials/database/upload-files-sql-database-cs/
        ' DEVELOPER'S NOTE: web.config may need edit is file uploads are timing out in prod, see http://docs.telerik.com/devtools/aspnet-ajax/controls/upload/uploading-files/uploading-large-files
        If Me.Page.IsPostBack Then
            If Not Page.IsValid Then Exit Sub

            If fuProjectAttachment.HasFile Then
                If fuProjectAttachment.PostedFile.ContentLength() <= Convert.ToInt64(ConfigurationManager.AppSettings("MAXFILESIZE")) Then
                    Using Conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString)
                        Try
                            Const SQL As String = "INSERT INTO [Project_Attachments] (Project_ID, File_Name, Description, MIMEType, FileSize, FileData) VALUES (@Project_ID, @File_Name, @Description, @MIMEType, @FileSize, @FileData)"
                            Dim cmd As New SqlCommand(SQL, Conn)

                            cmd.Parameters.AddWithValue("@Project_ID", Request.QueryString("ProjectID"))
                            cmd.Parameters.AddWithValue("@File_Name", fuProjectAttachment.FileName.ToString())
                            cmd.Parameters.AddWithValue("@MIMEType", fuProjectAttachment.PostedFile.ContentType.ToString())
                            cmd.Parameters.AddWithValue("@Description", txtProjectattachmentDescription.Text)
                            cmd.Parameters.AddWithValue("@FileSize", fuProjectAttachment.PostedFile.ContentLength())

                            Dim bytes As Byte() = New Byte(fuProjectAttachment.PostedFile.InputStream.Length) {}
                            fuProjectAttachment.PostedFile.InputStream.Read(bytes, 0, bytes.Length)
                            cmd.Parameters.AddWithValue("@FileData", bytes)

                            Conn.Open()
                            cmd.ExecuteNonQuery()
                            Conn.Close()
                        Catch sqlEx As SqlException
                            ' bad upload
                            Conn.Close()
                        Finally
                            upAttachments.Update()
                            Project_Attachments_Grid.DataBind()
                        End Try
                    End Using
                Else
                    ' File too big
                End If
                fuProjectAttachment = New FileUpload
                txtProjectattachmentDescription.Text = ""
            End If
        End If
    End Sub

    Protected Sub Project_Attachments_Grid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles Project_Attachments_Grid.RowCommand
        If e.CommandName = "downloadprojectattachment" Then
            Dim objSQLConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString)
            Dim objSQLCommand As New SqlCommand("SELECT FileSize, File_Name, FileData, MIMEType FROM [Project_Attachments] WHERE ID = " & e.CommandArgument)
            objSQLCommand.Connection = objSQLConnection
            objSQLConnection.Open()
            Dim objSQLDataReader As SqlDataReader = objSQLCommand.ExecuteReader()
            objSQLDataReader.Read()
            Dim FileData As Byte() = DirectCast(objSQLDataReader("FileData"), [Byte]())
            Dim MIMEType As String = objSQLDataReader("MIMEType").ToString()
            Dim File_Name As String = objSQLDataReader("File_Name").ToString()
            Dim FileSize As String = objSQLDataReader("FileSize").ToString()
            objSQLConnection.Close()
            Response.Clear()
            Response.Buffer = False
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = MIMEType
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + File_Name)
            Response.AppendHeader("Content-Length", FileSize.ToString())
            Response.OutputStream.Write(FileData, 0, FileData.Length)
            Response.Flush()
            Response.End()
        End If
    End Sub

    Protected Sub Tasks_Grid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles Tasks_Grid.RowCommand
        Dim TaskID As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "viewtask" Then
            Response.Redirect("TaskDetails.aspx?TaskID=" & TaskID, False)
        End If
    End Sub

    Protected Sub Tasks_Grid_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles Tasks_Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ib As ImageButton = DirectCast(e.Row.Cells(9).Controls(0), ImageButton)
            ib.OnClientClick = "if (!confirm('Are you sure you want to delete this task and all log entries and attachments?')) return;"
        End If
    End Sub

    Protected Sub Project_Attachments_Grid_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles Project_Attachments_Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ib As ImageButton = DirectCast(e.Row.Cells(2).Controls(0), ImageButton)
            ib.OnClientClick = "if (!confirm('Are you sure you want to delete this file?')) return;"
        End If
    End Sub

    Protected Sub Issues_Grid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles Issues_Grid.RowCommand
        Dim IssueID As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "viewissue" Then
            Response.Redirect("IssueDetails.aspx?IssueID=" & IssueID, False)
        End If
    End Sub

    Protected Sub Issues_Grid_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles Issues_Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ib As ImageButton = DirectCast(e.Row.Cells(4).Controls(0), ImageButton)
            ib.OnClientClick = "if (!confirm('Are you sure you want to delete this issue and all log entries?')) return;"
        End If
    End Sub

    Protected Sub Tasks_Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Tasks_Grid.RowDeleted
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("TASKOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("DELETEACTIONID")), _
            e.Values("Task").ToString() _
        )
    End Sub

    Protected Sub Issues_Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Issues_Grid.RowDeleted
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("ISSUEOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("DELETEACTIONID")), _
            e.Values("Issue").ToString() _
        )
    End Sub

    Protected Sub Project_Attachments_Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Project_Attachments_Grid.RowDeleted
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("PROJECTATTACHMENTOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("DELETEACTIONID")), _
            e.Values("File_Name").ToString() _
        )
    End Sub

    Protected Sub DetailsView1_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles DetailsView1.ItemUpdated
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("PROJECTOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
            e.OldValues("Project").ToString() _
        )
    End Sub

    Protected Sub btnCancelTask_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTask.Click
        txtTaskName.Text = ""
        txtTaskDescription.Text = ""
        txtDueDate.Text = ""
    End Sub

    Protected Sub btnCancelIssue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelIssue.Click
        txtIssueName.Text = ""
        txtIssueDescription.Text = ""
    End Sub
End Class
