﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Projects" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
		<div style="float:left;">
			<span class="menubarbutton">
				<asp:ImageButton ID="imbnewproject" runat="server" ImageAlign="absMiddle"
					ImageUrl="~/images/Add_16x.png" AlternateText="Create a new project" ToolTip="Create a new project"
                    CausesValidation="false" />
                <asp:Label ID="lblNewProject" runat="server" Text="New Project" AssociatedControlID="imbnewproject" CssClass="menutext"
                    ToolTip="New Project" />
			</span>
			<span class="menubarbutton">
				<asp:ImageButton ID="imbexport" runat="server" ImageAlign="absMiddle"
					ImageUrl="~/images/ExcelWorksheetView_16x.png" AlternateText="Export to Excel" ToolTip="Export to Excel"
					CausesValidation="false" />
                <asp:Label ID="lblExport" runat="server" Text="Export" AssociatedControlID="imbexport" CssClass="menutext"
                    ToolTip="Export to Excel" />
			</span>
           	<span class="menubarbutton">
				<asp:ImageButton ID="imbMyView" runat="server" ImageAlign="absMiddle"
					ImageUrl="~/images/TaskList_16x.png" AlternateText="View all your tasks and issues"
					ToolTip="View all your tasks and issues"
                    CausesValidation="false" />
                <asp:Label ID="lblMyView" runat="server" Text="My Tasks" AssociatedControlID="imbMyView" CssClass="menutext"
                    ToolTip="View all your tasks and issues" />
			</span>
           	<span class="menubarbutton">
				<asp:ImageButton ID="imbReports" runat="server" ImageAlign="absMiddle"
					ImageUrl="~/images/Chart_16x.png" AlternateText="View reports" ToolTip="View reports"
                    CausesValidation="false" />
                <!--<asp:Label ID="lblReports" runat="server" Text="Reports" AssociatedControlID="imbReports" CssClass="menutext"
                    ToolTip="View reports" />-->
			</span>
           	<span class="menubarbutton">
				<asp:ImageButton ID="imbDashboard" runat="server" ImageAlign="absMiddle"
					ImageUrl="~/images/Report_16x.png" AlternateText="View Dashboard" ToolTip="View Dashboard"
                    CausesValidation="false" />
                <!--<asp:Label ID="lblDashboard" runat="server" Text="Dashboard" AssociatedControlID="imbDashboard" CssClass="menutext"
                    ToolTip="View Dashboard" />-->
			</span>
           	<span class="menubarbutton" style="position: relative;">
				<asp:ImageButton ID="imbSearch" runat="server" ImageAlign="absMiddle"
					ImageUrl="~/images/AddFilter_16x.png" AlternateText="Apply Filter" ToolTip="Apply Filter"
                    CausesValidation="false" Visible="true" Enabled="true" />
                <!--<asp:Label ID="lblSearch" runat="server" Text="Search" AssociatedControlID="imbSearch" CssClass="menutext"
                    ToolTip="Search Projects, Tasks and Issues" />-->
                <asp:UpdatePanel ID="upSearch" runat="server" Visible="false" RenderMode="Inline" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Panel runat="server" DefaultButton="btnHidden1">
                            <asp:TextBox ID="txtSearchBox" runat="server" Width="200" AutoCompleteType="Search" />
                            <asp:Button ID="btnHidden1" runat="server" style="display:none;" CausesValidation="false" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
			</span>
			<span class="menubarbutton">
				<asp:ImageButton ID="imbsettings" runat="server" ImageAlign="absMiddle"
					ImageUrl="~/images/Settings_16x.png" AlternateText="Go to Settings" ToolTip="Go to Settings"
					CausesValidation="false" />
               <!--<asp:Label ID="lblsettings" runat="server" Text="Settings" AssociatedControlID="imbsettings" CssClass="menutext"
                    ToolTip="Go to Settings" />-->
			</span>
			<span class="menubarbutton">
				<asp:ImageButton ID="imbHelp" runat="server" ImageAlign="absMiddle"
					ImageUrl="~/images/StatusHelp_gray_16x.png" AlternateText="Go to Help" ToolTip="Go to Help"
					CausesValidation="false" />
               <!--<asp:Label ID="lblHelp" runat="server" Text="Help" AssociatedControlID="imbHelp" CssClass="menutext"
                    ToolTip="Go to Help" />-->
			</span>
		</div>
		<div style="float:right;">
			<span class="menubarbutton">
				<asp:CheckBox ID="chkShowComplete" runat="server" ToolTip="Show Completed Projects" Text="Completed"
				    TextAlign="Left" AutoPostBack="true" />
			</span>
			<span class="menubarbutton">
				<asp:Label ID="lblRegionsFilter" runat="server" Text="Region" AssociatedControlID="ddlRegionsFilter"
					 CssClass="menutext" ToolTip="Filter by Region" />
				<asp:DropDownList ID="ddlRegionsFilter" runat="server" ToolTip="Filter by Region" AutoPostBack="true" />
			</span>
			<span class="menubarbutton">
				<asp:Label ID="lblOwnerFilter" runat="server" Text="Owner" AssociatedControlID="ddlOwnerFilter"
					 CssClass="menutext" ToolTip="Filter by Owner" />
				<asp:DropDownList ID="ddlOwnerFilter" runat="server" ToolTip="Filter by Owner" AutoPostBack="true" />
			</span>
			<span class="menubarbutton">
				<asp:Label ID="lblBragFilter" runat="server" Text="BRAG" AssociatedControlID="ddlBragFilter"
					 CssClass="menutext" ToolTip="Filter By Brag" />
				<asp:DropDownList ID="ddlBragFilter" runat="server" ToolTip="Filter By Brag" AutoPostBack="true" >
					<asp:ListItem Text="All" Value="%" Selected="True" />
					<asp:ListItem Text="RED" Value="RED" />
					<asp:ListItem Text="AMBER" Value="AMBER" />
					<asp:ListItem Text="GREEN" Value="GREEN" />
					<asp:ListItem Text="BLUE" Value="BLUE" />
				</asp:DropDownList>
            </span>
           	<span class="menubarbutton">
				<asp:Label ID="lblCategoriesFilter" runat="server" Text="Category" AssociatedControlID="ddlCategoriesFilter"
					 CssClass="menutext" ToolTip="Filter by Category" />
				<asp:DropDownList ID="ddlCategoriesFilter" runat="server" ToolTip="Filter by Category" AutoPostBack="true" />
			</span>
			<span class="menubarbutton">
				<asp:Label ID="lblPlatformsFilter" runat="server" Text="Platform" AssociatedControlID="ddlPlatformsFilter"
					 CssClass="menutext" ToolTip="Filter by Platform" />
				<asp:DropDownList ID="ddlPlatformsFilter" runat="server" ToolTip="Filter by Platform" AutoPostBack="true" />
			</span>
			<span class="menubarbutton">
				<asp:DropDownList ID="ddlPageSize" runat="server" ToolTip="Set the number of rows to return per page"
				    AutoPostBack="true">
					<asp:ListItem Text="10" Value="10" />
					<asp:ListItem Text="20" Value="20" />
					<asp:ListItem Text="50" Value="50" Selected="True" />
					<asp:ListItem Text="100" Value="100" />
				</asp:DropDownList>
				<asp:Label ID="lblPageSize" runat="server" Text="Page Size" AssociatedControlID="ddlPageSize"
					 CssClass="menutext" ToolTip="Set the number of rows to return per page" />
			</span>
		</div>
    </div>
    <div class="subTitle" style="clear: both;">Projects</div>
    <asp:UpdatePanel ID="upProjects" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="Projects_Grid" runat="server" CssClass="gridView"
                AutoGenerateColumns="False"
                DataKeyNames="ID"
                DataSourceID="Projects_data"
                AllowPaging="true"
                EmptyDataText="There are no projects defined." AllowSorting="True" PageSize="50"
                PagerSettings-Mode="Numeric" EnableViewState="true">
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" ItemStyle-Width="60" />
                    <asp:TemplateField HeaderText="Project" SortExpression="Project">
                        <ItemTemplate>
                            <div style="width:895px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;">
                                <asp:LinkButton ID="btnviewproject" runat="server"
                                    CommandName="viewproject"
                                    CommandArgument='<%#Bind("ID") %>'
                                    Text='<%#Bind("Project") %>'
                                    CausesValidation="false" CssClass="gridviewlinkbutton" />
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Region" HeaderText="Admin Region" SortExpression="Region" ItemStyle-Width="160" />
                    <asp:TemplateField HeaderText="Category" SortExpression="Category">
                        <ItemTemplate>
                            <div style="width: 160px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                <asp:Label ID="lblGridCategory" runat="server" Text='<%#Bind("Category") %>' />
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
					<asp:BoundField DataField="Platform" HeaderText="Platform" SortExpression="Platform" ItemStyle-Width="160" />
                    <asp:BoundField DataField="full_name" HeaderText="Owner" SortExpression="full_name" ItemStyle-Width="160" />
                    <asp:CheckBoxField DataField="BaU" HeaderText="BaU" SortExpression="BaU" ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center" Visible="false" />
                    <asp:BoundField DataField="Brag" HeaderText="BRAG" SortExpression="Brag"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80" />
                    <asp:TemplateField ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center" Visible="true">
                        <ItemTemplate>
                            <asp:imageButton ID="imbArchiveProject" runat="server"
                                ImageUrl="~/images/delete_16.png"
                                ImageAlign="Middle"
                                CommandName="archive"
                                CommandArgument='<%#Bind("ID") %>'
                                AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete"
                                OnClientClick="return confirm('Are you sure you want to delete this project?');" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel id="pnlNewProject" runat="server" CssClass="modalpopup"
        Height="490px" Width="426px" style="display: none;" DefaultButton="btnSaveProject">
        <div id="newprojecttitle" class="popuptitle">New Project</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:175px;" class="popuplabel">
                    <asp:Label id="lblprojectname" runat="server"
                        Text="Project Name"
                        AssociatedControlID="txtProjectName" />
                </td>
                <td style="width:10px;"></td>
                <td style="width:215px;">
                    <asp:TextBox ID="txtProjectName" runat="server" Width="195" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvProjectName" runat="server" ControlToValidate="txtProjectName"
                        ErrorMessage="Project Name" Display="None" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblProjectDescription" runat="server" Text="Description" AssociatedControlID="txtProjectDescription" />
                </td>
                <td></td>
                <td>
                    <asp:TextBox ID="txtProjectDescription" runat="server" Width="195" TextMode="MultiLine" Rows="11" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvProjectDescription" runat="server" ControlToValidate="txtProjectDescription"
                        ErrorMessage="Description" Display="None" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblRegions" runat="server" Text="Admin Region" />
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlRegions" runat="server" Width="202" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlRegions" ErrorMessage="Region"
                        InitialValue="<%$ AppSettings: REGIONMENUDEFAULT %>" Display="None" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblowners" runat="server" Text="Project Owner" />
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlOwners" runat="server" Width="202" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlOwners" ErrorMessage="Project Owner"
                        InitialValue="<%$ AppSettings: PROJECTOWNERMENUDEFAULT %>" Display="None" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblplatforms" runat="server" Text="Platform" />
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlPlatforms" runat="server" Width="202" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlPlatforms" ErrorMessage="Platform"
                        InitialValue="<%$ AppSettings: PLATFORMMENUDEFAULT %>" Display="None" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblCategories" runat="server" Text="Category" />
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlCategories" runat="server" Width="202" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlCategories" ErrorMessage="Category"
                        InitialValue="<%$ AppSettings: CATEGORYMENUDEFAULT %>" Display="None" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblBau" runat="server" Text="Business as Usual" />
                </td>
                <td></td>
                <td>
                    <asp:CheckBox ID="chkBaU" runat="server" CssClass="newitemcheckbox" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    &nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnSaveProject" runat="server" Text="Save" CssClass="tradewebbutton" />&nbsp;&nbsp;
                    <asp:button ID="btnCancelProject" runat="server" Text="Cancel" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>
  
    <cc1:ModalPopupExtender ID="mpeNewProject" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewproject"
        PopupControlID="pnlNewProject"
        CancelControlID="btnCancelProject"
        DropShadow="true"
        PopupDragHandleControlID="newprojecttitle" />

    <asp:ValidationSummary ID="vsNewProject" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" />

	<asp:SqlDataSource ID="Projects_Data" runat="server"
		ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
		SelectCommand="SELECT * FROM ProjectSummary WHERE archive = 0 AND CASE WHEN Brag = 'Blue' THEN 'True' ELSE 'False' END <= @show_completed"
		FilterExpression ="brag LIKE '{0}' AND full_name LIKE '{1}' AND platform LIKE '{2}' AND category LIKE '{3}' AND region LIKE '{4}'">
		<SelectParameters>
			<asp:ControlParameter Name="show_completed" Type="String"
				ControlID="chkShowComplete"
				PropertyName="Checked" />
		</SelectParameters>
		<FilterParameters>
			<asp:ControlParameter ControlID="ddlBragFilter" PropertyName="SelectedValue" Name="brag" />
			<asp:ControlParameter ControlID="ddlOwnerFilter" PropertyName="SelectedValue" Name="full_name" />
			<asp:ControlParameter ControlID="ddlPlatformsFilter" PropertyName="SelectedValue" Name="platform" />
            <asp:ControlParameter ControlID="ddlCategoriesFilter" PropertyName="SelectedValue" Name="category" />
			<asp:ControlParameter ControlID="ddlRegionsFilter" PropertyName="SelectedValue" Name="region" />
		</FilterParameters>
	</asp:SqlDataSource>
</asp:Content>
