﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Drawing

Partial Class DashBoard
    Inherits System.Web.UI.Page

    Protected Sub imbhome_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
	End Sub

	Protected Sub Chart1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart1.DataBound
		For Each Point1 In Chart1.Series("Series1").Points
			Select Case Point1.AxisLabel
				Case "BLUE"
					Point1.Color = Color.FromArgb(255, 0, 102, 255)
				Case "RED"
					Point1.Color = Color.Red
				Case "AMBER"
					Point1.Color = Color.FromArgb(255, 255, 181, 71)
				Case "GREEN"
					Point1.Color = Color.FromArgb(255, 0, 176, 80)
				Case Else
					Point1.Color = Color.Gray
			End Select
		Next
	End Sub

    Protected Sub imbMyView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbMyView.Click
        Dim intUserID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & Request.ServerVariables("LOGON_USER") & "'")
        Response.Redirect("/MyView.aspx?PersonID=" & intUserID, False)
    End Sub
End Class
