﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Reports
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            ' dynamically create treeview root from DB
            Dim dt As DataTable = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Parent_ID, Title, Query, (SELECT COUNT(*) FROM Reports WHERE Parent_ID = r.id) childnodecount FROM Reports r WHERE Parent_ID = 0 ORDER BY Title").Tables(0)
            For Each dr As DataRow In dt.Rows
                Dim tn As New TreeNode()
                tn.Text = dr("Title").ToString()
                tn.ToolTip = dr("Title").ToString()
                tn.Value = dr("ID").ToString() & ";" & dr("Query").ToString()
                tvReports.Nodes.Add(tn)
                tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
            Next
        End If
    End Sub

    Protected Sub imbhome_Click(sender As Object, e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub imbMyView_Click(sender As Object, e As ImageClickEventArgs) Handles imbMyView.Click
        Dim intUserID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & Request.ServerVariables("LOGON_USER") & "'")
        Response.Redirect("/MyView.aspx?PersonID=" & intUserID, False)
    End Sub

    Protected Sub tvReports_SelectedNodeChanged(sender As Object, e As EventArgs) Handles tvReports.SelectedNodeChanged
        Reports_Data.SelectCommand = tvReports.SelectedValue.Split(";")(1)
        Reports_Grid.Caption = tvReports.SelectedNode.Text
		Reports_Grid.DataBind()
    End Sub

    Protected Sub tvReports_TreeNodePopulate(sender As Object, e As TreeNodeEventArgs) Handles tvReports.TreeNodePopulate
		' query for treeview sub-nodes on demand
		Dim intParentID As Int32 = CInt(e.Node.Value.Split(";")(0))
		Dim dt As DataTable = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Parent_ID, Title, Query, (SELECT COUNT(*) FROM Reports WHERE Parent_ID = r.id) childnodecount FROM Reports r WHERE Parent_ID = " & intParentID & " ORDER BY Title").Tables(0)
		For Each dr As DataRow In dt.Rows
			Dim tn As New TreeNode()
            tn.Text = dr("Title").ToString()
            tn.ToolTip = dr("Title").ToString()
			tn.Value = dr("ID").ToString() & ";" & dr("Query").ToString()
			e.Node.ChildNodes.Add(tn)
			tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
		Next
    End Sub

    Protected Sub imbexport_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbexport.Click
        If Reports_Grid.Rows.Count > 0 Then
            Dim sw As New IO.StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            Dim dg As New DataGrid

            dg.DataSource = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, tvReports.SelectedValue.Split(";")(1))
            dg.DataBind()

            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=" & tvReports.SelectedNode.Text & ".xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/ms-excel"
            dg.RenderControl(hw)
            Response.Write(sw.ToString())
            Response.End()

            dg.Dispose()
        End If
    End Sub

    Protected Sub Reports_Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Reports_Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            For Each col As TableCell In e.Row.Cells
                Dim encoded As String = col.Text
                col.Text = Context.Server.HtmlDecode(encoded)
            Next
        End If
    End Sub
End Class
