﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Dashboard.aspx.vb" Inherits="DashBoard" title="" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
		<div style="float:left;">
            <span class="menubarbutton">
                <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                    ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
                <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                    ToolTip="Navigate Home" />
            </span>
			<span class="menubarbutton">
				<asp:ImageButton ID="imbMyView" runat="server" ImageAlign="absMiddle"
					ImageUrl="~/images/TaskList_16x.png" AlternateText="View all your tasks and issues" ToolTip="View all your tasks and issues"
					CausesValidation="false" />
				<asp:Label ID="lblMyView" runat="server" Text="My Tasks" AssociatedControlID="imbMyView" CssClass="menutext"
					ToolTip="View all your tasks and issues" />
			</span>
		</div>
		<div style="float:right; padding: 3px 0px;">
		</div>
    </div>
    <div class="subTitle" style="clear: both;">DashBoard</div>
    <div style="float: left; width: 50%">
        <asp:Chart ID="Chart1" runat="server" DataSourceID="Brag_Data" Width="500px">
            <Titles>
                <asp:Title Text="Open Projects by Status" Font="Microsoft Sans Serif, 14pt" />
            </Titles>
            <Series>
                <asp:Series Name="Series1" ChartType="Pie" IsValueShownAsLabel="true"
                    XValueMember="Brag" YValueMembers="CountOfProject" YValueType="Int32" 
                    XValueType="String">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <Area3DStyle Enable3D="True" LightStyle="Realistic" Rotation="-90" />
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
    <div style="float:left; width: 50%; text-align:center;">
        <asp:Chart ID="Chart6" runat="server" DataSourceID="IncompleteByPlatform" Width="500px" Palette="Excel">
            <Titles>
                <asp:Title Text="Open Projects By Platform" Font="Microsoft Sans Serif, 14pt" />
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1">
                </asp:Legend>
            </Legends>
            <Series>
                <asp:Series Name="Series1" ChartType="Pie" IsValueShownAsLabel="true"
                    XValueMember="Platform" YValueMembers="Count" YValueType="Int32" 
                    XValueType="String" Legend="Legend1">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <Area3DStyle Enable3D="True" LightStyle="Realistic" />
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
    <div style="clear: both;">
        &nbsp;
    </div>
    <div style="float:left; width: 50%; text-align: center;">
        <asp:Chart ID="Chart2" runat="server" DataSourceID="Tasks_Data" Width="500px">
            <Titles>
                <asp:Title Text="Completed Tasks" Font="Microsoft Sans Serif, 14pt" />
            </Titles>
            <Series>
                <asp:Series Name="Series1" IsValueShownAsLabel="true"
                    XValueMember="Span" YValueMembers="Count" YValueType="Int32" 
                    XValueType="String" CustomProperties="DrawingStyle=Cylinder" ShadowOffset="2">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX LineColor="64, 64, 64, 64">
                        <majorgrid linecolor="64, 64, 64, 64" />
                    </AxisX>
                    <AxisY LineColor="64, 64, 64, 64">
                        <majorgrid linecolor="64, 64, 64, 64" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
    <div style="float:left; width: 50%;">
        <asp:Chart ID="Chart3" runat="server" DataSourceID="Project_Data" Width="500px">
            <Titles>
                <asp:Title Text="Completed Projects" Font="Microsoft Sans Serif, 14pt" />
            </Titles>
            <Series>
                <asp:Series Name="Series1" IsValueShownAsLabel="true"
                    XValueMember="Span" YValueMembers="Count" YValueType="Int32" 
                    XValueType="String" CustomProperties="DrawingStyle=Cylinder" ShadowOffset="2">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX LineColor="64, 64, 64, 64">
                        <majorgrid linecolor="64, 64, 64, 64" />
                    </AxisX>
                    <AxisY LineColor="64, 64, 64, 64">
                        <majorgrid linecolor="64, 64, 64, 64" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>  
    </div>
    <div style="clear: both;">&nbsp;</div>
    <div style="float:left; width: 50%; text-align: center;">
        <asp:Chart ID="Chart4" runat="server" DataSourceID="CompletedByPlatformThisQuarter" Width="500px">
            <Titles>
                <asp:Title Text="Projects Completed This Quarter by Platform" Font="Microsoft Sans Serif, 14pt" />
            </Titles>
            <Series>
                <asp:Series Name="Series1" IsValueShownAsLabel="true"
                    XValueMember="Platform" YValueMembers="Count" YValueType="Int32" 
                    XValueType="String" CustomProperties="DrawingStyle=Cylinder" ShadowOffset="2">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX LineColor="64, 64, 64, 64">
                        <majorgrid linecolor="64, 64, 64, 64" />
                    </AxisX>
                    <AxisY LineColor="64, 64, 64, 64">
                        <majorgrid linecolor="64, 64, 64, 64" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
    <div style="float:left; width: 50%;">
        <asp:Chart ID="Chart5" runat="server" DataSourceID="IncompleteByPlatform" Width="500px">
            <Titles>
                <asp:Title Text="Open Projects by Platform" Font="Microsoft Sans Serif, 14pt" />
            </Titles>
            <Series >
                <asp:Series Name="Series1" IsValueShownAsLabel="true"
                    XValueMember="Platform" YValueMembers="Count" YValueType="Int32" 
                    XValueType="String" CustomProperties="DrawingStyle=Cylinder" ShadowOffset="2">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX LineColor="64, 64, 64, 64">
                        <majorgrid linecolor="64, 64, 64, 64" />
                    </AxisX>
                    <AxisY LineColor="64, 64, 64, 64">
                        <majorgrid linecolor="64, 64, 64, 64" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>  
    </div>
    <div style="clear: both;">&nbsp;</div>
	<div style="float:left; width: 50%; text-align: center;">
        <asp:Chart ID="Chart7" runat="server" DataSourceID="OpenProjectByRegion" Width="500px">
            <Titles>
                <asp:Title Text="Open Projects by Region" Font="Microsoft Sans Serif, 14pt" />
            </Titles>
            <Series>
                <asp:Series Name="Series1" IsValueShownAsLabel="true"
                    XValueMember="Region" YValueMembers="Count" YValueType="Int32" 
                    XValueType="String" CustomProperties="DrawingStyle=Cylinder" ShadowOffset="2">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX LineColor="64, 64, 64, 64">
                        <majorgrid linecolor="64, 64, 64, 64" />
                    </AxisX>
                    <AxisY LineColor="64, 64, 64, 64">
                        <majorgrid linecolor="64, 64, 64, 64" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
	<div style="clear: both;">&nbsp;</div>
    <asp:SqlDataSource ID="Brag_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT convert(int, count(Project)) AS CountOfProject, Brag FROM ProjectSummary WHERE archive = 0 AND Brag <> 'BLUE' GROUP BY Brag ORDER BY CASE WHEN Brag = 'BLUE' THEN 1 WHEN Brag = 'RED' THEN 2 WHEN Brag = 'AMBER' THEN 3 WHEN Brag = 'GREEN' THEN 4 END ASC">
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Tasks_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT 'Week' AS [Span], Count(*) AS [Count] FROM Tasks t INNER JOIN Projects p ON t.Project_ID = p.ID WHERE t.Status_ID = @CompleteID AND p.archive = 0 AND t.Completed_Date IS NOT NULL AND DATEPART(wk, Completed_Date) = DATEPART(wk, GetDate()) UNION ALL SELECT 'Month' AS [Metric], Count(*) AS [Count] FROM Tasks t INNER JOIN Projects p ON t.Project_ID = p.ID WHERE t.Status_ID = @CompleteID AND p.archive = 0 AND t.Completed_Date IS NOT NULL AND Month(Completed_Date) = Month(GetDate()) UNION ALL SELECT 'Quarter' AS [Metric], Count(*) AS [Count] FROM Tasks t INNER JOIN Projects p ON t.Project_ID = p.ID WHERE t.Status_ID = @CompleteID AND p.archive = 0 AND t.Completed_Date IS NOT NULL AND DATEPART(Quarter, Completed_Date) = DATEPART(Quarter, GetDate()) UNION ALL SELECT 'Year' AS [Metric], Count(*) AS [Count] FROM Tasks t INNER JOIN Projects p ON t.Project_ID = p.ID WHERE t.Status_ID = @CompleteID AND p.archive = 0 AND t.Completed_Date IS NOT NULL AND DATEPART(YEAR, Completed_Date) = DATEPART(YEAR, GetDate())">
        <SelectParameters>
            <asp:Parameter DefaultValue='<%$ AppSettings:STATUSCOMPLETEINDEX %>' Name="CompleteID" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Project_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT 'Week' AS [Span], Count(p1.Project) AS [Count] FROM Projects p1 WHERE DATEPART(wk, (SELECT MAX(t.Completed_Date) FROM Tasks t WHERE t.Project_ID = p1.ID)) = DATEPART(wk, GetDate()) AND (SELECT Count(t.Status_ID) FROM Tasks t WHERE t.Project_ID = p1.ID AND t.Status_ID <> @CompleteID) = 0 UNION ALL SELECT 'Month' AS [Span], Count(p1.Project) AS [Count] FROM Projects p1 WHERE Month((SELECT MAX(t.Completed_Date) FROM Tasks t WHERE t.Project_ID = p1.ID)) = Month(GetDate()) AND (SELECT Count(t.Status_ID) FROM Tasks t WHERE t.Project_ID = p1.ID AND t.Status_ID <> @CompleteID) = 0 UNION ALL SELECT 'Quarter' AS [Span], Count(p1.Project) AS [Count] FROM Projects p1 WHERE DATEPART(Quarter, (SELECT MAX(t.Completed_Date) FROM Tasks t WHERE t.Project_ID = p1.ID)) = DATEPART(Quarter, GetDate()) AND (SELECT Count(t.Status_ID) FROM Tasks t WHERE t.Project_ID = p1.ID AND t.Status_ID <> @CompleteID) = 0 UNION ALL SELECT 'Year' AS [Span], Count(p1.Project) AS [Count] FROM Projects p1 WHERE DATEPART(Year, (SELECT MAX(t.Completed_Date) FROM Tasks t WHERE t.Project_ID = p1.ID)) = DATEPART(Year, GetDate()) AND (SELECT Count(t.Status_ID) FROM Tasks t WHERE t.Project_ID = p1.ID AND t.Status_ID <> @CompleteID) = 0">
        <SelectParameters>
            <asp:Parameter DefaultValue='<%$ AppSettings:STATUSCOMPLETEINDEX %>' Name="CompleteID" />
        </SelectParameters>
    </asp:SqlDataSource>
   
    <asp:SqlDataSource ID="CompletedByPlatformThisQuarter" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT Count(p.ID) As [Count], c.Platform FROM Projects p INNER JOIN Platforms c ON p.Platform_ID = c.ID WHERE DATEPART(Quarter, (SELECT MAX(t.Completed_Date) FROM Tasks t WHERE t.Project_ID = p.ID)) = DATEPART(Quarter, GetDate()) AND p.archive = 0 AND (SELECT Count(t.Status_ID) FROM Tasks t WHERE t.Project_ID = p.ID AND t.Status_ID <> @CompleteID) = 0 GROUP BY c.Platform">
        <SelectParameters>
            <asp:Parameter DefaultValue='<%$ AppSettings:STATUSCOMPLETEINDEX %>' Name="CompleteID" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="IncompleteByPlatform" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT Count(p.ID) As [Count], c.Platform FROM Projects p INNER JOIN Platforms c ON p.Platform_ID = c.ID WHERE p.archive = 0 AND (SELECT Count(t.Status_ID) FROM Tasks t WHERE t.Project_ID = p.ID AND t.Status_ID <> @CompleteID) > 0 GROUP BY c.Platform">
        <SelectParameters>
            <asp:Parameter DefaultValue='<%$ AppSettings:STATUSCOMPLETEINDEX %>' Name="CompleteID" />
        </SelectParameters>
    </asp:SqlDataSource>

	<asp:SqlDataSource ID="OpenProjectByRegion" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT Count(p.ID) As [Count], r.Region FROM Projects p INNER JOIN Regions r ON p.Region_ID = r.ID WHERE p.archive = 0 AND ((SELECT Count(t.Status_ID) FROM Tasks t WHERE t.Project_ID = p.ID AND t.Status_ID <> @CompleteID) > 0 AND (SELECT Count(t.Status_ID) FROM Tasks t WHERE t.Project_ID = p.ID AND t.Status_ID <> @CancelledID) > 0) GROUP By r.Region">
        <SelectParameters>
            <asp:Parameter DefaultValue='<%$ AppSettings:STATUSCOMPLETEINDEX %>' Name="CompleteID" />
        </SelectParameters>
        <SelectParameters>
            <asp:Parameter DefaultValue='<%$ AppSettings:STATUSCANCELLEDINDEX %>' Name="CancelledID" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
