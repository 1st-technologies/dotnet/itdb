﻿<%@ Page Title="" Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="Dashboard2.aspx.vb" Inherits="Dashboard2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
		<div style="float:left; padding: 3px 0px;">
            <span class="menubarbutton">
                <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                    ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
                <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                    ToolTip="Navigate Home" />
            </span>
			<span class="menubarbutton">
				<asp:ImageButton ID="imbMyView" runat="server" ImageAlign="absMiddle"
					ImageUrl="~/images/TaskList_16x.png" AlternateText="View all your tasks and issues" ToolTip="View all your tasks and issues"
					CausesValidation="false" />
				<asp:Label ID="lblMyView" runat="server" Text="My Tasks" AssociatedControlID="imbMyView" CssClass="menutext"
					ToolTip="View all your tasks and issues" />
			</span>
		</div>
		<span class="menubarbutton">
			<asp:Label ID="lblCharts" runat="server" Text="Charts" AssociatedControlID="ddlcharts"
					CssClass="menutext" ToolTip="Select a chart" />
			<asp:DropDownList ID="ddlCharts" runat="server" ToolTip="Select a chart" AutoPostBack="true" />
		</span>
		<span class="menubarbutton">
			<asp:Label ID="lblChartType" runat="server" Text="Charts Types" AssociatedControlID="ddlChartTypes"
					CssClass="menutext" ToolTip="Select a Chart Type" />
			<asp:DropDownList ID="ddlChartTypes" runat="server" ToolTip="Select a Chart Type" AutoPostBack="true">
				<asp:ListItem Text="Column" Value="10" />
				<asp:ListItem Text="Bar" Value="7" />
				<asp:ListItem Text="Area" Value="13" />
				<asp:ListItem Text="Boxplot" Value="28" />
				<asp:ListItem Text="Bubble" Value="2" />
				<asp:ListItem Text="Candlestick" Value="20" />
				<asp:ListItem Text="Doughnut" Value="18" />
				<asp:ListItem Text="ErrorBar" Value="27" />
				<asp:ListItem Text="FastLine" Value="6" />
				<asp:ListItem Text="FastPoint" Value="1" />
				<asp:ListItem Text="Funnel" Value="33" />
				<asp:ListItem Text="Kagi" Value="31" />
				<asp:ListItem Text="Line" Value="3" />
				<asp:ListItem Text="Pie" Value="17" />
				<asp:ListItem Text="Point" Value="0" />
				<asp:ListItem Text="Point and Figure" Value="32" />
				<asp:ListItem Text="Polar" Value="26" />
				<asp:ListItem Text="Pyramid" Value="34" />
				<asp:ListItem Text="Radar" Value="25" />
				<asp:ListItem Text="Range" Value="21" />
				<asp:ListItem Text="Range Bar" Value="23" />
				<asp:ListItem Text="Range Column" Value="24" />
				<asp:ListItem Text="Renko" Value="29" />
				<asp:ListItem Text="Spline" Value="4" />
				<asp:ListItem Text="Spline Area" Value="14" />
				<asp:ListItem Text="Spline Range" Value="22" />
				<asp:ListItem Text="Stacked Area" Value="15" />
				<asp:ListItem Text="Stacked Area 100" Value="16" />
				<asp:ListItem Text="Stacked Bar" Value="8" />
				<asp:ListItem Text="Stacked Bar 100" Value="9" />
				<asp:ListItem Text="Stacked Column" Value="11" />
				<asp:ListItem Text="Stacked Column 100" Value="12" />
				<asp:ListItem Text="Stepline" Value="5" />
				<asp:ListItem Text="Stock" Value="19" />
				<asp:ListItem Text="Three line Break" Value="30" />
			</asp:DropDownList>
		</span>
    </div>
    <div class="subTitle" style="clear: both;">DashBoard</div>
	<asp:UpdatePanel runat="server">
		<ContentTemplate>
			<asp:Chart ID="Chart1" runat="server" DataSourceID="chart1_data" />
		</ContentTemplate>
	</asp:UpdatePanel>
	<asp:SqlDataSource ID="chart1_data" runat="server" ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>" />
</asp:Content>

