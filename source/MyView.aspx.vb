﻿
Partial Class MyView
    Inherits System.Web.UI.Page
    Protected Sub imbhome_Click(sender As Object, e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub MyView_Grid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles MyView_Grid.RowCommand
        Dim ItemID As Integer
        If e.CommandName = "Task" Then
            ItemID = Convert.ToInt32(e.CommandArgument)
            Response.Redirect("TaskDetails.aspx?TaskID=" & ItemID, False)
        ElseIf e.CommandName = "Issue" Then
            ItemID = Convert.ToInt32(e.CommandArgument)
            Response.Redirect("IssueDetails.aspx?IssueID=" & ItemID, False)
        ElseIf e.CommandName = "Project" Then
            ItemID = Convert.ToInt32(e.CommandArgument)
            Response.Redirect("ProjectDetails.aspx?ProjectID=" & ItemID, False)
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Me.Page.IsPostBack Then
            ddlStatusFilter.DataSource = common.fGetAllStatus_Types()
            ddlStatusFilter.DataTextField = "Status"
            ddlStatusFilter.DataValueField = "Status"
            ddlStatusFilter.DataBind()
            'ddlStatusFilter.Items.Insert(0, New ListItem("N/A", "N/A"))
            ddlStatusFilter.Items.Insert(0, New ListItem("All", "%"))

            ddlPriorityFilter.DataSource = common.fGetPriorities()
            ddlPriorityFilter.DataTextField = "Priority"
            ddlPriorityFilter.DataValueField = "Priority"
            ddlPriorityFilter.DataBind()
            ddlPriorityFilter.Items.Insert(0, New ListItem("All", "%"))

            If Not Request.Cookies("MYVIEWPAGESIZE") Is Nothing Then
                MyView_Grid.PageSize = Convert.ToInt32(Request.Cookies("MYVIEWPAGESIZE").Value)
                ddlPageSize.SelectedValue = Request.Cookies("MYVIEWPAGESIZE").Value
            End If

            If Not Request.Cookies("MYVIEWPRIORITYFILTER") Is Nothing Then
                ddlPriorityFilter.SelectedValue = Request.Cookies("MYVIEWPRIORITYFILTER").Value
            End If

            If Not Request.Cookies("MYVIEWSTATUSFILTER") Is Nothing Then
                ddlStatusFilter.SelectedValue = Request.Cookies("MYVIEWSTATUSFILTER").Value
            End If

            If Not Request.Cookies("MYVIEWTASKTYPEFILTER") Is Nothing Then
                ddlTaskType.SelectedValue = Request.Cookies("MYVIEWTASKTYPEFILTER").Value
            End If

            If Not Request.Cookies("SHOWMYCOMPLETED") Is Nothing Then
                chkShowComplete.Checked = Request.Cookies("SHOWMYCOMPLETED").Value
            End If
        End If
    End Sub

    Protected Sub imbexport_Click(sender As Object, e As ImageClickEventArgs) Handles imbexport.Click
        Dim sw As New IO.StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        Dim dg As New DataGrid

        dg.DataSource = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT * FROM MySummary2 WHERE Person_ID = " & Request.QueryString("PersonID") & " AND Priority LIKE '" & ddlPriorityFilter.SelectedValue & "' AND Status LIKE '" & ddlStatusFilter.SelectedValue & "' AND Work_Type LIKE '" & ddlTaskType.SelectedValue & "'")
        dg.DataBind()

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=MyView.xls")
        Response.Charset = ""
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/ms-excel"
        dg.RenderControl(hw)
        Response.Write(sw.ToString())
        Response.End()

        dg.Dispose()
    End Sub

    Protected Sub MyView_Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles MyView_Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DueDate As String = e.Row.Cells(2).Text
            Dim lblGridDebug1 As Label = DirectCast(e.Row.FindControl("lblGridDebug"), Label)
            If IsDate(DueDate) Then
                If DateDiff(DateInterval.Day, Convert.ToDateTime(DueDate), Now()) > -1 Then
                    e.Row.Cells(2).BackColor = Drawing.Color.Red
                    e.Row.Cells(2).ForeColor = Drawing.Color.White
                    e.Row.Cells(2).Font.Bold = True
                End If
            End If
        End If
    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        Response.Cookies("MYVIEWPAGESIZE").Value = ddlPageSize.SelectedValue
        Response.Cookies("MYVIEWPAGESIZE").Expires = DateTime.Now.AddMonths(1)
        MyView_Grid.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue)
    End Sub

    Protected Sub ddlStatusFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatusFilter.SelectedIndexChanged
        Response.Cookies("MYVIEWSTATUSFILTER").Value = ddlStatusFilter.SelectedValue
        Response.Cookies("MYVIEWSTATUSFILTER").Expires = DateTime.Now.AddMonths(1)
    End Sub

    Protected Sub ddlTaskType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTaskType.SelectedIndexChanged
        Response.Cookies("MYVIEWTASKTYPEFILTER").Value = ddlTaskType.SelectedValue
        Response.Cookies("MYVIEWTASKTYPEFILTER").Expires = DateTime.Now.AddMonths(1)
    End Sub

    Protected Sub ddlPriorityFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPriorityFilter.SelectedIndexChanged
        Response.Cookies("MYVIEWPRIORITYFILTER").Value = ddlTaskType.SelectedValue
        Response.Cookies("MYVIEWPRIORITYFILTER").Expires = DateTime.Now.AddMonths(1)
    End Sub

    Protected Sub chkShowComplete_CheckedChanged(sender As Object, e As EventArgs) Handles chkShowComplete.CheckedChanged
        Response.Cookies("SHOWMYCOMPLETED").Value = chkShowComplete.Checked
        Response.Cookies("SHOWMYCOMPLETED").Expires = DateTime.Now.AddMonths(12)
    End Sub
End Class
