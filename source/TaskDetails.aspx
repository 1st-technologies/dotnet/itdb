﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="TaskDetails.aspx.vb"
    Inherits="Tasks" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar" style="padding: 3px 0px;">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
        <span class="menubarbutton">
            <asp:ImageButton ID="imbback" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/BranchRelationshipParent_16x.png" AlternateText="Back to parent project" CausesValidation="false"/>
            <asp:Label ID="lblback" runat="server" Text="Parent" AssociatedControlID="imbback" CssClass="menutext"
                ToolTip="Back to parent project" />
        </span>
		<span class="menubarbutton">
			<asp:ImageButton ID="imbMyView" runat="server" ImageAlign="absMiddle"
				ImageUrl="~/images/TaskList_16x.png" AlternateText="View all your tasks and issues" ToolTip="View all your tasks and issues"
				CausesValidation="false" />
			<asp:Label ID="lblMyView" runat="server" Text="My Tasks" AssociatedControlID="imbMyView" CssClass="menutext"
                ToolTip="View all your tasks and issues" />
		</span>
        <span class="menubarbutton">
            <asp:ImageButton ID="imbnewtasklog" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Comment_16x.png" AlternateText="Create a new log entry" CausesValidation="false"/>
            <asp:Label ID="lblnewtasklog" runat="server" Text="New Log" AssociatedControlID="imbnewtasklog" CssClass="menutext"
                ToolTip="Create a new log entry" />
        </span>

        <span class="menubarbutton">
            <asp:ImageButton ID="imbnewtaskattachment" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Attach_16x.png" AlternateText="Upload an attachment" CausesValidation="false"/>
            <asp:Label ID="lblnewattachment" runat="server" Text="Upload Attachment" AssociatedControlID="imbnewtaskattachment"
                ToolTip="Upload an attachment" CssClass="menutext" />
        </span>
    </div>
    <div class="subTitle">Task Details</div>

    <asp:DetailsView ID="DetailsView1" runat="server"
        DataKeyNames="ID"
        DataSourceID="Task_Data"
        AutoGenerateRows="False"
        AutoGenerateEditButton="True"
        CssClass="detailsView" RowStyle-Height="23">
        <FieldHeaderStyle CssClass="detailsView_header" />
        <AlternatingRowStyle CssClass="gridView_text_alternate" />
        <Fields>
            <asp:TemplateField HeaderText="Project">
                <ItemTemplate>
                    <div style="width:820px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;">
                        <asp:Label ID="lblProject" runat="server" Text='<%# Bind("Project") %>' />
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Task">
                <EditItemTemplate>
                    <asp:TextBox ID="txtTask" runat="server" Text='<%# Bind("Task")%>' CssClass="gridView_TextBox" MaxLength="128" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblTask" runat="server" Text='<%# Bind("Task") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="txtTaskDescription" runat="server" TextMode="MultiLine" Text='<%# Bind("Description") %>' Height="100"
                        CssClass="gridView_TextBox" MaxLength="4000" />
                </EditItemTemplate>
                <ItemTemplate>
                    <div style="word-wrap: normal; overflow-y: auto; height: 100px;">
                        <%#Eval("Description")%>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Priority">
                <EditItemTemplate>
                    <asp:DropDownList id="ddlPriority" runat="server"
                        CSSClass="gridView_TextBox"
                        DataTextField="Priority"
                        DataValueField="ID"
                        DataSourceID="Priority_Data"
                        SelectedValue='<%#Eval("Priority_ID")%>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPriority" runat="server" Text='<%# Bind("Priority")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status">
                <EditItemTemplate>
                    <asp:DropDownList id="ddlStatus" runat="server" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"
                        CSSClass="gridView_TextBox"
                        DataTextField="Status"
                        DataValueField="ID"
                        DataSourceID="Status_Data"
                        SelectedValue='<%#Eval("Status_ID")%>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Owner">
                <EditItemTemplate>
                    <asp:DropDownList id="ddlEditOwner" runat="server"
                        CSSClass="gridView_TextBox"
                        DataTextField="full_name"
                        DataValueField="ID"
                        DataSourceID="People_Data"
                        SelectedValue='<%#Eval("Person_ID")%>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblOwner" runat="server" Text='<%# Bind("full_name")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Percent Complete">
                <ItemTemplate>
                    <asp:Label ID="lblPercentComplete" runat="server" Text='<%# Eval("Percent_Complete", "{0}%")%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtPercentComplete" runat="server" Text='<%# Bind("Percent_Complete")%>' CssClass="gridView_TextBox" OnTextChanged="txtPercentComplete_TextChanged" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <ItemTemplate>
                    <asp:label ID="lblState_Date" runat="server" Text='<%# Bind("Start_Date", "{0:MM/dd/yyyy}")%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox runat="server" ID="txtStartDate" AutoCompleteType="Disabled"
                        Text='<%# Bind("Start_Date", "{0:MM/dd/yyyy}")%>' CSSClass="gridView_TextBox" />
                    <cc1:CalendarExtender ID="ceStartDate" runat="server" TargetControlID="txtStartDate" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Due Date">
                <ItemTemplate>
                    <asp:label ID="lblDue_Date" runat="server" Text='<%# Bind("Due_Date", "{0:MM/dd/yyyy}")%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox runat="server" ID="txtDueDate" AutoCompleteType="Disabled"
                        Text='<%# Bind("Due_Date", "{0:MM/dd/yyyy}")%>' CSSClass="gridView_TextBox" />
                    <cc1:CalendarExtender ID="ceDueDate" runat="server" TargetControlID="txtDueDate" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Completed Date">
                <ItemTemplate>
                    <asp:label ID="lblCompleted_Date" runat="server" Text='<%# Bind("Completed_Date", "{0:MM/dd/yyyy}")%>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox runat="server" ID="txtCompletedDate" AutoCompleteType="Disabled"
                        Text='<%# Bind("Completed_Date", "{0:MM/dd/yyyy}")%>' CSSClass="gridView_TextBox" OnTextChanged="txtCompletedDate_TextChanged" />
                    <cc1:CalendarExtender ID="ceCompletedDate" runat="server" TargetControlID="txtCompletedDate" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Accomplishment">
                <ItemTemplate>
                    <asp:CheckBox ID="chkAchievement" runat="server" Checked='<%# Bind("achievement")%>' Enabled="false" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="chkAchievement" runat="server" Checked='<%# Bind("achievement")%>' Enabled="true" CSSClass="gridView_TextBox" />
                </EditItemTemplate>
            </asp:TemplateField>
        </Fields>
    </asp:DetailsView>
    
    <div style="line-height: 22px;">&nbsp;</div>
    <div class="subTitle">History</div>

    <asp:UpdatePanel ID="upTaskLog" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="TaskLog_Grid" runat="server"
                AutoGenerateColumns="False"
                DataKeyNames="ID"
                DataSourceID="Task_History_Data"
                AllowPaging="true" CssClass="gridView" EmptyDataText="There are no log entries associated with this task."
                PageSize="3" PagerSettings-Mode="Numeric">
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <Columns>
                    <asp:BoundField DataField="Date" HeaderText="Date" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="190" />
                    <asp:TemplateField HeaderText="Entry">
                        <ItemTemplate>
                            <div style="width: 1403px; max-height: 300px; word-wrap: normal; overflow: auto;">
								<%#Eval("log_entry")%>
							</div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div style="line-height: 22px;">&nbsp;</div>
    <div class="subTitle">Attachments</div>

    <asp:UpdatePanel ID="upAttachments" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="Task_Attachments_Grid" runat="server"
                AutoGenerateColumns="False"
                DataKeyNames="ID"
                DataSourceID="Task_Attachment_Data"
                AllowPaging="True" CssClass="gridView" EmptyDataText="There are no files attached to this task."
                AllowSorting="True" PagerSettings-Mode="Numeric" EnableModelValidation="True">
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <Columns>
                    <asp:TemplateField HeaderText="File" SortExpression="File_Name" ItemStyle-Width="190">
                        <ItemTemplate>
                            <asp:LinkButton id="btndownload" runat="server" CommandName="downloadtaskattachment"
                                CommandArgument='<%#Bind("ID") %>' Text='<%#Bind("File_Name") %>' CssClass="gridviewlinkbutton" />
                        </ItemTemplate>
                        <ItemStyle Width="190px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <div style="width: 100%; overflow: hidden; text-overflow: ellipsis;">
								<%#Eval("Description")%>
							</div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ButtonType="Image" DeleteImageUrl="~/images/DeleteAttachment_16x.png" ShowDeleteButton="True"
                        ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center" >
                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel id="pnlNewTaskLog" runat="server" CssClass="modalpopup"
        Height="418px" Width="760px" style="display: none;" DefaultButton="btnSaveTaskLog">
        <div id="newpTasklogtitle" class="popuptitle">New Log</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td>
                    <CKEditor:CKEditorControl ID="ceNewTaskLogEntry" runat="server"
                        Height="235" ResizeEnabled="false" BorderWidth="0"
                        Toolbar="Source|-|Cut|Copy|Paste|PasteText|PasteFromWord|-|Undo|Redo|-|Bold|Italic|Underline|Strike|Subscript|Superscript|-|NumberedList|BulletedList|-|Outdent|Indent|-|JustifyLeft|justifyCenter|JustifyRight|-|Link|Unlink
                        /
                    	Format|Font|FontSize" Skin="office2003"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align:center">
                    <asp:Button ID="btnSaveTaskLog" runat="server" Text="Save" CssClass="tradewebbutton" ValidationGroup="newtasklog" />
                    &nbsp;&nbsp;
                    <asp:button ID="btnCancelTaskLog" runat="server" Text="Cancel" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel id="pnlNewTaskattachment" runat="server" CssClass="modalpopup"
        Height="363px" Width="426px" style="display: none;" DefaultButton="btnSaveNewtaskAttachment">
        <div id="newtaskattachemnttitle" class="popuptitle">New attachment</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:400px; text-align: center;" class="popuplabel" colspan="3">
                    <asp:Label id="lblNewTaskAttachment" runat="server" Text="File" AssociatedControlID="fuTaskAttachment" />
                </td>
                    
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: center;">
                    <asp:FileUpload ID="fuTaskAttachment" runat="server"  Width="400px" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="fuTaskAttachment"
                        ErrorMessage="File" Display="None" ValidationGroup="newtaskattachment" />
                    <div style="text-align: center;">
                        <span class="maxsizenote">Maximum file size:</span>
                        <span><asp:label id="lblmaxwarning" runat="server" CssClass="maxsizenote" Text='<%$ AppSettings: MAXSIZESTRING %>' /></span>
                    </div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: center;" class="popuplabel" colspan="3">
                    <asp:Label id="lblTaskattachmentDescription" runat="server" Text="Description"
                        AssociatedControlID="txtTaskattachmentDescription" />
                </td>
                    
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:TextBox ID="txtTaskattachmentDescription" runat="server" Width="398" TextMode="MultiLine" Rows="11"
                        ValidationGroup="newtaskattachment" MaxLength="4000" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                        ControlToValidate="txttaskattachmentDescription"
                        ErrorMessage="Description" Display="None" ValidationGroup="newtaskattachment" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnSaveNewTaskAttachment" runat="server" Text="Save" CssClass="tradewebbutton"
                        ValidationGroup="newtaskattachment" />&nbsp;&nbsp;
                    <asp:button ID="btnCancelNewTaskAttachment" runat="server" Text="Cancel" CausesValidation="false"
                        CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:SqlDataSource ID="Task_Attachment_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Task_Attachments WHERE Task_ID = @TaskID"
        DeleteCommand="DELETE FROM Task_Attachments WHERE ID = @ID">
        <SelectParameters>
            <asp:QueryStringParameter Name="TaskID" QueryStringField="TaskID" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="int32" />
        </DeleteParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Task_History_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Task_History WHERE Task_ID = @TaskID ORDER BY [Date] DESC">
        <SelectParameters>
            <asp:QueryStringParameter Name="TaskID" QueryStringField="TaskID" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Task_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM TaskDetails WHERE ID = @TaskID"
        UpdateCommand="UPDATE Tasks SET Task = @Task, Description = @Description, Person_ID = @PersonID, Status_ID = @StatusID, Priority_ID = @PriorityID, Percent_Complete = @Percent_Complete, Start_Date = @Start_Date, Due_Date = @Due_Date, Completed_Date = @Completed_Date, achievement = @Achievement WHERE ID = @TaskID">
        <SelectParameters>
            <asp:QueryStringParameter Name="TaskID" QueryStringField="TaskID" />
        </SelectParameters>
        <UpdateParameters>
            <asp:QueryStringParameter Name="TaskID" QueryStringField="TaskID" />
            <asp:Parameter Name="Task" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Percent_Complete" Type="int32" />
            <asp:ControlParameter Name="PersonID" Type="Int32" ControlID="ctl00$ContentPlaceHolder1$DetailsView1$ddlEditOwner"
                PropertyName="SelectedValue" />
            <asp:ControlParameter Name="StatusID" Type="Int32" ControlID="ctl00$ContentPlaceHolder1$DetailsView1$ddlStatus"
                PropertyName="SelectedValue" />
            <asp:ControlParameter Name="PriorityID" Type="Int32" ControlID="ctl00$ContentPlaceHolder1$DetailsView1$ddlPriority"
                PropertyName="SelectedValue" />
            <asp:Parameter Name="Start_Date" Type="string" />
            <asp:Parameter Name="Due_Date" Type="string" />
            <asp:Parameter Name="Completed_Date" Type="string" />
            <asp:Parameter Name="Achievement" Type="Boolean" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="People_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM People ORDER BY full_name">
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Priority_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Priority_Types">
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Status_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Status_Types">
    </asp:SqlDataSource>

    <cc1:ModalPopupExtender ID="mpeNewTaskLog" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewtasklog"
        PopupControlID="pnlNewTaskLog"
        CancelControlID="btnCancelTaskLog"
        DropShadow="true"
        PopupDragHandleControlID="newtasklogtitle" />

    <cc1:ModalPopupExtender ID="mprNewTaskattachment" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewtaskattachment"
        PopupControlID="pnlNewTaskattachment"
        CancelControlID="btnCancelNewTaskAttachment"
        DropShadow="true"
        PopupDragHandleControlID="newtaskattachemnttitle" />

    <asp:ValidationSummary ID="vsTaskLog" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newtasklog" />

    <asp:ValidationSummary ID="vsTaskAttachment" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newtaskattachment" />
</asp:Content>

