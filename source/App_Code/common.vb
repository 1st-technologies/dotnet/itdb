﻿Imports Microsoft.VisualBasic
Imports System.Data

Public Class common
    Public Shared Function fGetPeople() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, full_name FROM People ORDER BY full_name")
    End Function

    Public Shared Function fGetPeopleEnabled() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, full_name FROM People WHERE Enabled = 1 ORDER BY full_name")
    End Function

    Public Shared Function fGetManagers() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, full_name FROM People WHERE Role_ID = " & ConfigurationManager.AppSettings("MANAGERROLEINDEX").ToString & " ORDER BY full_name")
    End Function

    Public Shared Function fGetManagersEnabled() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, full_name FROM People WHERE Role_ID = " & ConfigurationManager.AppSettings("MANAGERROLEINDEX").ToString & " AND Enabled = 1 ORDER BY full_name")
    End Function

    Public Shared Function fGetPriorities() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Priority FROM Priority_types")
    End Function

    Public Shared Function fGetPlatforms() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Platform FROM Platforms")
    End Function

    Public Shared Function fGetCategories() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Category FROM Categories")
    End Function
    Public Shared Function fGetRegions() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Region FROM Regions")
    End Function

    Public Shared Function fGetStatus_Types() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Status FROM Status_Types WHERE Status <> 'Complete'")
    End Function

    Public Shared Function fGetAllStatus_Types() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Status FROM Status_Types")
    End Function

    Public Shared Function fGetReports() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Title FROM Reports")
    End Function

    Public Shared Function fGetProjects() As DataSet
        Return sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Project FROM Projects ORDER BY Project")
    End Function

    Public Shared Function fLogActivity(ByVal Item_ID As Int32, ByVal Record_Type_ID As Int32, ByVal Action_ID As Int32, ByVal Note As String) As String
        Dim Person_ID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & HttpContext.Current.Request.ServerVariables("LOGON_USER") & "'")
        Dim Query As String = "INSERT INTO Activity (Date, Record_Type_ID, Item_ID, Action_ID, Person_ID, Note) VALUES ('" & Now() & "', '" & Record_Type_ID & "', '" & Item_ID & "', '" & Action_ID & "', '" & Person_ID & "', '" & Note & "')"
        sql.ExecuteCommand(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, Query)
        fLogActivity = Query
    End Function

    Public Shared Function fDiffDictionary(ByVal od1 As Specialized.IOrderedDictionary, ByVal od2 As Specialized.IOrderedDictionary) As String
        Dim bolFound As Boolean = False
        Dim Results As String = "<dl>"
        For Each de1 As DictionaryEntry In od1
            bolFound = True
            If de1.Value.ToString() <> od2(de1.Key.ToString).ToString() Then
                Results = Results & "<dt>" & de1.Key.ToString() & " (Old)</dt>"
                Results = Results & "<dd>" & de1.Value.ToString() & "</dd>"
                Results = Results & "<dt>" & de1.Key.ToString() & " (New)</dt>"
                Results = Results & "<dd>" & od2(de1.Key.ToString).ToString() & "</dd>"
            End If
        Next
        Results = Results & "</dl>"

        If bolFound = True Then
            fDiffDictionary = Results
        Else
            fDiffDictionary = ""
        End If

    End Function
End Class
