﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Public Class sql
    Public Shared Function ExecuteCommand(ByVal ConnectionString As String, ByVal Query As String) As Exception
        ' Execute a single SQL command, return error
        Dim scn As New SqlConnection(ConnectionString)
        Try
            scn.Open()
            Dim sqt As SqlTransaction = scn.BeginTransaction()
            Dim sqm As New SqlCommand(Query, scn, sqt)
            sqm.ExecuteNonQuery()
            sqt.Commit()
            sqm.Dispose()
            sqt.Dispose()
            Return Nothing
        Catch err As Exception
            Return err
        Finally
            scn.Close()
        End Try
        scn.Dispose()
    End Function

    Public Shared Function GetDataSet(ByVal ConnectionString As String, ByVal Query As String) As DataSet
        ' Execute a SQL select query or stored procedure, return dataset
        Dim scn As New SqlConnection(ConnectionString)
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable
        Try
            Dim sda As New SqlDataAdapter(Query, scn)

            scn.Open()
            sda.Fill(ds, "Table")


            Return ds
            sda.Dispose()
        Catch err As Exception
            ds.Tables.Add(dt)
            Return ds
        Finally
            scn.Close()
        End Try
        dt.Dispose()
        ds.Dispose()
        scn.Dispose()
    End Function

    Public Shared Function ExecuteScalar(ByVal ConnectionString As String, ByVal Query As String)
        ' Execute a sql select query or stored procedure, return a single element, WHERE clause should be set to return a
        ' single field from a single row as this will only return the 1st column of the last row
        ' DEVELOPER'S NOTE: do not declare a return value type, that leave this feature free to return any data type
        Dim scn As New SqlConnection(ConnectionString)
        Try
            scn.Open()
            Dim scm As SqlCommand = New SqlCommand(Query, scn)
            Return Trim(scm.ExecuteScalar())
        Catch err As SqlException
            Return Nothing
        Finally
            scn.Close()
        End Try
        scn.Dispose()
    End Function
End Class
