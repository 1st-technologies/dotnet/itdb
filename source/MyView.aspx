﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MyView.aspx.vb" Inherits="MyView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <div style="float:left;">
            <span class="menubarbutton">
                <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                    ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false" ToolTip="Navigate Home" />
                <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                    ToolTip="Navigate Home" />
            </span>
			<span class="menubarbutton">
				<asp:ImageButton ID="imbexport" runat="server" ImageAlign="absMiddle" Enabled="true"
					ImageUrl="~/images/ExcelWorksheetView_16x.png" AlternateText="Export to Excel" ToolTip="Export to Excel" CausesValidation="false" />
				<asp:Label ID="lblsettings" runat="server" Text="Export" AssociatedControlID="imbexport"  CssClass="menutext" ToolTip="Export to Excel" />
			</span>
        </div>
        <div style="float:right;">
			<span class="menubarbutton">
				<asp:CheckBox ID="chkShowComplete" runat="server" ToolTip="Show Completed Projects" Text="Completed"
				    TextAlign="Left" AutoPostBack="true" />
			</span>
            <span class="menubarbutton">
                <asp:Label ID="lblWorkType" runat="server" Text="Type" AssociatedControlID="ddlTaskType"
			        CssClass="menutext" ToolTip="Filter by Type" />
                <asp:DropDownList ID="ddlTaskType" runat="server" ToolTip="Filter by Type" AutoPostBack="true">
                    <asp:ListItem Value="%" Text="All" Selected="True" />
                    <asp:ListItem Value="task" Text="Tasks" />
                    <asp:ListItem Value="issue" Text="Issues" />
                    <asp:ListItem Value="project" Text="Projects" />
                </asp:DropDownList>
            </span>
            <span class="menubarbutton">
                <asp:Label ID="lblPriorityFilter" runat="server" Text="Priority" AssociatedControlID="ddlPriorityFilter"
			        CssClass="menutext" ToolTip="Filter by Priority" />
                <asp:DropDownList ID="ddlPriorityFilter" runat="server" ToolTip="Filter by Priority" AutoPostBack="true" />
            </span>
            <span class="menubarbutton">
                <asp:Label ID="lblStatusFilter" runat="server" Text="Status" AssociatedControlID="ddlStatusFilter"
			        CssClass="menutext" ToolTip="Filter by Status" />
                <asp:DropDownList ID="ddlStatusFilter" runat="server" ToolTip="Filter by Status" AutoPostBack="true" />
            </span>
			<span class="menubarbutton">
				<asp:DropDownList ID="ddlPageSize" runat="server" ToolTip="Set the number of rows to return per page"
				    AutoPostBack="true">
					<asp:ListItem Text="10" Value="10" />
					<asp:ListItem Text="20" Value="20" Selected="True" />
					<asp:ListItem Text="50" Value="50" />
					<asp:ListItem Text="100" Value="100" />
				</asp:DropDownList>
				<asp:Label ID="lblPageSize" runat="server" Text="Page Size" AssociatedControlID="ddlPageSize"
					 CssClass="menutext" ToolTip="Set the number of rows to return per page" />
			</span>
        </div>
    </div>
    <div class="subTitle" style="clear: both;">Open Work Items</div>
    <asp:UpdatePanel ID="upMyView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="MyView_Grid" runat="server" DataKeyNames="Item_ID" CssClass="gridView"
                AllowPaging="True" AutoGenerateColumns="False" AllowSorting="true"
                DataSourceID="MyView_Data" EmptyDataText="You have no tasks or issues assigned"
                EnableModelValidation="True">
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <Columns>
                    <asp:BoundField DataField="Work_Type" HeaderText="Type" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-Width="80" SortExpression="work_type" />
                    <asp:TemplateField HeaderText="Project" SortExpression="Project">
                        <ItemTemplate>
                            <div style="width:447px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;">
                                <asp:LinkButton ID="btnviewproject" runat="server"
                                    Text='<%#Bind("Project")%>'
                                    CommandName='Project'
                                    CommandArgument='<%#Bind("Project_ID")%>'
                                    CausesValidation="false" CssClass="gridviewlinkbutton" />
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Title" SortExpression="title">
                        <ItemTemplate>
                            <div style="width:447px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;">
                                <asp:LinkButton ID="btnviewworkitem" runat="server"
                                    Text='<%#Bind("Title")%>'
                                    CommandName='<%#Bind("Work_Type")%>'
                                    CommandArgument='<%#Bind("Item_ID")%>'
                                    CausesValidation="false" CssClass="gridviewlinkbutton" />
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Due_Date" HeaderText="Due" SortExpression="Due_Date"
                        ItemStyle-HorizontalAlign="Center" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="160" />
                    <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-Width="160" SortExpression="Status" />
                    <asp:BoundField DataField="Priority" HeaderText="Priority" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-Width="160" SortExpression="Priority" />                        
                    <asp:BoundField DataField="Last_Update" HeaderText="Last Update" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-Width="160" SortExpression="Last_Update" />
                    <asp:TemplateField HeaderText="DEBUG" SortExpression="title" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblGridDebug" runat="server" Text="Debug" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
	<asp:SqlDataSource ID="MyView_Data" runat="server"
		ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
		SelectCommand="SELECT * FROM MySummary3 WHERE Person_ID = @Person_ID ORDER BY Due_Date DESC, Title"
        FilterExpression="Complete_bit <= {0} AND Status LIKE '{3}' AND Priority LIKE '{2}' AND Work_Type LIKE '{1}'">
        <SelectParameters>
            <asp:QueryStringParameter Name="Person_ID" QueryStringField="PersonID" Type="Int32" />
        </SelectParameters>
        <FilterParameters>
            <asp:ControlParameter ControlID="chkShowComplete" PropertyName="Checked" Name="Complete_bit" />
            <asp:ControlParameter ControlID="ddlTaskType" PropertyName="SelectedValue" Name="Work_type" />
            <asp:ControlParameter ControlID="ddlPriorityFilter" PropertyName="SelectedValue" Name="Priority" />
            <asp:ControlParameter ControlID="ddlStatusFilter" PropertyName="SelectedValue" Name="Status" />
        </FilterParameters>
    </asp:SqlDataSource>
</asp:Content>

