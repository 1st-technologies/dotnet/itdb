﻿Imports System.Data

Partial Class admin_ManageAlerts
    Inherits System.Web.UI.Page
    Dim dt As DataTable

    Protected Sub imbhome_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub btnsaveAlert_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsaveAlert.Click
        If Not Page.IsValid Then Exit Sub
        Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
            "INSERT INTO Alerts (Query, Alert, Body, Enabled) VALUES ('" & txtNewAlertQuery.Text.Replace("'", "''").Trim() & "', '" & txtNewAlertTitle.Text.Replace("'", "''").Trim() & "', '" & txtNewAlertBody.Text.Replace("'", "''").Trim() & "', 1); SELECT SCOPE_IDENTITY();")
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("ALERTOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            txtNewAlertTitle.Text.Replace("'", "''").Trim() _
        )
        alerts_Grid.DataBind()
        upalerts.Update()
    End Sub

    Protected Sub alerts_Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles alerts_Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ib1 As ImageButton = DirectCast(e.Row.Cells(4).Controls(2), ImageButton)
            If e.Row.RowState < DataControlRowState.Edit Then
                ib1.OnClientClick = "if (!confirm('Are you sure you want to delete this alert?')) return;"
            End If
        End If
    End Sub

    Protected Sub alerts_Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles alerts_Grid.RowDeleted
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("ALERTOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("DELETEACTIONID")), _
            e.Values("Alert").ToString() _
        )
    End Sub

    Protected Sub alerts_Grid_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles alerts_Grid.RowUpdated
        Dim intID = e.Keys("ID").ToString()
        Dim strNote = e.NewValues.Values
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("ALERTOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
            e.OldValues("Alert").ToString() _
        )
    End Sub
End Class
