﻿Imports System.Data

Partial Class admin_ManageReports
    Inherits System.Web.UI.Page
    Dim dt As DataTable

    Protected Sub imbhome_Click(sender As Object, e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub Reports_Grid_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles Reports_Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ib1 As ImageButton = DirectCast(e.Row.Cells(3).Controls(2), ImageButton)
            Dim intChildCount As Int32 = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "childnodecount").ToString())
            Dim intParentID As Int32 = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Parent_ID").ToString())
            Dim lbl1 As Label = DirectCast(e.Row.FindControl("lblGridDebug"), Label)

            If e.Row.RowState < DataControlRowState.Edit Then
                ib1.OnClientClick = "if (!confirm('Are you sure you want to delete this report?')) return;"
                ' Hide delete button if the object contains child nodes
                If intChildCount > 0 Then
                    ib1.Visible = False
                End If
            ElseIf e.Row.RowState >= DataControlRowState.Edit Then
                Dim ddl1 As DropDownList = DirectCast(e.Row.FindControl("ddlEditNode"), DropDownList)
                Dim intID As Int32 = DirectCast(Me.Reports_Grid.DataKeys(e.Row.RowIndex)("ID"), Int32)

                ' Add Root List Item to Node DropDownMenu
                Dim li2 As New ListItem()
                li2.Text = "Root"
                li2.Value = 0
                ddl1.Items.Add(li2)

                ' Add all nodes to the node dropdownlist except that of the currently editing row
                Dim dt As DataTable = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT * FROM Reports WHERE ID <> " & intID).Tables(0)
                For Each dr As DataRow In dt.Rows
                    Dim li1 As New ListItem()
                    li1.Text = dr("Title").ToString()
                    li1.Value = Convert.ToInt32(dr("ID").ToString())
                    ddl1.Items.Add(li1)
                    If Convert.ToInt32(dr("ID").ToString()) = intParentID Then
                        li1.Selected = True
                    End If
                Next
                dt.Dispose()
            End If
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ' Populate the add report node dropdownlist
            ddlNewReportNode.DataSource = common.fGetReports()
            ddlNewReportNode.DataTextField = "Title"
            ddlNewReportNode.DataValueField = "ID"
            ddlNewReportNode.DataBind()
            Dim li As New ListItem
            li.Value = 0
            li.Text = "Root"
            ddlNewReportNode.Items.Insert(0, li)
            ddlNewReportNode.Items.Insert(0, ConfigurationManager.AppSettings("NODEMENUEFAULT"))
        End If
    End Sub

    Protected Sub btnsaveReport_Click(sender As Object, e As EventArgs) Handles btnsaveReport.Click
        If Not Page.IsValid Then Exit Sub
        Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
            "INSERT INTO Reports (Parent_ID, Title, Query) VALUES ('" & ddlNewReportNode.SelectedItem.Value & "', '" & txtNewReportTitle.Text.Replace("'", "''").Trim() & "', '" & txtNewReportQuery.Text.Replace("'", "''").Trim() & "'); SELECT SCOPE_IDENTITY();")
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("REPORTTYPEOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            txtNewReportTitle.Text.Replace("'", "''").Trim() _
        )
        Reports_Grid.DataBind()
        upReports.Update()
    End Sub

    Protected Sub Reports_Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Reports_Grid.RowDeleted
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("REPORTTYPEOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("DELETEACTIONID")), _
            e.Values("Title").ToString() _
        )
    End Sub

    Protected Sub Reports_Grid_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Reports_Grid.RowUpdated
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("REPORTTYPEOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
            e.OldValues("Title").ToString() _
        )
    End Sub

    Protected Sub Reports_Grid_RowUpdating1(sender As Object, e As GridViewUpdateEventArgs) Handles Reports_Grid.RowUpdating
        ' change update parameter to the id of the node dropdownlist in the gridview
        Dim ddl1 As DropDownList = DirectCast(Reports_Grid.Rows(e.RowIndex).FindControl("ddlEditNode"), DropDownList)
        Dim ct1 As ControlParameter = DirectCast(Reports_Data.UpdateParameters.Item("ParentID"), ControlParameter)
        ct1.ControlID = ddl1.UniqueID.ToString()
    End Sub
End Class
