﻿
Partial Class admin_ManageStatus
    Inherits System.Web.UI.Page

    Protected Sub imbhome_Click(sender As Object, e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub btnsaveStatus_Click(sender As Object, e As EventArgs) Handles btnsaveStatus.Click
        If Not Page.IsValid Then Exit Sub
        Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
            "INSERT INTO [Status_Types] ([Status]) VALUES ('" & txtStatus.Text.Replace("'", "''").Trim() & "'); SELECT SCOPE_IDENTITY();")
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("STATUSTYPEOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            txtStatus.Text.Replace("'", "''").Trim() _
        )
        upStatus_Types.Update()
        Status_Grid.DataBind()
    End Sub

    Protected Sub Status_Grid_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Status_Grid.RowUpdated
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("STATUSTYPEOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
            e.OldValues("Status").ToString() _
        )
    End Sub
End Class
