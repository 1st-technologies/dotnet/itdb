﻿Imports System.Data

Partial Class admin_MAnagePeople
    Inherits System.Web.UI.Page
    Protected Sub imbhome_Click(sender As Object, e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub btnsaveperson_Click(sender As Object, e As EventArgs) Handles btnsaveperson.Click
        If Page.IsValid Then
            Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
                "INSERT INTO [People] ([email], [phone], [full_name], [enabled], Role_ID, login) VALUES ('" & txtpersonemail.Text.Replace("'", "''").Trim() & "', '" & txtpersonphone.Text & "', '" & txtpersonfullname.Text.Replace("'", "''").Trim() & "', 1, " & ddlPersonRole.SelectedValue & ", '" & txtPersonLogin.Text.Replace("'", "''").Trim() & "'); SELECT SCOPE_IDENTITY();")
            common.fLogActivity( _
                intID, _
                Convert.ToInt32(ConfigurationManager.AppSettings("PERSONOBJECTID")), _
                Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
                txtpersonfullname.Text.Replace("'", "''").Trim() _
            )
            upPeople.Update()
            People_Grid.DataBind()
        End If
    End Sub

    Protected Sub People_Grid_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles People_Grid.RowUpdated
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("PERSONOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
            "Enabled: " & e.NewValues("enabled").ToString() _
        )
    End Sub

    Protected Sub People_Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles People_Grid.RowUpdating
        Dim ddl1 As DropDownList = DirectCast(People_Grid.Rows(e.RowIndex).FindControl("ddlEditRole"), DropDownList)
        Dim ct1 As ControlParameter = DirectCast(People_Data.UpdateParameters.Item("Role_ID"), ControlParameter)
        ct1.ControlID = ddl1.UniqueID.ToString()
    End Sub

    Sub PeopleFullNameExistsCheck(ByVal sender As Object, ByVal args As ServerValidateEventArgs)
        For Each dr As DataRow In sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT full_name FROM People").Tables(0).Rows
            If dr(0).ToString.ToLower = args.Value Then
                args.IsValid = False
            End If
        Next
        If args.IsValid = False Then
            If ClientScript.IsClientScriptBlockRegistered("CustomValidation") = False Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "CustomValidation", "<script>alert('The specified full name already registered.');</script>")
            End If
        End If
    End Sub

    Sub PeopleEmailExistsCheck(ByVal sender As Object, ByVal args As ServerValidateEventArgs)
        For Each dr As DataRow In sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT email FROM People").Tables(0).Rows
            If dr(0).ToString.ToLower = args.Value Then
                args.IsValid = False
            End If
        Next
        If args.IsValid = False Then
            If ClientScript.IsClientScriptBlockRegistered("CustomValidation") = False Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "CustomValidation", "<script>alert('The specified email address already registered.');</script>")
            End If
        End If
    End Sub

    Sub PeopleLoginExistsCheck(ByVal sender As Object, ByVal args As ServerValidateEventArgs)
        For Each dr As DataRow In sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT login FROM People").Tables(0).Rows
            If dr(0).ToString.ToLower = args.Value Then
                args.IsValid = False
            End If
        Next
        If args.IsValid = False Then
            If ClientScript.IsClientScriptBlockRegistered("CustomValidation") = False Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "CustomValidation", "<script>alert('The specified login already registered.');</script>")
            End If
        End If
    End Sub
End Class