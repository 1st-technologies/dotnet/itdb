﻿<%@ Page Title="" Language="VB" MasterPageFile="~/admin/admin.master" AutoEventWireup="false" CodeFile="ManagePeople.aspx.vb"
    Inherits="admin_ManagePeople" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
        <span class="menubarbutton">
            <asp:ImageButton ID="imbnewperson" runat="server"
                AlternateText="Create a person" CausesValidation="false"
                ImageAlign="absMiddle" ImageUrl="~/images/Add_16x.png" />
            <asp:Label ID="lblnewperson" runat="server" Text="New Person" AssociatedControlID="imbnewperson" CssClass="menutext"
                ToolTip="Create a person" />
        </span>
    </div>
    <div class="subTitle">People</div>
    <asp:UpdatePanel ID="upPeople" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="People_Grid" runat="server" CssClass="gridView" AutoGenerateColumns="False"
                DataSourceID="People_Data" EnableModelValidation="True" DataKeyNames="ID" AllowPaging="True"
                PageSize="20" PagerSettings-Mode="Numeric" EmptyDataText="There are no people defined." >
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" Visible="true" ReadOnly="true" ItemStyle-Width="60" >
                    <ItemStyle Width="60px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Full Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtfull_name" runat="server" Text='<%# Bind("full_name")%>' MaxLength="128" CssClass="gridView_TextBox" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblfull_name" runat="server" Text='<%# Bind("full_name")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Phone">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtphone" runat="server" Text='<%# Bind("phone")%>' MaxLength="128" CssClass="gridView_TextBox" />
                            <cc1:MaskedEditExtender ID="meephone" runat="server" TargetControlID="txtphone" ClearMaskOnLostFocus="False"
                                MaskType="Number" Mask="(999) 999-9999" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblphone" runat="server" Text='<%# Bind("phone")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtemail" runat="server" Text='<%# Bind("email")%>' MaxLength="128" CssClass="gridView_TextBox" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblemail" runat="server" Text='<%# Bind("email")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Login">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLogin" runat="server" Text='<%# Bind("login")%>' MaxLength="64" CssClass="gridView_TextBox" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLogin" runat="server" Text='<%# Bind("login")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Role" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <div style="width:100px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;">
                                <asp:Label ID="lblReportNode" runat="server" Text='<%# Bind("Role")%>' />
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList id="ddlEditRole" runat="server"
                                CSSClass="gridView_TextBox"
                                DataTextField="Role"
                                DataValueField="ID"
                                DataSourceID="Role_Data"
                                SelectedValue='<%#Eval("Role_ID")%>' />
                        </EditItemTemplate>
                        <ItemStyle Width="100px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Enabled" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkPersonEnaled" runat="server" Checked='<%# Bind("enabled")%>' Enabled="false" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkPersonEnaled" runat="server" Checked='<%# Bind("enabled")%>' Enabled="true" />
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="True" ButtonType="Image" ItemStyle-Width="60"
                        CancelImageUrl="~/images/Undo_grey_16x.png"
                        EditImageUrl="~/images/Edit_grey_16x.png" ItemStyle-HorizontalAlign="Center"
                        UpdateImageUrl="~/images/Save_16x.png">
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel id="pnlNewPerson" runat="server" CssClass="modalpopup"
        Height="307px" Width="426px" style="display: none;" DefaultButton="btnSavePerson">
        <div id="newpersontitle" class="popuptitle">New Person</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:175px;" class="popuplabel">
                    <asp:Label id="lblpersonfullname" runat="server" Text="Full Name" AssociatedControlID="txtpersonfullname" />
                </td>
                <td style="width:10px;"></td>
                <td style="width:215px;">
                    <asp:TextBox ID="txtpersonfullname" runat="server" Width="195" ValidationGroup="newperson" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvpersonfullname" runat="server"
                        ControlToValidate="txtpersonfullname"
                        ErrorMessage="No full name was specified."
                        Display="None"
                        ValidationGroup="newperson" />&nbsp;

					<asp:CustomValidator ID="cvPeopleFullName" runat="server"
						ControlToValidate="txtpersonfullname"
						OnServerValidate="PeopleFullNameExistsCheck"
						Text="The specified full name already registered."
						ErrorMessage="The specified full name already registered."
						Display="None"
						ValidationGroup="newperson" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblpersonemail" runat="server" Text="Email" AssociatedControlID="txtpersonemail" />
                </td>
                <td></td>
                <td>
                    <asp:TextBox ID="txtpersonemail" runat="server" Width="195" ValidationGroup="newperson" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvpersonemail" runat="server"
                        ControlToValidate="txtpersonemail"
                        ErrorMessage="No email address was specified." Display="None" ValidationGroup="newperson" />&nbsp;

					<asp:RegularExpressionValidator ID="revpersonemail" runat="server"
						ControlToValidate="txtpersonemail"
						ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"
						ErrorMessage="The specified email address is invalid."
						Display="None"
						ValidationGroup="newperson" />

					<asp:CustomValidator ID="cvpersonemail" runat="server"
						ControlToValidate="txtpersonemail"
						OnServerValidate="PeopleEmailExistsCheck"
						Text="The specified email address already registered."
						ErrorMessage="The specified email address already registered."
						Display="None"
						ValidationGroup="newperson" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblpersonphone" runat="server" Text="Phone" AssociatedControlID="txtpersonphone" />
                </td>
                <td></td>
                <td>
                    <asp:TextBox ID="txtpersonphone" runat="server" Width="195" ValidationGroup="newperson" />
                    <cc1:MaskedEditExtender ID="meepersonphone" runat="server" TargetControlID="txtpersonphone"
                        ClearMaskOnLostFocus="False" MaskType="Number" Mask="(999) 999-9999" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rvfpersonphone" runat="server" ControlToValidate="txtpersonphone"
                        ErrorMessage="No phone number was specified." Display="None" ValidationGroup="newperson" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblPersonLogin" runat="server" Text="Login" AssociatedControlID="txtPersonLogin" />
                </td>
                <td></td>
                <td>
                    <asp:TextBox ID="txtPersonLogin" runat="server" Width="195" ValidationGroup="newperson" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvpersonlogin" runat="server" ControlToValidate="txtPersonLogin"
                        ErrorMessage="No login was specified." Display="None" ValidationGroup="newperson" />&nbsp;

					<asp:RegularExpressionValidator ID="revpersonlogin" runat="server"
						ControlToValidate="txtPersonLogin"
						ValidationExpression="^\w+\\\w+$"
						ErrorMessage="The specified login ID is invalid."
						Display="None"
						ValidationGroup="newperson" />

					<asp:CustomValidator ID="cvpersonlogin" runat="server"
						ControlToValidate="txtPersonLogin"
						OnServerValidate="PeopleLoginExistsCheck"
						Text="The specified login already registered."
						ErrorMessage="The specified login already registered."
						Display="None"
						ValidationGroup="newperson" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblPersonRole" runat="server" Text="Role" AssociatedControlID="ddlPersonRole" />
                </td>
                <td></td>
                <td>
                <asp:DropDownList id="ddlPersonRole" runat="server"
                    CSSClass="gridView_TextBox"
                    DataTextField="Role"
                    DataValueField="ID"
                    DataSourceID="Role_Data" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnsaveperson" runat="server" Text="Save" CssClass="tradewebbutton" ValidationGroup="newperson" />
                    &nbsp;&nbsp;
                    <asp:button ID="btncancelperson" runat="server" Text="Cancel" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:SqlDataSource ID="People_Data" runat="server" ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        DeleteCommand="DELETE FROM [People] WHERE [ID] = @ID"
        SelectCommand="SELECT People.ID AS ID, People.full_name AS full_name, People.Phone AS Phone, People.Email AS Email, People.Login As Login, People.Enabled AS Enabled, People.Role_ID AS Role_ID, Roles.Role AS Role FROM People INNER JOIN Roles ON People.Role_ID = Roles.ID ORDER BY full_name"
        UpdateCommand="UPDATE [People] SET [email] = @email, [phone] = @phone, [full_name] = @full_name, [Enabled] = @Enabled, Login = @Login, Role_ID = @Role_ID WHERE [ID] = @ID" >
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="email" Type="String" />
            <asp:Parameter Name="phone" Type="String" />
            <asp:Parameter Name="full_name" Type="String" />
            <asp:Parameter Name="ID" Type="Int32" />
            <asp:Parameter Name="Enabled" Type="Boolean" />
            <asp:Parameter Name="login" Type="String" />
            <asp:ControlParameter Name="Role_ID" Type="Int32"
                ControlID="ctl00$ContentPlaceHolder1$People_Grid$ctl03$ddlEditRole"
                PropertyName="SelectedValue" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Role_Data" runat="server" ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Roles ORDER BY Role">
    </asp:SqlDataSource>

    <cc1:ModalPopupExtender ID="mpeNewPerson" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewperson"
        PopupControlID="pnlNewPerson"
        CancelControlID="btncancelperson"
        DropShadow="true"
        PopupDragHandleControlID="newpersontitle" />

    <asp:ValidationSummary ID="vsPeople" runat="server"
        ShowMessageBox="true"
        ShowSummary="false"
        EnableClientScript="true" ValidationGroup="newperson" />
</asp:Content>

