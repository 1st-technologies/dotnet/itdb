﻿Imports System.Data

Partial Class admin_Utilities
    Inherits System.Web.UI.Page

    Protected Sub imbhome_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub btnReassign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReassign.Click
        If Page.IsValid Then
            sql.ExecuteCommand(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "UPDATE Projects SET Person_ID = " & ddlToUser.SelectedValue & " WHERE Person_ID = " & ddlFromUser.SelectedValue)
            sql.ExecuteCommand(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "UPDATE Tasks SET Person_ID = " & ddlToUser.SelectedValue & " WHERE Person_ID = " & ddlFromUser.SelectedValue)
            sql.ExecuteCommand(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "UPDATE Issues SET Person_ID = " & ddlToUser.SelectedValue & " WHERE Person_ID = " & ddlFromUser.SelectedValue)
            ddlFromUser.SelectedIndex = 0
            ddlToUser.SelectedIndex = 0
        End If
    End Sub


    Protected Sub btnMoveTask_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMoveTask.Click
        If Page.IsValid Then
            sql.ExecuteCommand(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "UPDATE Tasks SET Project_ID = " & ddlNewProjectT.SelectedValue & " WHERE ID = " & ddlTask.SelectedValue)
            ddlOldProjectT.SelectedIndex = 0
            ddlTask.SelectedIndex = 0
            ddlNewProjectT.SelectedIndex = 0
        End If
    End Sub

    Protected Sub btnMoveIssue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMoveIssue.Click
        If Page.IsValid Then
            sql.ExecuteCommand(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "UPDATE Issues SET Project_ID = " & ddlNewProjectI.SelectedValue & " WHERE ID = " & ddlIssue.SelectedValue)
            ddlOldProjectI.SelectedIndex = 0
            ddlIssue.SelectedIndex = 0
            ddlNewProjectI.SelectedIndex = 0
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            btnReassign.OnClientClick = "javascript:return confirm('Are you sure that you wish to proceed?');"
            btnMoveTask.OnClientClick = "javascript:return confirm('Are you sure that you wish to proceed?');"
            btnMoveIssue.OnClientClick = "javascript:return confirm('Are you sure that you wish to proceed?');"

            Dim ds As DataSet
            ds = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Project FROM Projects WHERE archive = 0 ORDER BY Project")

            With ddlFromUser
                .DataSource = common.fGetPeopleEnabled()
                .DataTextField = "full_name"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, New ListItem(ConfigurationManager.AppSettings("USERMENUDEFAULT"), 0))
            End With

            With ddlToUser
                .DataSource = common.fGetPeopleEnabled()
                .DataTextField = "full_name"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, New ListItem(ConfigurationManager.AppSettings("USERMENUDEFAULT"), 0))
            End With

            With ddlFromSelectUser
                .DataSource = common.fGetPeopleEnabled()
                .DataTextField = "full_name"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, New ListItem(ConfigurationManager.AppSettings("USERMENUDEFAULT"), 0))
            End With

            With ddlToSelectUser
                .DataSource = common.fGetPeopleEnabled()
                .DataTextField = "full_name"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, New ListItem(ConfigurationManager.AppSettings("USERMENUDEFAULT"), 0))
            End With

            With ddlOldProjectT
                .DataSource = ds
                .DataTextField = "Project"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, New ListItem(ConfigurationManager.AppSettings("PROJECTMENUDEFAULT"), 0))
            End With

            With ddlNewProjectT
                .DataSource = ds
                .DataTextField = "Project"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, New ListItem(ConfigurationManager.AppSettings("PROJECTMENUDEFAULT"), 0))
            End With

            With ddlOldProjectI
                .DataSource = ds
                .DataTextField = "Project"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, New ListItem(ConfigurationManager.AppSettings("PROJECTMENUDEFAULT"), 0))
            End With

            With ddlNewProjectI
                .DataSource = ds
                .DataTextField = "Project"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, New ListItem(ConfigurationManager.AppSettings("PROJECTMENUDEFAULT"), 0))
            End With

            ddlSelectTasks.Items.Insert(0, New ListItem("No Tasks", 0))
            ddlSelectTasks.Enabled = False

            ddlTask.Items.Insert(0, New ListItem("No Tasks", 0))
            ddlTask.Enabled = False

            ddlIssue.Items.Insert(0, New ListItem("No Issues", 0))
            ddlIssue.Enabled = False
        End If
    End Sub

    Protected Sub ddlOldProjectT_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOldProjectT.SelectedIndexChanged
        Dim ds As DataSet
        ds = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Task FROM Tasks WHERE Project_ID = " & ddlOldProjectT.SelectedValue & " ORDER BY Task")
        If ds.Tables(0).Rows.Count > 0 Then
            With ddlTask
                .Enabled = True
                .Items.Clear()
                .DataSource = ds
                .DataTextField = "Task"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, New ListItem("Select a Task", 0))
            End With
        Else
            ddlTask.Items.Clear()
            ddlTask.Items.Insert(0, New ListItem("No Tasks", 0))
            ddlTask.Enabled = False
        End If
    End Sub

    Protected Sub ddlOldProjectI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOldProjectI.SelectedIndexChanged
        Dim ds As DataSet
        ds = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID, Issue FROM Issues WHERE Project_ID = " & ddlOldProjectI.SelectedValue & " ORDER BY Issue")
        If ds.Tables(0).Rows.Count > 0 Then
            With ddlIssue
                .Enabled = True
                .Items.Clear()
                .DataSource = ds
                .DataTextField = "Issue"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, New ListItem("Select an Issue", 0))
            End With
        Else
            ddlIssue.Items.Clear()
            ddlIssue.Items.Insert(0, New ListItem("No Issues", 0))
            ddlIssue.Enabled = False
        End If
    End Sub

    Protected Sub ddlFromSelectUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFromSelectUser.SelectedIndexChanged
        Dim ds As DataSet
        ds = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT t.ID, t.Task FROM Tasks t INNER JOIN Projects p ON t.Project_ID = p.ID WHERE t.Person_ID = " & ddlFromSelectUser.SelectedValue & " AND p.archive = 0 ORDER BY t.Task")
        If ds.Tables(0).Rows.Count > 0 Then
            With ddlSelectTasks
                .Enabled = True
                .Items.Clear()
                .DataSource = ds
                .DataTextField = "Task"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, New ListItem("Select a Task", 0))
            End With
        Else
            ddlSelectTasks.Items.Clear()
            ddlSelectTasks.Items.Insert(0, New ListItem("No Tasks", 0))
            ddlSelectTasks.Enabled = False
        End If
    End Sub

    Protected Sub btnMoveSelectTask_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMoveSelectTask.Click
        If Page.IsValid Then
            sql.ExecuteCommand(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "UPDATE Tasks SET Person_ID = " & ddlToSelectUser.SelectedValue & " WHERE ID = " & ddlSelectTasks.SelectedValue)
            ddlFromSelectUser.SelectedIndex = 0
            ddlSelectTasks.SelectedIndex = 0
            ddlToSelectUser.SelectedIndex = 0
        End If
    End Sub
End Class
