﻿
Partial Class admin_ManagePlatforms
    Inherits System.Web.UI.Page
    Protected Sub imbhome_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub btnsavePlatform_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsavePlatform.Click
        If Not Page.IsValid Then Exit Sub
        Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
            "INSERT INTO Platforms (Platform) VALUES ('" & txtPlatform.Text.Replace("'", "''").Trim() & "'); SELECT SCOPE_IDENTITY();")
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("CATEGOROBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            txtPlatform.Text.Replace("'", "''").Trim() _
        )
        upPlatforms.Update()
        Platforms_Grid.DataBind()
    End Sub

    Protected Sub Platforms_Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Platforms_Grid.RowUpdating
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("CATEGOROBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
            e.OldValues("Platform").ToString() _
        )
    End Sub
End Class