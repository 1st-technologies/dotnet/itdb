﻿<%@ Page Title="" Language="VB" MasterPageFile="~/admin/admin.master" AutoEventWireup="false" CodeFile="ManageStatus.aspx.vb"
    Inherits="admin_ManageStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
        <span class="menubarbutton">
            <asp:ImageButton ID="imbnewStatus" runat="server"
                AlternateText="Create a Status" CausesValidation="false"
                ImageAlign="absMiddle" ImageUrl="~/images/Add_16x.png" />
            <asp:Label ID="lblnewStatus" runat="server" Text="New Status" AssociatedControlID="imbnewStatus" CssClass="menutext"
                ToolTip="Create a Status" />
        </span>
    </div>
    <div class="subTitle">Status Types</div>
    <asp:UpdatePanel ID="upStatus_Types" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="Status_Grid" runat="server" CssClass="gridView" AutoGenerateColumns="False"
                DataSourceID="Status_Data" EnableModelValidation="True" DataKeyNames="ID" AllowPaging="True"
                PageSize="20" PagerSettings-Mode="Numeric" EmptyDataText="There are no Statuses defined." >
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" Visible="true" ReadOnly="true" ItemStyle-Width="30" >
                    <ItemStyle Width="30px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Status">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtStatus" runat="server" Text='<%# Bind("status") %>' MaxLength="64" CssClass="gridView_TextBox" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("status") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="True" ButtonType="Image" ItemStyle-Width="60"
                        CancelImageUrl="~/images/Undo_grey_16x.png" EditImageUrl="~/images/Edit_grey_16x.png"
                        ItemStyle-HorizontalAlign="Center" UpdateImageUrl="~/images/Save_16x.png">
                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="text-align:center; background-color: red; color: white; font-size: x-small; height: 20px; line-height: 20px;">
        <span style="font-weight: bold;">Warning: </span><span>Any change to the order or purpose of status types must be coordinated with changes in the IIS admin console application settings or within the web.config file for this site!</span>
    </div>
    <asp:Panel id="pnlNewStatus" runat="server" CssClass="modalpopup"
        Height="150px" Width="426px" style="display: none;" DefaultButton="btnSaveStatus">
        <div id="newStatustitle" class="popuptitle">New Status</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:175px;" class="popuplabel">
                    <asp:Label id="lblStatus" runat="server" Text="Status" AssociatedControlID="txtStatus" />
                </td>
                <td style="width:10px;"></td>
                <td style="width:215px;">
                    <asp:TextBox ID="txtStatus" runat="server" Width="195" ValidationGroup="newStatus" MaxLength="64" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvStatus" runat="server" ControlToValidate="txtStatus"
                        ErrorMessage="Status" Display="None" ValidationGroup="newStatus" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnsaveStatus" runat="server" Text="Save" CssClass="tradewebbutton" ValidationGroup="newStatus" />
                    &nbsp;&nbsp;
                    <asp:button ID="btncancelStatus" runat="server" Text="Cancel" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:SqlDataSource ID="Status_Data" runat="server" ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM [Status_Types]"
        UpdateCommand="UPDATE [Status_Types] SET [Status] = @Status WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Status" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <cc1:ModalPopupExtender ID="mpeNewStatus" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewStatus"
        PopupControlID="pnlNewStatus"
        CancelControlID="btncancelStatus"
        DropShadow="true"
        PopupDragHandleControlID="newStatustitle" />

    <asp:ValidationSummary ID="vsStatus" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newStatus" />
</asp:Content>

