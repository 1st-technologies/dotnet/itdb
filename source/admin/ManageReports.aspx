﻿<%@ Page Title="" Language="VB" MasterPageFile="~/admin/admin.master" AutoEventWireup="false" CodeFile="ManageReports.aspx.vb"
    Inherits="admin_ManageReports" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
        <span class="menubarbutton">
            <asp:ImageButton ID="imbnewreport" runat="server"
                AlternateText="Create a report" CausesValidation="false"
                ImageAlign="absMiddle" ImageUrl="~/images/Add_16x.png" />
            <asp:Label ID="lblnewreport" runat="server" Text="New Report" AssociatedControlID="imbnewreport" CssClass="menutext"
                ToolTip="Create a Report" />
        </span>
    </div>
    <div class="subTitle">Reports</div>
    <asp:UpdatePanel ID="upReports" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="Reports_Grid" runat="server" CssClass="gridView" AutoGenerateColumns="False"
                DataSourceID="Reports_Data" EnableModelValidation="True" DataKeyNames="ID" AllowPaging="True"
                PageSize="20" PagerSettings-Mode="Numeric" EmptyDataText="There are no reports defined." >
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <Columns>
                    <asp:TemplateField HeaderText="Title" ItemStyle-Width="200px">
                        <ItemTemplate>
                            <div style="width:200px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;">
                                <asp:Label ID="lblReportTitle" runat="server" Text='<%# Bind("Title")%>' />
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtReportTitle" runat="server" Text='<%# Bind("Title")%>' CssClass="gridView_TextBox"
                                MaxLength="64" />
                        </EditItemTemplate>
                        <ItemStyle Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Query">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtReportQuery" runat="server" TextMode="MultiLine" Text='<%# Bind("Query")%>'
                                Height="50" CssClass="gridView_TextBox" MaxLength="4000" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <div style="word-wrap: normal; overflow-y: auto; height: 50px;">
                                <%#System.Web.HttpUtility.HtmlEncode(Eval("Query").ToString())%>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Node" ItemStyle-Width="200px">
                        <ItemTemplate>
                            <div style="width:200px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;">
                                <asp:Label ID="lblReportNode" runat="server" Text='<%# Bind("Parent_Text")%>' />
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList id="ddlEditNode" runat="server"  CSSClass="gridView_TextBox" DataValueField="ID" />
                        </EditItemTemplate>
                        <ItemStyle Width="200px" />
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="True" ShowDeleteButton="true" ShowCancelButton="true"
                        ButtonType="Image" ItemStyle-Width="60"
                        CancelImageUrl="~/images/Undo_grey_16x.png" EditImageUrl="~/images/Edit_grey_16x.png"
                        DeleteImageUrl="~/images/delete_16.png"
                        ItemStyle-HorizontalAlign="Center" UpdateImageUrl="~/images/Save_16x.png">
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:CommandField>
                    <asp:BoundField DataField="childnodecount" HeaderText="Child Node Count" Visible="false" ReadOnly="true"
                        ItemStyle-Width="30" >
                    <ItemStyle Width="30px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Debug" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblGridDebug" runat="server" Text="Grid Debug" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel id="pnlNewReport" runat="server" CssClass="modalpopup"
        Height="410px" Width="426px" style="display: none;" DefaultButton="btnSaveReport">
        <div id="newReporttitle" class="popuptitle">New Report/Node</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:400px; text-align: center;" class="popuplabel" colspan="3">
                    <asp:Label id="lblNewReportTitle" runat="server" Text="Title" AssociatedControlID="txtNewReportTitle" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:TextBox ID="txtNewReportTitle" runat="server" Width="398" ValidationGroup="newreport" MaxLength="64" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvNewReportTitle" runat="server" ControlToValidate="txtNewReportTitle"
                        ErrorMessage="Title" Display="None" ValidationGroup="newreport" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:400px; text-align: center;" class="popuplabel" colspan="3">
                    <asp:Label id="lblNewReportQuery" runat="server" Text="Query" AssociatedControlID="txtNewReportQuery" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:TextBox ID="txtNewReportQuery" runat="server" Width="398" TextMode="MultiLine" Rows="11"
                        ValidationGroup="newreport" MaxLength="4000" CausesValidation="false" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblNewReportNode" runat="server" Text="Node" AssociatedControlID="ddlNewReportNode" />
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlNewReportNode" runat="server" Width="310" ValidationGroup="newreport" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlNewReportNode" ErrorMessage="Node"
                        InitialValue="<%$ AppSettings: NODEMENUEFAULT %>" Display="None" ValidationGroup="newreport" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnsaveReport" runat="server" Text="Save" CssClass="tradewebbutton" ValidationGroup="newreport"
                        PostBackUrl="~/admin/ManageReports.aspx" />
                    &nbsp;&nbsp;
                    <asp:button ID="btncancelReport" runat="server" Text="Cancel" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:SqlDataSource ID="Reports_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT ID, Parent_ID, ISNULL((SELECT Title FROM Reports WHERE ID = r.Parent_ID), 'Root') AS Parent_Text, Title, ISNULL(Query, '') AS Query, (SELECT COUNT(*) FROM Reports WHERE Parent_ID = r.id) childnodecount FROM Reports r ORDER BY Title"
        DeleteCommand="DELETE FROM Reports WHERE ID = @ID"
        UpdateCommand="UPDATE [Reports] SET [Parent_ID] = @ParentID, [Title] = @Title, [Query] = @Query WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="ID" Type="Int32" />
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Query" Type="String" />
            <asp:ControlParameter Name="ParentID" Type="Int32" ControlID="ctl00$ContentPlaceHolder1$Reports_Grid$ctl02$ddlEditNode"
                PropertyName="SelectedValue" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <cc1:ModalPopupExtender ID="mpeNewReport" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewreport"
        PopupControlID="pnlNewReport"
        CancelControlID="btncancelReport"
        DropShadow="true"
        PopupDragHandleControlID="newReportTitle" />

    <asp:ValidationSummary ID="vsReports" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newreport" />
</asp:Content>

