﻿<%@ Page Title="" Language="VB" MasterPageFile="~/admin/admin.master" AutoEventWireup="false" CodeFile="ManageRegions.aspx.vb"
    Inherits="admin_ManageRegions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
        <span class="menubarbutton">
            <asp:ImageButton ID="imbnewRegion" runat="server"
                AlternateText="Create a Region" CausesValidation="false"
                ImageAlign="absMiddle" ImageUrl="~/images/Add_16x.png" />
            <asp:Label ID="lblnewRegion" runat="server" Text="New Region" AssociatedControlID="imbnewRegion" CssClass="menutext"
                ToolTip="Create a Region" />
        </span>
    </div>
    <div class="subTitle">Regions</div>
    <asp:UpdatePanel ID="upRegions" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="Regions_Grid" runat="server" CssClass="gridView" AutoGenerateColumns="False"
                DataSourceID="Regions_Data" EnableModelValidation="True" DataKeyNames="ID" AllowPaging="True"
                PageSize="20" PagerSettings-Mode="Numeric" EmptyDataText="There are no Regions defined." >
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" Visible="true" ReadOnly="true" ItemStyle-Width="30" >
                    <ItemStyle Width="30px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Region">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRegion" runat="server" Text='<%# Bind("Region")%>' MaxLength="64" CssClass="gridView_TextBox" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRegion" runat="server" Text='<%# Bind("Region")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowDeleteButton="false" ShowEditButton="True" ButtonType="Image" ItemStyle-Width="60"
                        CancelImageUrl="~/images/Undo_grey_16x.png" EditImageUrl="~/images/Edit_grey_16x.png"
                        UpdateImageUrl="~/images/Edit_grey_16x.png" ItemStyle-HorizontalAlign="Center">
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel id="pnlNewRegion" runat="server" CssClass="modalpopup"
        Height="150px" Width="426px" style="display: none;" DefaultButton="btnSaveRegion">
        <div id="newRegiontitle" class="popuptitle">New Region</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:175px;" class="popuplabel">
                    <asp:Label id="lblRegion" runat="server" Text="Region" AssociatedControlID="txtRegion" />
                </td>
                <td style="width:10px;"></td>
                <td style="width:215px;">
                    <asp:TextBox ID="txtRegion" runat="server" Width="195" ValidationGroup="newRegion" MaxLength="64" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvRegion" runat="server" ControlToValidate="txtRegion"
                        ErrorMessage="Region" Display="None" ValidationGroup="newRegion" />&nbsp;

                    <asp:CustomValidator ID="cvRegion" runat="server"
						ControlToValidate="txtRegion"
						OnServerValidate="RegionExistsCheck"
						Text="The specified Region is already registered."
						ErrorMessage="The specified Region is already registered."
						Display="None"
						ValidationGroup="newRegion" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnsaveRegion" runat="server" Text="Save" CssClass="tradewebbutton" ValidationGroup="newRegion" />
                    &nbsp;&nbsp;
                    <asp:button ID="btncancelRegion" runat="server" Text="Cancel" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:SqlDataSource ID="Regions_Data" runat="server" ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM [Regions]"
        UpdateCommand="UPDATE [Regions] SET [Region] = @Region WHERE [ID] = @ID" >
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Region" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <cc1:ModalPopupExtender ID="mpeNewRegion" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewRegion"
        PopupControlID="pnlNewRegion"
        CancelControlID="btncancelRegion"
        DropShadow="true"
        PopupDragHandleControlID="newRegiontitle" />

    <asp:ValidationSummary ID="vsRegions" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newRegion" />
</asp:Content>

