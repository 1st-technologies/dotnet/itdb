﻿<%@ Page Language="VB" MasterPageFile="~/admin/admin.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="admin_default" title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" style="width: 16px"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
    </div>
    <div class="subTitle">Settings</div>
    <div style="margin: 40px; text-align: center;">Setting home page for ITDB. Select a configuration item above.</div>
</asp:Content>

