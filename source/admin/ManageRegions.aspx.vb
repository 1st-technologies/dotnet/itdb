﻿Imports System.Data

Partial Class admin_ManageRegions
	Inherits System.Web.UI.Page
	Protected Sub imbhome_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbhome.Click
		Response.Redirect("/", False)
	End Sub

	Protected Sub btnsaveRegion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsaveRegion.Click
        If Not Page.IsValid Then

            Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
                "INSERT INTO Regions (Region) VALUES ('" & txtRegion.Text.Trim() & "'); SELECT SCOPE_IDENTITY();")
            common.fLogActivity( _
                intID, _
                Convert.ToInt32(ConfigurationManager.AppSettings("REGIONOBJECTID")), _
                Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
                txtRegion.Text.Replace("'", "''").Trim() _
            )
            upRegions.Update()
            Regions_Grid.DataBind()
        End If
    End Sub

    Sub RegionExistsCheck(ByVal sender As Object, ByVal args As ServerValidateEventArgs)
        For Each dr As DataRow In sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT Region FROM Regions").Tables(0).Rows
            If dr(0).ToString.ToLower = args.Value.ToLower Then
                args.IsValid = False
            End If
        Next
        If args.IsValid = False Then
            If ClientScript.IsClientScriptBlockRegistered("CustomValidation") = False Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "CustomValidation", "<script>alert('The specified Region is already registered.');</script>")
            End If
        End If
    End Sub

	Protected Sub Regions_Grid_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Regions_Grid.RowUpdated
		Dim intID = e.Keys("ID").ToString()
		common.fLogActivity( _
			intID, _
			Convert.ToInt32(ConfigurationManager.AppSettings("REGIONOBJECTID")), _
			Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
			e.OldValues("Region").ToString() _
		)
	End Sub
End Class