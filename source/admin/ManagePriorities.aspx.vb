﻿
Partial Class admin_ManagePriorities
    Inherits System.Web.UI.Page

    Protected Sub imbhome_Click(sender As Object, e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub btnsavePriority_Click(sender As Object, e As EventArgs) Handles btnsavePriority.Click
        If Not Page.IsValid Then Exit Sub
        Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
            "INSERT INTO [Priority_Types] ([Priority]) VALUES ('" & txtPriority.Text.Replace("'", "''").Trim() & "'); SELECT SCOPE_IDENTITY();")
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("PRIORITYOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            txtPriority.Text.Replace("'", "''").Trim() _
        )
        upPriorities.Update()
        Priorities_Data.DataBind()
    End Sub

    Protected Sub Priorities_Grid_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Priorities_Grid.RowUpdated
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("PRIORITYOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
            e.OldValues("Priority").ToString() _
        )
    End Sub
End Class
