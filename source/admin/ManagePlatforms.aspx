﻿<%@ Page Title="" Language="VB" MasterPageFile="~/admin/admin.master" AutoEventWireup="false" CodeFile="ManagePlatforms.aspx.vb"
    Inherits="admin_ManagePlatforms" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
        <span class="menubarbutton">
            <asp:ImageButton ID="imbnewPlatform" runat="server"
                AlternateText="Create a Platform" CausesValidation="false"
                ImageAlign="absMiddle" ImageUrl="~/images/Add_16x.png" />
            <asp:Label ID="lblnewPlatform" runat="server" Text="New Platform" AssociatedControlID="imbnewPlatform" CssClass="menutext"
                ToolTip="Create a Platform" />
        </span>
    </div>
    <div class="subTitle">Platforms</div>
    <asp:UpdatePanel ID="upPlatforms" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="Platforms_Grid" runat="server" CssClass="gridView" AutoGenerateColumns="False"
                DataSourceID="Platforms_Data" EnableModelValidation="True" DataKeyNames="ID" AllowPaging="True"
                PageSize="20" PagerSettings-Mode="Numeric" EmptyDataText="There are no platforms defined." >
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" Visible="true" ReadOnly="true" ItemStyle-Width="30" >
                    <ItemStyle Width="30px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Platform">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPlatform" runat="server" Text='<%# Bind("Platform")%>' MaxLength="64" CssClass="gridView_TextBox" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPlatform" runat="server" Text='<%# Bind("Platform")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowDeleteButton="false" ShowEditButton="True" ButtonType="Image" ItemStyle-Width="60"
                        CancelImageUrl="~/images/Undo_grey_16x.png" EditImageUrl="~/images/Edit_grey_16x.png"
                        UpdateImageUrl="~/images/Save_16x.png" ItemStyle-HorizontalAlign="Center">
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel id="pnlNewPlatform" runat="server" CssClass="modalpopup"
        Height="150px" Width="426px" style="display: none;" DefaultButton="btnSavePlatform">
        <div id="newPlatformtitle" class="popuptitle">New Platform</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:175px;" class="popuplabel">
                    <asp:Label id="lblPlatform" runat="server" Text="Platform" AssociatedControlID="txtPlatform" />
                </td>
                <td style="width:10px;"></td>
                <td style="width:215px;">
                    <asp:TextBox ID="txtPlatform" runat="server" Width="195" ValidationGroup="newPlatform" MaxLength="64" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvPlatform" runat="server" ControlToValidate="txtPlatform"
                        ErrorMessage="Platform" Display="None" ValidationGroup="newPlatform" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnsavePlatform" runat="server" Text="Save" CssClass="tradewebbutton" ValidationGroup="newPlatform" />
                    &nbsp;&nbsp;
                    <asp:button ID="btncancelPlatform" runat="server" Text="Cancel" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:SqlDataSource ID="Platforms_Data" runat="server" ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM [Platforms]"
        UpdateCommand="UPDATE [Platforms] SET [Platform] = @Platform WHERE [ID] = @ID" >
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Platform" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <cc1:ModalPopupExtender ID="mpeNewPlatform" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewPlatform"
        PopupControlID="pnlNewPlatform"
        CancelControlID="btncancelPlatform"
        DropShadow="true"
        PopupDragHandleControlID="newPlatformtitle" />

    <asp:ValidationSummary ID="vsPlatforms" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newPlatform" />
</asp:Content>