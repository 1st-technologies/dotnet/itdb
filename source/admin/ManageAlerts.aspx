﻿<%@ Page Title="" Language="VB" MasterPageFile="~/admin/admin.master" AutoEventWireup="false" CodeFile="ManageAlerts.aspx.vb"
    Inherits="admin_ManageAlerts" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
        <span class="menubarbutton">
            <asp:ImageButton ID="imbnewalert" runat="server"
                AlternateText="Create a alert" CausesValidation="false"
                ImageAlign="absMiddle" ImageUrl="~/images/Add_16x.png" />
            <asp:Label ID="lblnewAlert" runat="server" Text="New Alert" AssociatedControlID="imbnewAlert" CssClass="menutext"
                ToolTip="Create an Alert" />
        </span>
    </div>
    <div class="subTitle">Alerts</div>
    <asp:UpdatePanel ID="upalerts" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="alerts_Grid" runat="server" CssClass="gridView" AutoGenerateColumns="False"
                DataSourceID="alerts_Data" EnableModelValidation="True" DataKeyNames="ID" AllowPaging="True"
                PageSize="20" PagerSettings-Mode="Numeric" EmptyDataText="There are no alerts defined." >
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <Columns>
                    <asp:TemplateField HeaderText="Title" ItemStyle-Width="200px">
                        <ItemTemplate>
                            <div style="width:200px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;">
                                <asp:Label ID="lblAlertTitle" runat="server" Text='<%# Bind("Alert")%>' />
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAlertTitle" runat="server" Text='<%# Bind("Alert")%>' CssClass="gridView_TextBox"
                                MaxLength="64" />
                        </EditItemTemplate>
                        <ItemStyle Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Query">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAlertQuery" runat="server" TextMode="MultiLine" Text='<%# Bind("Query")%>'
                                Height="50" CssClass="gridView_TextBox" MaxLength="4000" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <div style="word-wrap: normal; overflow-y: auto; height: 50px;">
                                <%#System.Web.HttpUtility.HtmlEncode(Eval("Query").ToString())%>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Body">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAlertBody" runat="server" TextMode="MultiLine" Text='<%# Bind("Body")%>'
                                Height="50" CssClass="gridView_TextBox" MaxLength="4000" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <div style="word-wrap: normal; overflow-y: auto; height: 50px;">
                                <%#System.Web.HttpUtility.HtmlEncode(Eval("Body").ToString())%>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Enabled" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkAlertEnaled" runat="server" Checked='<%# Bind("enabled")%>' Enabled="false" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkAlertEnaled" runat="server" Checked='<%# Bind("enabled")%>' Enabled="true" />
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="True" ShowDeleteButton="true" ShowCancelButton="true"
                        ButtonType="Image" ItemStyle-Width="60"
                        CancelImageUrl="~/images/Undo_grey_16x.png" EditImageUrl="~/images/Edit_grey_16x.png"
                        DeleteImageUrl="~/images/delete_16.png"
                        ItemStyle-HorizontalAlign="Center" UpdateImageUrl="~/images/Save_16x.png">
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel id="pnlNewAlert" runat="server" CssClass="modalpopup"
        Height="590px" Width="426px" style="display: none;" DefaultButton="btnSaveAlert">
        <div id="newAlerttitle" class="popuptitle">New Alert</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:400px; text-align: center;" class="popuplabel" colspan="3">
                    <asp:Label id="lblNewAlertTitle" runat="server" Text="Title" AssociatedControlID="txtNewAlertTitle" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:TextBox ID="txtNewAlertTitle" runat="server" Width="398" ValidationGroup="newAlert" MaxLength="64" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvNewAlertTitle" runat="server" ControlToValidate="txtNewAlertTitle"
                        ErrorMessage="Title" Display="None" ValidationGroup="newAlert" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:400px; text-align: center;" class="popuplabel" colspan="3">
                    <asp:Label id="lblNewAlertQuery" runat="server" Text="Query" AssociatedControlID="txtNewAlertQuery" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:TextBox ID="txtNewAlertQuery" runat="server" Width="398" TextMode="MultiLine" Rows="11"
                        ValidationGroup="newAlert" MaxLength="4000" CausesValidation="false" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvNewAlertQuery" runat="server" ControlToValidate="txtNewAlertQuery"
                        ErrorMessage="Query" Display="None" ValidationGroup="newAlert" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:400px; text-align: center;" class="popuplabel" colspan="3">
                    <asp:Label id="lblNewAlertBody" runat="server" Text="Body" AssociatedControlID="txtNewAlertBody" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:TextBox ID="txtNewAlertBody" runat="server" Width="398" TextMode="MultiLine" Rows="11"
                        ValidationGroup="newAlert" MaxLength="4000" CausesValidation="false" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvNewAlertBody" runat="server" ControlToValidate="txtNewAlertBody"
                        ErrorMessage="Body" Display="None" ValidationGroup="newAlert" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnsaveAlert" runat="server" Text="Save" CssClass="tradewebbutton" ValidationGroup="newAlert"
                        PostBackUrl="~/admin/ManageAlerts.aspx" />
                    &nbsp;&nbsp;
                    <asp:button ID="btncancelAlert" runat="server" Text="Cancel" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:SqlDataSource ID="alerts_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM alerts"
        DeleteCommand="DELETE FROM Alerts WHERE ID = @ID"
        UpdateCommand="UPDATE [Alerts] SET Alert = @Alert, Query = @Query, Body = @Body, Enabled = @Enabled WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="ID" Type="Int32" />
            <asp:Parameter Name="Alert" Type="String" />
            <asp:Parameter Name="Query" Type="String" />
            <asp:Parameter Name="Body" Type="String" />
            <asp:Parameter Name="Enabled" Type="Boolean" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <cc1:ModalPopupExtender ID="mpeNewAlert" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewAlert"
        PopupControlID="pnlNewAlert"
        CancelControlID="btncancelAlert"
        DropShadow="true"
        PopupDragHandleControlID="newAlertTitle" />

    <asp:ValidationSummary ID="vsalerts" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newalert" />
</asp:Content>

