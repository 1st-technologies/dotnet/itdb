﻿<%@ Page Language="VB" MasterPageFile="~/admin/admin.master" AutoEventWireup="false" CodeFile="Activity.aspx.vb" Inherits="admin_Activity" title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" style="width: 16px"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
    </div>
    <div class="subTitle">Activity</div>
    <asp:UpdatePanel ID="upActivity" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="Activity_Grid" runat="server" CssClass="gridView" AutoGenerateColumns="false"
                DataSourceID="Activity_Data" DataKeyNames="ID" AllowPaging="True" AllowSorting="true"
                PageSize="20" PagerSettings-Mode="Numeric" EmptyDataText="There is no activity." >
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <Columns>
                    <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Start_Date"
                        ItemStyle-HorizontalAlign="Center" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="80" />
                    <asp:BoundField DataField="Type" HeaderText="Type" ItemStyle-Width="160" SortExpression="Type" />
                    <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="60" SortExpression="ID" />
                    <asp:BoundField DataField="Action" HeaderText="Action" ItemStyle-Width="80" SortExpression="Action" />
                    <asp:BoundField DataField="User" HeaderText="User" ItemStyle-Width="160"  SortExpression="User"/>
                    <asp:TemplateField HeaderText="Notes">
                        <ItemTemplate>
                            <div style="width: 100%;">
								<%#Eval("Notes")%>
							</div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:SqlDataSource ID="Activity_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM ActivitySummary ORDER BY Date DESC">
    </asp:SqlDataSource>
</asp:Content>

