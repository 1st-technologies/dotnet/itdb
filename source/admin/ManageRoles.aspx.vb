﻿Imports System.Data

Partial Class admin_ManageRoles
    Inherits System.Web.UI.Page
    Protected Sub imbhome_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub btnsaveRole_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsaveRole.Click
        If Not Page.IsValid Then
            Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
                "INSERT INTO Roles (Role) VALUES ('" & txtRole.Text.Replace("'", "''").Trim() & "'); SELECT SCOPE_IDENTITY();")
            common.fLogActivity( _
                intID, _
                Convert.ToInt32(ConfigurationManager.AppSettings("ROLEOBJECTID")), _
                Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
                txtRole.Text.Replace("'", "''").Trim() _
            )
            upRoles.Update()
            Roles_Grid.DataBind()
        End If
    End Sub

    Sub RoleExistsCheck(ByVal sender As Object, ByVal args As ServerValidateEventArgs)
        For Each dr As DataRow In sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT role FROM Roles").Tables(0).Rows
            If dr(0).ToString.ToLower = args.Value.ToLower Then
                args.IsValid = False
            End If
        Next
        If args.IsValid = False Then
            If ClientScript.IsClientScriptBlockRegistered("CustomValidation") = False Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "CustomValidation", "<script>alert('The specified role is already registered.');</script>")
            End If
        End If
    End Sub

    Protected Sub Roles_Grid_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Roles_Grid.RowUpdated
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("ROLEOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
            e.OldValues("Role").ToString() _
        )
    End Sub
End Class