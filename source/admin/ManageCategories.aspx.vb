﻿
Partial Class admin_ManageCategories
    Inherits System.Web.UI.Page
    Protected Sub imbhome_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub btnsaveCategory_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsaveCategory.Click
        If Not Page.IsValid Then Exit Sub
        Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
            "INSERT INTO Categories (Category) VALUES ('" & txtCategory.Text.Replace("'", "''").Trim() & "'); SELECT SCOPE_IDENTITY();")
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("CATEGORYOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            txtCategory.Text.Replace("'", "''").Trim() _
        )
        upCategories.Update()
        Category_Grid.DataBind()
    End Sub

    Protected Sub Category_Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Category_Grid.RowUpdating
        Dim intID = e.Keys("ID").ToString()
        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("CATEGORYOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("UPDATEACTIONID")), _
            e.OldValues("Category").ToString() _
        )
    End Sub
End Class