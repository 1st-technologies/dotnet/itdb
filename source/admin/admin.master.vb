﻿
Partial Class admin_admin
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ' DEVELOEPRER'S NOTE: The code below is a work-around for known bug in ASP menu control
        ' credit: http://shermanstechnicalblog.blogspot.com/2012/09/one-possible-solution-to-problem-of.html
        Dim currentPage As String = Page.AppRelativeVirtualPath
        For Each item As MenuItem In Menu1.Items
            If item.NavigateUrl = currentPage Then
                item.Selected = True
            End If
        Next
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        ' Code to handle browser specific CSS.
        ' NOTE: IE 10+ no longer support CSS rules <!--[if IE x]>
        Dim strBrowser = Request.Browser.Browser.ToString()
        Dim strBrowserVersion = Request.Browser.MajorVersion.ToString()
        Dim css As New HtmlLink()

        lblUserAgent.Text = "Browser: " & Request.Browser.Browser.ToString() & "<br />" & _
            "Version: " & Request.Browser.Version().ToString() & "<br />" & _
            "MajorVersion: " & Request.Browser.MajorVersion.ToString() & "<br />" & _
            "MinorVersion: " & Request.Browser.MinorVersion.ToString() & "<br />" & _
            "MinorVersionString: " & Request.Browser.MinorVersionString.ToString() & "<br />" & _
            "Type: " & Request.Browser.Type.ToString() & "<br />"
        Select Case strBrowser
            Case "Chrome" ' NOTE: Also applies to Opera Browser
                Response.Write("apply chrome css")
                css.Href = ResolveClientUrl("/css/chrome.css")
                css.Attributes("rel") = "stylesheet"
                css.Attributes("type") = "text/css"
                css.Attributes("media") = "all"
                Page.Header.Controls.Add(css)
            Case "Firefox"
                css.Href = ResolveClientUrl("/css/firefox.css")
                css.Attributes("rel") = "stylesheet"
                css.Attributes("type") = "text/css"
                css.Attributes("media") = "all"
                Page.Header.Controls.Add(css)
            Case "Safari"
                css.Href = ResolveClientUrl("/css/safari.css")
                css.Attributes("rel") = "stylesheet"
                css.Attributes("type") = "text/css"
                css.Attributes("media") = "all"
                Page.Header.Controls.Add(css)
            Case "IE", "InternetExplorer"
                Select Case strBrowserVersion
                    Case "7"
                        css.Href = ResolveClientUrl("/css/ie7.css")
                        css.Attributes("rel") = "stylesheet"
                        css.Attributes("type") = "text/css"
                        css.Attributes("media") = "all"
                        Page.Header.Controls.Add(css)
                    Case "8"
                        css.Href = ResolveClientUrl("/css/ie8.css")
                        css.Attributes("rel") = "stylesheet"
                        css.Attributes("type") = "text/css"
                        css.Attributes("media") = "all"
                        Page.Header.Controls.Add(css)
                    Case "9"
                        css.Href = ResolveClientUrl("/css/ie9.css")
                        css.Attributes("rel") = "stylesheet"
                        css.Attributes("type") = "text/css"
                        css.Attributes("media") = "all"
                        Page.Header.Controls.Add(css)
                    Case "10"
                        css.Href = ResolveClientUrl("/css/ie10.css")
                        css.Attributes("rel") = "stylesheet"
                        css.Attributes("type") = "text/css"
                        css.Attributes("media") = "all"
                        Page.Header.Controls.Add(css)
                    Case "11"
                        css.Href = ResolveClientUrl("/css/ie11.css")
                        css.Attributes("rel") = "stylesheet"
                        css.Attributes("type") = "text/css"
                        css.Attributes("media") = "all"
                        Page.Header.Controls.Add(css)
                End Select
        End Select
    End Sub
End Class

