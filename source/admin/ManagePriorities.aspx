﻿<%@ Page Title="" Language="VB" MasterPageFile="~/admin/admin.master" AutoEventWireup="false" CodeFile="ManagePriorities.aspx.vb"
    Inherits="admin_ManagePriorities" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
        <span class="menubarbutton">
            <asp:ImageButton ID="imbnewpriority" runat="server"
                AlternateText="Create a priority" CausesValidation="false"
                ImageAlign="absMiddle" ImageUrl="~/images/Add_16x.png" />
            <asp:Label ID="lblnewpriority" runat="server" Text="New Priority" AssociatedControlID="imbnewPriority" CssClass="menutext"
                ToolTip="Create a Priority" />
        </span>
    </div>
    <div class="subTitle">Priorities</div>
    <asp:UpdatePanel ID="upPriorities" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="Priorities_Grid" runat="server" CssClass="gridView" AutoGenerateColumns="False"
                DataSourceID="Priorities_Data" EnableModelValidation="True" DataKeyNames="ID" AllowPaging="True"
                PageSize="20" PagerSettings-Mode="Numeric" EmptyDataText="There are no priorities defined." >
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" Visible="true" ReadOnly="true" ItemStyle-Width="30" >
                    <ItemStyle Width="30px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Priority">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPriority" runat="server" Text='<%# Bind("Priority")%>' MaxLength="64"
                                CssClass="gridView_TextBox" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPriority" runat="server" Text='<%# Bind("Priority")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowDeleteButton="false" ShowEditButton="True" ButtonType="Image" ItemStyle-Width="60"
                        CancelImageUrl="~/images/Undo_grey_16x.png" DeleteImageUrl="~/images/administrator1_(delete)_16x16.gif"
                        EditImageUrl="~/images/Edit_grey_16x.png" ItemStyle-HorizontalAlign="Center"
                        UpdateImageUrl="~/images/Save_16x.png">
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="text-align:center; background-color: red; color: white; font-size: x-small; height: 20px; line-height: 20px;">
        <span style="font-weight: bold;">Warning: </span><span>Any change to the &quot;Normal&quot; priority must be coordinated with changes in the IIS admin console application settings or within the web.config file for this site!</span>
    </div>

    <asp:Panel id="pnlNewPriority" runat="server" CssClass="modalpopup"
        Height="150px" Width="426px" style="display: none;" DefaultButton="btnSavePriority">
        <div id="newPrioritytitle" class="popuptitle">New Priority</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:175px;" class="popuplabel">
                    <asp:Label id="lblPriority" runat="server" Text="Priority" AssociatedControlID="txtPriority" />
                </td>
                <td style="width:10px;"></td>
                <td style="width:215px;">
                    <asp:TextBox ID="txtPriority" runat="server" Width="195" ValidationGroup="newPriority" MaxLength="64" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvPriority" runat="server" ControlToValidate="txtPriority"
                        ErrorMessage="Priority" Display="None" ValidationGroup="newPriority" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnsavePriority" runat="server" Text="Save" CssClass="tradewebbutton" ValidationGroup="newPriority" />
                    &nbsp;&nbsp;
                    <asp:button ID="btncancelPriority" runat="server" Text="Cancel" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:SqlDataSource ID="Priorities_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        DeleteCommand="DELETE FROM [Priority_Types] WHERE [ID] = @ID"
        SelectCommand="SELECT * FROM [Priority_Types]"
        UpdateCommand="UPDATE [Priority_Types] SET [Priority] = @Priority WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Priority" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <cc1:ModalPopupExtender ID="mpeNewPriority" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewPriority"
        PopupControlID="pnlNewPriority"
        CancelControlID="btncancelPriority"
        DropShadow="true"
        PopupDragHandleControlID="newPrioritytitle" />

    <asp:ValidationSummary ID="vsPriorities" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newPriority" />
</asp:Content>

