﻿
Partial Class admin_ManageArchive
    Inherits System.Web.UI.Page
    Protected Sub imbhome_Click(sender As Object, e As ImageClickEventArgs) Handles imbhome.Click
        Response.Redirect("/", False)
    End Sub

    Protected Sub ArchivedProjects_Grid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles ArchivedProjects_Grid.RowCommand
        Dim ProjectID As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Restore" Then
            sql.ExecuteCommand(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "UPDATE Projects SET archive = 0 WHERE  ID = " & ProjectID)
            common.fLogActivity( _
                ProjectID, _
                Convert.ToInt32(ConfigurationManager.AppSettings("PROJECTOBJECTID")), _
                Convert.ToInt32(ConfigurationManager.AppSettings("RESTOREACTIONID")), _
                "" _
            )
            upArchivedProjects.Update()
            ArchivedProjects_Grid.DataBind()
        ElseIf e.CommandName = "viewproject" Then
            Response.Redirect("/ProjectDetails.aspx?ProjectID=" & ProjectID, False)
        End If
    End Sub

    Protected Sub ArchivedProjects_Grid_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles ArchivedProjects_Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim bragvalue As String = e.Row.Cells(3).Text.ToLower()
            Select Case bragvalue
                Case "green"
                    e.Row.Cells(3).CssClass = "bragTextGreen"
                Case "amber"
                    e.Row.Cells(3).CssClass = "bragTextAmber"
                Case "red"
                    e.Row.Cells(3).CssClass = "bragTextRed"
				Case "blue"
					e.Row.Cells(3).CssClass = "bragTextBlue"
            End Select
        End If
    End Sub
End Class
