﻿<%@ Page Language="VB" MasterPageFile="~/admin/admin.master" AutoEventWireup="false" CodeFile="Utilities.aspx.vb"
    Inherits="admin_Utilities" title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" style="width: 16px"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
    </div>
    <div class="subTitle">Utilities</div>

    <asp:UpdatePanel ID="upReassign" runat="server"><ContentTemplate>
    <div style="margin: 10px 10px 0px 10px; border: solid 1px #cbc19b; padding: 10px; border-radius: 5px;">
        <div style="text-align: center; font-size: medium; font-weight: bold; margin: 0px 0px 10px 0px;">
            Bulk reassign all projects, tasks and issues...
        </div>

        <div style="float:left; height: 40px; width: 50%; text-align: center;">
            <asp:Label ID="lblFromUser" runat="server" AssociatedControlID="ddlFromUser" Text="From:" />
            <asp:DropDownList ID="ddlFromUser" runat="server" ValidationGroup="reassign" /><br />
            <asp:RequiredFieldValidator ID="rfvFromUser" runat="server" CssClass="validation"
                ControlToValidate="ddlFromUser"
                ErrorMessage="Select a 'From' user."
                InitialValue="0"
                ValidationGroup="reassign" />
        </div>

        <div style="float:left; height: 40px; width: 50%; text-align: center;">
            <asp:Label ID="lblToUser" runat="server" AssociatedControlID="ddlToUser" Text="To:" />
            <asp:DropDownList ID="ddlToUser" runat="server" ValidationGroup="reassign" /><br />
            <asp:RequiredFieldValidator ID="rfvToUser" runat="server" CssClass="validation"
                ControlToValidate="ddlToUser"
                ErrorMessage="Select a 'To' user."
                InitialValue="0"
                ValidationGroup="reassign" />
        </div>

        <div style="clear: both; text-align: center;">
            <asp:CompareValidator id="cpvToUser" runat="server" CssClass="validation"
                ControlToValidate="ddlToUser" 
                ControlToCompare="ddlFromUser"
                Operator="NotEqual"
                Type="Integer"
                ValidationGroup="reassign" 
                ErrorMessage="Please select different 'From' and 'To' users." />
        </div>

        <div style="text-align: right;">
            <asp:Button id="btnReassign" runat="server" Text="OK" CssClass="tradewebbutton" ValidationGroup="reassign" />
        </div>
    </div>
    </ContentTemplate></asp:UpdatePanel>

    <asp:UpdatePanel ID="upSelectReassign" runat="server"><ContentTemplate>
    <div style="margin: 10px 10px 0px 10px; border: solid 1px #cbc19b; padding: 10px; border-radius: 5px;">
        <div style="text-align: center; font-size: medium; font-weight: bold; margin: 0px 0px 10px 0px;">
            Reassign select tasks...
        </div>

        <div style="float:left; height: 40px; width: 33%; text-align: center;">
            <asp:Label ID="lblFromSelectUser" runat="server" AssociatedControlID="ddlFromSelectUser" Text="From:" />
            <asp:DropDownList ID="ddlFromSelectUser" runat="server" CssClass="ddloverflow" AutoPostBack="true"
                ValidationGroup="moveselecttask" /><br />
            <asp:RequiredFieldValidator ID="rfvFromSelectUser" runat="server" CssClass="validation"
                ControlToValidate="ddlFromSelectUser"
                ErrorMessage="Select a 'From' User."
                InitialValue="0"
                ValidationGroup="moveselecttask" />
        </div>

        <div style="float:left; height: 40px; width: 33%; text-align: center;">
            <asp:Label ID="lblSelectTasks" runat="server" AssociatedControlID="ddlTask" Text="Task:" />
            <asp:DropDownList ID="ddlSelectTasks" runat="server" CssClass="ddloverflow" ValidationGroup="moveselecttask" /><br />
            <asp:RequiredFieldValidator ID="rfvSelectTasks" runat="server" CssClass="validation"
                ControlToValidate="ddlSelectTasks"
                ErrorMessage="Select a Task."
                InitialValue="0"
                ValidationGroup="moveselecttask" />
        </div>

         <div style="float:left; height: 40px; width: 33%; text-align: center;">
            <asp:Label ID="lblToSelectUser" runat="server" AssociatedControlID="ddlToSelectUser" Text="To:" />
            <asp:DropDownList ID="ddlToSelectUser" runat="server" CssClass="ddloverflow" ValidationGroup="moveselecttask" /><br />
            <asp:RequiredFieldValidator ID="rfvToSelectUser" runat="server" CssClass="validation"
                ControlToValidate="ddlToSelectUser"
                ErrorMessage="Select a 'To' Project."
                InitialValue="0"
                ValidationGroup="moveselecttask" />
        </div>

        <div style="clear: both; text-align: center;">
            <asp:CompareValidator id="cmpMoveSelectTask" runat="server" CssClass="validation"
                ControlToValidate="ddlFromSelectUser"
                ControlToCompare="ddlToSelectUser"
                Operator="NotEqual"
                Type="Integer"
                ValidationGroup="moveselecttask"
                ErrorMessage="Please select different 'From' and 'To' Users." />
        </div>

        <div style="text-align: right;">
            <asp:Button id="btnMoveSelectTask" runat="server" Text="OK" CssClass="tradewebbutton" ValidationGroup="moveselecttask" />
        </div>
    </div>
    </ContentTemplate></asp:UpdatePanel>

    <asp:UpdatePanel ID="upMoveTask" runat="server"><ContentTemplate>
    <div style="margin: 10px 10px 0px 10px; border: solid 1px #cbc19b; padding: 10px; border-radius: 5px;">
        <div style="text-align: center; font-size: medium; font-weight: bold; margin: 0px 0px 10px 0px;">
            Move Task to New Project...
        </div>

        <div style="float:left; height: 40px; width: 33%; text-align: center;">
            <asp:Label ID="lblOldProjectT" runat="server" AssociatedControlID="ddlOldProjectT" Text="From:" />
            <asp:DropDownList ID="ddlOldProjectT" runat="server" CssClass="ddloverflow" AutoPostBack="true" ValidationGroup="movetask" /><br />
            <asp:RequiredFieldValidator ID="rfvOldProjectT" runat="server" CssClass="validation"
                ControlToValidate="ddlOldProjectT"
                ErrorMessage="Select a 'From' Project."
                InitialValue="0"
                ValidationGroup="movetask" />
        </div>

        <div style="float:left; height: 40px; width: 33%; text-align: center;">
            <asp:Label ID="lblTask" runat="server" AssociatedControlID="ddlTask" Text="Task:" />
            <asp:DropDownList ID="ddlTask" runat="server" CssClass="ddloverflow" ValidationGroup="movetask" /><br />
            <asp:RequiredFieldValidator ID="rfvTask" runat="server" CssClass="validation"
                ControlToValidate="ddlTask"
                ErrorMessage="Select a Task."
                InitialValue="0"
                ValidationGroup="movetask" />
        </div>

         <div style="float:left; height: 40px; width: 33%; text-align: center;">
            <asp:Label ID="lblNewProjectT" runat="server" AssociatedControlID="ddlNewProjectT" Text="To:" />
            <asp:DropDownList ID="ddlNewProjectT" runat="server" CssClass="ddloverflow" ValidationGroup="movetask" /><br />
            <asp:RequiredFieldValidator ID="rfvNewProjectT" runat="server" CssClass="validation"
                ControlToValidate="ddlNewProjectT"
                ErrorMessage="Select a 'To' Project."
                InitialValue="0"
                ValidationGroup="movetask" />
        </div>

        <div style="clear: both; text-align: center;">
            <asp:CompareValidator id="cmpMoveTask" runat="server" CssClass="validation"
                ControlToValidate="ddlNewProjectT" 
                ControlToCompare="ddlOldProjectT"
                Operator="NotEqual"
                Type="Integer"
                ValidationGroup="movetask" 
                ErrorMessage="Please select different 'From' and 'To' Projects." />
        </div>

        <div style="text-align: right;">
            <asp:Button id="btnMoveTask" runat="server" Text="OK" CssClass="tradewebbutton" ValidationGroup="movetask" />
        </div>
    </div>
    </ContentTemplate></asp:UpdatePanel>

    <asp:UpdatePanel ID="upMoveIssue" runat="server"><ContentTemplate>
    <div style="margin: 10px 10px 0px 10px; border: solid 1px #cbc19b; padding: 10px; border-radius: 5px;">
        <div style="text-align: center; font-size: medium; font-weight: bold; margin: 0px 0px 10px 0px;">
            Move Issue to New Project...
        </div>

        <div style="float:left; height: 40px; width: 33%; text-align: center;">
            <asp:Label ID="lblOldProjectI" runat="server" AssociatedControlID="ddlOldProjectI" Text="From:" />
            <asp:DropDownList ID="ddlOldProjectI" runat="server" CssClass="ddloverflow" AutoPostBack="true" ValidationGroup="moveissue" /><br />
            <asp:RequiredFieldValidator ID="rfvMoveIssue" runat="server" CssClass="validation"
                ControlToValidate="ddlOldProjectI"
                ErrorMessage="Select a 'From' Project."
                InitialValue="0"
                ValidationGroup="moveissue" />
        </div>

        <div style="float:left; height: 40px; width: 33%; text-align: center;">
            <asp:Label ID="lblIssue" runat="server" AssociatedControlID="ddlIssue" Text="Issue:" />
            <asp:DropDownList ID="ddlIssue" runat="server" CssClass="ddloverflow" ValidationGroup="moveissue" /><br />
            <asp:RequiredFieldValidator ID="rfvIssue" runat="server" CssClass="validation"
                ControlToValidate="ddlIssue"
                ErrorMessage="Select an Issue."
                InitialValue="0"
                ValidationGroup="moveissue" />
        </div>

         <div style="float:left; height: 40px; width: 33%; text-align: center;">
            <asp:Label ID="lblNewProjectI" runat="server" AssociatedControlID="ddlNewProjectI" Text="To:" />
            <asp:DropDownList ID="ddlNewProjectI" runat="server" CssClass="ddloverflow" ValidationGroup="moveissue" /><br />
            <asp:RequiredFieldValidator ID="rfvNewProjectI" runat="server" CssClass="validation"
                ControlToValidate="ddlNewProjectI"
                ErrorMessage="Select a 'To' Project."
                InitialValue="0"
                ValidationGroup="moveissue" />
        </div>

        <div style="clear: both; text-align: center;">
            <asp:CompareValidator id="cmpMoveIssue" runat="server" CssClass="validation"
                ControlToValidate="ddlNewProjectI" 
                ControlToCompare="ddlOldProjectI"
                Operator="NotEqual"
                Type="Integer"
                ValidationGroup="moveissue" 
                ErrorMessage="Please select different 'From' and 'To' Projects." />
        </div>

        <div style="text-align: right;">
            <asp:Button id="btnMoveIssue" runat="server" Text="OK" CssClass="tradewebbutton" ValidationGroup="moveissue" />
        </div>
    </div>
    </ContentTemplate></asp:UpdatePanel>
</asp:Content>