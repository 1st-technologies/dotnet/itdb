﻿<%@ Page Title="" Language="VB" MasterPageFile="~/admin/admin.master" AutoEventWireup="false" CodeFile="ManageArchive.aspx.vb"
    Inherits="admin_ManageArchive" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <span class="menubarbutton">
            <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
            <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                ToolTip="Navigate Home" />
        </span>
    </div>
    <div class="subTitle">Archived Projects</div>
    <asp:UpdatePanel ID="upArchivedProjects" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="ArchivedProjects_Grid" runat="server" CssClass="gridView" AutoGenerateColumns="False"
                DataSourceID="ArchivedProjects_Data" EnableModelValidation="True" DataKeyNames="ID" AllowPaging="True"
                PageSize="20" PagerSettings-Mode="Numeric" EmptyDataText="There are no archived projects." >
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" ItemStyle-Width="60" ControlStyle-Width="80" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <div style="width:518px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;">
                                <asp:LinkButton ID="btnviewproject" runat="server"
                                    CommandName="viewproject"
                                    CommandArgument='<%#Bind("ID") %>'
                                    Text='<%#Bind("Project") %>'
                                    CausesValidation="false" CssClass="tradeweboverflow" />
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="full_name" HeaderText="Owner" SortExpression="full_name" ItemStyle-Width="160" />
                    <asp:BoundField DataField="Brag" HeaderText="Brag" SortExpression="Brag"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80" />
                    <asp:TemplateField ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center" Visible="true">
                        <ItemTemplate>
                            <asp:imageButton ID="imbRestoreProject" runat="server" ImageUrl="/images/trash_(restore)_16x16.gif"
                                CommandName="Restore" ImageAlign="Top"
                                CommandArgument='<%#Bind("ID") %>'
                                AlternateText="Restore"
                                CausesValidation="false" ToolTip="Restore"
                                OnClientClick="return confirm('Are you sure you want to restore this project?');" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:SqlDataSource ID="ArchivedProjects_Data" runat="server" ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM [ProjectSummary] WHERE [archive] = 1">
    </asp:SqlDataSource>
</asp:Content>

