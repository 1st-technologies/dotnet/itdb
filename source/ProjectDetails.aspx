﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ProjectDetails.aspx.vb"
    Inherits="Tasks" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">   
    <div style="float:left;">
        <div class="maintoolbar" style="padding: 3px 0px;">
            <span class="menubarbutton">
                <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                    ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
                <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                    ToolTip="Navigate Home" />
            </span>
		    <span class="menubarbutton">
			    <asp:ImageButton ID="imbMyView" runat="server" ImageAlign="absMiddle"
				    ImageUrl="~/images/TaskList_16x.png" AlternateText="View all your tasks and issues" ToolTip="View all your tasks and issues"
				    CausesValidation="false" />
			    <asp:Label ID="lblMyView" runat="server" Text="My Tasks" AssociatedControlID="imbMyView" CssClass="menutext"
                    ToolTip="View all your tasks and issues" />
		    </span>
            <span class="menubarbutton">
                <asp:ImageButton ID="imbnewtask" runat="server" ImageAlign="absMiddle"
                    ImageUrl="~/images/Task_16x.png" AlternateText="Create a new task" CausesValidation="false"/>
                <asp:Label ID="lblnewtask" runat="server" Text="New Task" AssociatedControlID="imbnewtask" CssClass="menutext"
                    ToolTip="Create a new task" />
            </span>
            <span class="menubarbutton">
                <asp:ImageButton ID="imbnewissue" runat="server" ImageAlign="absMiddle"
                    ImageUrl="~/images/StatusCriticalError_grey_16x.png" AlternateText="Create a new issue" CausesValidation="false"/>
                <asp:Label ID="lblnewissue" runat="server" Text="New Issue" AssociatedControlID="imbnewissue" CssClass="menutext"
                    ToolTip="Create a new issue" />
            </span>
            <span class="menubarbutton">
                <asp:ImageButton ID="imbnewprojectlog" runat="server" ImageAlign="absMiddle"
                    ImageUrl="~/images/Comment_16x.png" AlternateText="Create a new log entry" CausesValidation="false"/>
                <asp:Label ID="lblnewprojectlog" runat="server" Text="New Log" AssociatedControlID="imbnewprojectlog" CssClass="menutext"
                    ToolTip="Create a new log entry" />
            </span>
            <span class="menubarbutton">
                <asp:ImageButton ID="imbnewprojectattachment" runat="server" ImageAlign="absMiddle"
                    ImageUrl="~/images/Attach_16x.png" AlternateText="Upload an attachment" CausesValidation="false"/>
                <asp:Label ID="lblnewattachment" runat="server" Text="Upload Attachment" AssociatedControlID="imbnewprojectattachment"
                    ToolTip="Upload an attachment" CssClass="menutext" />
            </span>
        </div>
    </div>
    <div class="subTitle" style="clear: both;">Project Details</div>
    <asp:Label ID="archivewarning" runat="server" Visible="false" CssClass="warning">This Project has been Archived and cannot be edited!</asp:Label>
    <asp:DetailsView ID="DetailsView1" runat="server"
        DataKeyNames="ID"
        DataSourceID="Project_Data"
        AutoGenerateRows="False"
        AutoGenerateEditButton="True"
        CssClass="detailsView">
        <FieldHeaderStyle CssClass="detailsView_header" />
        <AlternatingRowStyle CssClass="gridView_text_alternate" />
        <Fields>
            <asp:TemplateField HeaderText="Project">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Project") %>' CssClass="gridView_TextBox" MaxLength="128" />
                </EditItemTemplate>
                <ItemTemplate>
                    <div style="width:820px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;">
                        <asp:Label ID="lblProject" runat="server" Text='<%# Bind("Project") %>' />
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Text='<%# Bind("Description") %>'
                        CssClass="gridView_TextBox" MaxLength="4000" />
                </EditItemTemplate>
                <ItemTemplate>
                    <div style="word-wrap: normal; overflow-y: auto;">
                        <%#Eval("Description")%>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Region">
                <EditItemTemplate>
                    <asp:DropDownList id="ddlEditRegion" runat="server"
                        CSSClass="gridView_TextBox"
                        DataTextField="Region"
                        DataValueField="ID"
                        DataSourceID="Region_Data"
                        SelectedValue='<%#Eval("Region_ID")%>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRegion" runat="server" Text='<%# Bind("Region")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Owner">
                <EditItemTemplate>
                    <asp:DropDownList id="ddlEditOwner" runat="server"
                        CSSClass="gridView_TextBox"
                        DataTextField="full_name"
                        DataValueField="ID"
                        DataSourceID="People_Data"
                        SelectedValue='<%#Eval("Person_ID")%>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblFullName" runat="server" Text='<%# Bind("full_name")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Category">
                <EditItemTemplate>
                    <asp:DropDownList id="ddlEditCategory" runat="server"
                        CSSClass="gridView_TextBox"
                        DataTextField="Category"
                        DataValueField="ID"
                        DataSourceID="Category_Data"
                        SelectedValue='<%#Eval("Category_ID")%>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCategoryDetails" runat="server" Text='<%# Bind("Category")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Platform">
                <EditItemTemplate>
                    <asp:DropDownList id="ddlEditPlatform" runat="server"
                        CSSClass="gridView_TextBox"
                        DataTextField="Platform"
                        DataValueField="ID"
                        DataSourceID="Platform_Data"
                        SelectedValue='<%#Eval("Platform_ID")%>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPlatform" runat="server" Text='<%# Bind("Platform")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="BAU Project">
                <ItemTemplate>
                    <asp:CheckBox ID="chkBaU" runat="server" Checked='<%# Bind("BaU")%>' Enabled="false" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="chkBaU" runat="server" Checked='<%# Bind("BaU")%>' Enabled="true" CSSClass="gridView_TextBox" />
                </EditItemTemplate>
            </asp:TemplateField>
        </Fields>
    </asp:DetailsView>
    <div style="line-height: 22px;">&nbsp;</div>
    <div class="subTitle">Tasks</div>

    <asp:UpdatePanel ID="upTasks" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="Tasks_Grid" runat="server"
                AutoGenerateColumns="False"
                DataKeyNames="ID"
                DataSourceID="Task_data"
                AllowPaging="True" CssClass="gridView"
                EmptyDataText="There are no tasks defined for this project." PagerSettings-Mode="Numeric" EnableModelValidation="True">
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <Columns>
                    <asp:TemplateField HeaderText="Task" SortExpression="Task">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnviewtask" runat="server"
                                CommandName="viewtask"
                                CommandArgument='<%#Bind("ID") %>'
                                Text='<%#Bind("Task")%>'
                                CausesValidation="false" CssClass="gridviewlinkbutton" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Assigned To" SortExpression="full_name" ItemStyle-Width="160">
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;width:100%">
								<%#Eval("full_name")%>
							</div>
						</ItemTemplate>
                        <ItemStyle Width="160px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Start_Date" HeaderText="Started" SortExpression="Start_Date"
                        ItemStyle-HorizontalAlign="Center" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="80" >
                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Due_Date" HeaderText="Due" SortExpression="Due_Date"
                        ItemStyle-HorizontalAlign="Center" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="80" >
                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Completed_Date" HeaderText="Completed" SortExpression="Completed_Date"
                        ItemStyle-HorizontalAlign="Center" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="80" >
                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Priority" HeaderText="Priority" SortExpression="Priority"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80" >
                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Percent" SortExpression="Percent_Complete"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80">
                        <ItemTemplate>
                            <%#Eval("Percent_Complete")%>%
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80" >
                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                    </asp:BoundField>
                    <asp:CheckBoxField DataField="achievement" HeaderText="A" SortExpression="achievement" ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center" >
                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                    </asp:CheckBoxField>
                    <asp:CommandField ButtonType="Image" DeleteImageUrl="~/images/delete_16.png"
                        ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center" ShowDeleteButton="True" >
                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div style="line-height: 22px;">&nbsp;</div>
    <div class="subTitle">Issues</div>

    <asp:UpdatePanel ID="upIssues" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="Issues_Grid" runat="server"
                AutoGenerateColumns="False"
                DataKeyNames="ID"
                DataSourceID="Issue_Data"
                AllowPaging="True" CssClass="gridView" EmptyDataText="There are no issues defined for this project." PagerSettings-Mode="Numeric" EnableModelValidation="True">
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <Columns>
                    <asp:TemplateField HeaderText="Issue" SortExpression="Issue">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnviewissue" runat="server"
                                CommandName="viewissue"
                                CommandArgument='<%#Bind("ID") %>'
                                Text='<%#Bind("Issue")%>'
                                CausesValidation="false" CssClass="gridviewlinkbutton" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Assigned To" SortExpression="full_name" ItemStyle-Width="160">
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;width:100%">
								<%#Eval("full_name")%>
							</div>
						</ItemTemplate>
                        <ItemStyle Width="160px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Priority" HeaderText="Priority" SortExpression="Priority" ItemStyle-Width="80"
                        ItemStyle-HorizontalAlign="Center" >
                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" ItemStyle-Width="80"
                        ItemStyle-HorizontalAlign="Center" >
                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                    </asp:BoundField>
                    <asp:CommandField ButtonType="Image" DeleteImageUrl="~/images/delete_16.png"
                        ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center" ShowDeleteButton="True" >
                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div style="line-height: 22px;">&nbsp;</div>
    <div class="subTitle">History</div>

    <asp:UpdatePanel ID="upProjectLog" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="ProjectLog_Grid" runat="server"
                AutoGenerateColumns="False"
                DataKeyNames="ID"
                DataSourceID="Project_History_Data"
                AllowPaging="true" CssClass="gridView" EmptyDataText="There are no log entries associated with this project."
                PageSize="3" PagerSettings-Mode="Numeric">
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <Columns>
                    <asp:BoundField DataField="Date" HeaderText="Date" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="190" />
                    <asp:TemplateField HeaderText="Entry">
                        <ItemTemplate>
                            <div style="width: 827px; max-height: 300px; word-wrap: normal; overflow: auto;">
								<%#Eval("log_entry")%>
							</div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div style="line-height: 22px;">&nbsp;</div>
    <div class="subTitle">Attachments</div>

    <asp:UpdatePanel ID="upAttachments" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="Project_Attachments_Grid" runat="server"
                AutoGenerateColumns="False"
                DataKeyNames="ID"
                DataSourceID="Project_Attachment_Data"
                AllowPaging="True" CssClass="gridView" EmptyDataText="There are no files attached to this project."
                AllowSorting="True" PagerSettings-Mode="Numeric" EnableModelValidation="True">
                <HeaderStyle CssClass="gridView_header" />
                <RowStyle CssClass="gridView_text" />
                <AlternatingRowStyle CssClass="gridView_text_alternate" />
                <Columns>
                    <asp:TemplateField HeaderText="File" SortExpression="File_Name" ItemStyle-Width="190">
                        <ItemTemplate>
                            <asp:LinkButton id="btndownload" runat="server" CommandName="downloadprojectattachment"
                                CommandArgument='<%#Bind("ID") %>' Text='<%#Bind("File_Name") %>'  />
                        </ItemTemplate>
                        <ItemStyle Width="190px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <div style="width: 100%; overflow: hidden; text-overflow: ellipsis;">
								<%#Eval("Description")%>
							</div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ButtonType="Image" DeleteImageUrl="~/images/delete_16.png" ShowDeleteButton="True"
                        ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center" >
                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel id="pnlNewTask" runat="server" CssClass="modalpopup"
        Height="465px" Width="426px" style="display: none;" DefaultButton="btnSaveTask">
        <div id="newtasktitle" class="popuptitle">New Task</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:175px;" class="popuplabel">
                    <asp:Label id="lbltaskname" runat="server" Text="Task" AssociatedControlID="txtTaskName" />
                </td>
                <td style="width:10px;"></td>
                <td style="width:215px;">
                    <asp:TextBox ID="txtTaskName" runat="server" Width="195" ValidationGroup="newtask" MaxLength="128" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvTaskName" runat="server" ControlToValidate="txtTaskName" ErrorMessage="Task"
                        Display="None" ValidationGroup="newtask" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblTaskDescription" runat="server" Text="Description" AssociatedControlID="txtTaskDescription" />
                </td>
                <td></td>
                <td>
                    <asp:TextBox ID="txtTaskDescription" runat="server" Width="195" TextMode="MultiLine" Rows="11"
                        ValidationGroup="newtask" MaxLength="4000" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="rfvTaskDescription" runat="server" ControlToValidate="txtTaskDescription"
                        ErrorMessage="Description" Display="None" ValidationGroup="newtask" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblAssignedTo" runat="server" Text="Assigned To" />
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlTaskAssignedTo" runat="server" Width="202" ValidationGroup="newtask" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlTaskAssignedTo" ErrorMessage="Assigned To"
                        InitialValue="<%$ AppSettings: TASKOWNERMENUDEFAULT %>" Display="None" ValidationGroup="newtask" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblPriority" runat="server" Text="Priority" AssociatedControlID="ddlTaskPriority" />
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlTaskPriority" runat="server" Width="202" ValidationGroup="newtask" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlTaskPriority" ErrorMessage="Priority"
                        InitialValue="<%$ AppSettings: PRIORITYMENUDEFAULT %>" Display="None" ValidationGroup="newtask" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblDueDate" runat="server" Text="Due Date" />
                </td>
                <td></td>
                <td>
                    <asp:TextBox runat="server" ID="txtDueDate" AutoCompleteType="Disabled" Width="202" />
                    <cc1:CalendarExtender ID="ceDueDate" runat="server" TargetControlID="txtDueDate" />
                </td>
                <td></td>
             </tr>
             <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblAccomplishment" runat="server" Text="Accomplishment" />
                </td>
                <td></td>
                <td>
                    <asp:CheckBox ID="chkAccomplishment" runat="server" CssClass="newitemcheckbox" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnSaveTask" runat="server" Text="Create" CssClass="tradewebbutton" ValidationGroup="newtask" />
                    &nbsp;&nbsp;
                    <asp:button ID="btnCancelTask" runat="server" Text="Close" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel id="pnlNewIssue" runat="server" CssClass="modalpopup"
        Height="377px" Width="426px" style="display: none;" DefaultButton="btnSaveIssue">
        <div id="newissuetitle" class="popuptitle">New Issue</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:175px;" class="popuplabel">
                    <asp:Label id="lblIssueName" runat="server" Text="Issue" AssociatedControlID="txtIssueName" />
                </td>
                <td style="width:10px;"></td>
                <td style="width:215px;">
                    <asp:TextBox ID="txtIssueName" runat="server" Width="195" ValidationGroup="newissue" MaxLength="128" />
                </td>
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtIssueName"
                        ErrorMessage="Issue" Display="None" ValidationGroup="newissue" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblIssueDescription" runat="server" Text="Description" AssociatedControlID="txtIssueDescription" />
                </td>
                <td></td>
                <td>
                    <asp:TextBox ID="txtIssueDescription" runat="server" Width="195" TextMode="MultiLine" Rows="11"
                        ValidationGroup="newissue" MaxLength="4000" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtIssueDescription"
                        ErrorMessage="Description" Display="None" ValidationGroup="newissue" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblIssueAssignedTo" runat="server" Text="Assigned To" />
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlIssueAssignedTo" runat="server" Width="202" ValidationGroup="newissue" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlIssueAssignedTo" ErrorMessage="Assigned To"
                        InitialValue="<%$ AppSettings: ISSUEOWNERMENUDEFAULT %>" Display="None" ValidationGroup="newissue" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="popuplabel">
                    <asp:Label id="lblIssuePriority" runat="server" Text="Priority" AssociatedControlID="ddlIssuePriority" />
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlIssuePriority" runat="server" Width="202" ValidationGroup="newissue" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlIssuePriority" ErrorMessage="Priority"
                        InitialValue="<%$ AppSettings: PRIORITYMENUDEFAULT %>" Display="None" ValidationGroup="newissue" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnSaveIssue" runat="server" Text="Create" CssClass="tradewebbutton" ValidationGroup="newissue" />
                    &nbsp;&nbsp;
                    <asp:button ID="btnCancelIssue" runat="server" Text="Close" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel id="pnlNewProjectLog" runat="server" CssClass="modalpopup"
        Height="418px" Width="760px" style="display: none;" DefaultButton="btnSaveProjectLog">
        <div id="newprojectlogtitle" class="popuptitle">New Log</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td>
                    <CKEditor:CKEditorControl ID="ceNewProjectLogEntry" runat="server"
                        Height="235" ResizeEnabled="false" BorderWidth="0"
                        Toolbar="Source|-|Cut|Copy|Paste|PasteText|PasteFromWord|-|Undo|Redo|-|Bold|Italic|Underline|Strike|Subscript|Superscript|-|NumberedList|BulletedList|-|Outdent|Indent|-|JustifyLeft|justifyCenter|JustifyRight|-|Link|Unlink
                        /
                    	Format|Font|FontSize" Skin="office2003"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align:center">
                    <asp:Button ID="btnSaveProjectLog" runat="server" Text="Save" CssClass="tradewebbutton" ValidationGroup="newprojectlog" />
                    &nbsp;&nbsp;
                    <asp:button ID="btnCancelProjectLog" runat="server" Text="Cancel" CausesValidation="false" CssClass="tradewebbutton" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel id="pnlNewProjectattachment" runat="server" CssClass="modalpopup"
        Height="363px" Width="426px" style="display: none;" DefaultButton="btnSaveNewProjectAttachment">
        <div id="newprojectattachemnttitle" class="popuptitle">New attachment</div>
        <table style="width: 100%; border-collapse: collapse; padding: 0px; margin: 0px;">
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:10px;"></td>
                <td style="width:400px; text-align: center;" class="popuplabel" colspan="3">
                    <asp:Label id="lblNewProjectAttachment" runat="server" Text="File" AssociatedControlID="fuProjectAttachment" />
                </td>
                    
                <td style="width:10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: center;">
                    <asp:FileUpload ID="fuProjectAttachment" runat="server"  Width="400px" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="fuProjectAttachment"
                        ErrorMessage="File" Display="None" ValidationGroup="newprojectattachment" />
                    <div style="text-align: center;">
                        <span class="maxsizenote">Maximum file size:</span>
                        <span><asp:label id="lblmaxwarning" runat="server" CssClass="maxsizenote" Text='<%$ AppSettings: MAXSIZESTRING %>' /></span>
                    </div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: center;" class="popuplabel" colspan="3">
                    <asp:Label id="lblProjectattachmentDescription" runat="server" Text="Description"
                        AssociatedControlID="txtProjectattachmentDescription" />
                </td>
                    
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:TextBox ID="txtProjectattachmentDescription" runat="server" Width="398" TextMode="MultiLine" Rows="11"
                        ValidationGroup="newprojectattachment" MaxLength="4000" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                        ControlToValidate="txtProjectattachmentDescription"
                        ErrorMessage="Description" Display="None" ValidationGroup="newprojectattachment" />&nbsp;
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:center" colspan="3">
                    <asp:Button ID="btnSaveNewProjectAttachment" runat="server" Text="Save" CssClass="tradewebbutton"
                        ValidationGroup="newprojectattachment" />&nbsp;&nbsp;
                    <asp:button ID="btnCancelNewProjectAttachment" runat="server" Text="Cancel" CausesValidation="false"
                        CssClass="tradewebbutton" />
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:SqlDataSource ID="Task_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM TaskSummary WHERE Project_ID = @ProjectID"
        DeleteCommand="DELETE FROM Task_History WHERE Task_ID = @ID; DELETE FROM Task_Attachments WHERE Task_ID = @ID; DELETE FROM Tasks WHERE ID = @ID;">
        <SelectParameters>
            <asp:QueryStringParameter Name="ProjectID" QueryStringField="ProjectID" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="int32" />
        </DeleteParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Issue_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM IssueSummary WHERE Project_ID = @ProjectID"
        DeleteCommand="DELETE FROM Issue_History WHERE Issue_ID = @ID; DELETE FROM Issues WHERE ID = @ID">
        <SelectParameters>
            <asp:QueryStringParameter Name="ProjectID" QueryStringField="ProjectID" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="int32" />
        </DeleteParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Project_Attachment_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Project_Attachments WHERE Project_ID = @ProjectID"
        DeleteCommand="DELETE FROM Project_Attachments WHERE ID = @ID">
        <SelectParameters>
            <asp:QueryStringParameter Name="ProjectID" QueryStringField="ProjectID" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="int32" />
        </DeleteParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Project_History_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Project_History WHERE Project_ID = @ProjectID ORDER BY [Date] DESC">
        <SelectParameters>
            <asp:QueryStringParameter Name="ProjectID" QueryStringField="ProjectID" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Project_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM ProjectDetails WHERE ID = @ProjectID"
        UpdateCommand="UPDATE Projects SET Project = @Project, Description = @Description, Person_ID = @PersonID, Platform_ID = @PlatformID, BaU = @BaU, Region_ID = @RegionID, Category_ID = @CategoryID WHERE ID = @ProjectID">
        <SelectParameters>
            <asp:QueryStringParameter Name="ProjectID" QueryStringField="ProjectID" />
        </SelectParameters>
        <UpdateParameters>
            <asp:QueryStringParameter Name="ProjectID" QueryStringField="ProjectID" />
            <asp:Parameter Name="Project" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:ControlParameter Name="PersonID" Type="Int32" ControlID="ctl00$ContentPlaceHolder1$DetailsView1$ddlEditOwner"
                PropertyName="SelectedValue" />
            <asp:ControlParameter Name="CategoryID" Type="Int32" ControlID="ctl00$ContentPlaceHolder1$DetailsView1$ddlEditCategory"
                PropertyName="SelectedValue" />
            <asp:ControlParameter Name="PlatformID" Type="Int32" ControlID="ctl00$ContentPlaceHolder1$DetailsView1$ddlEditPlatform"
                PropertyName="SelectedValue" />
            <asp:ControlParameter Name="RegionID" Type="Int32" ControlID="ctl00$ContentPlaceHolder1$DetailsView1$ddlEditRegion"
                PropertyName="SelectedValue" />
            <asp:Parameter Name="BaU" Type="Boolean" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Region_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Regions">
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="People_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM People WHERE Role_ID = @Role_ID ORDER BY full_name">
        <SelectParameters>
            <asp:Parameter Name="Role_ID" Type="Int32" DefaultValue="<%$ AppSettings:MANAGERROLEINDEX %>" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Platform_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Platforms">
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="Category_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Categories">
    </asp:SqlDataSource>

    <cc1:ModalPopupExtender ID="mpeNewTask" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewtask"
        PopupControlID="pnlNewtask"
        CancelControlID="btnCancelTask"
        DropShadow="true"
        PopupDragHandleControlID="newtasktitle" />

    <cc1:ModalPopupExtender ID="mpeNewIssue" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewissue"
        PopupControlID="pnlNewIssue"
        CancelControlID="btnCancelissue"
        DropShadow="true"
        PopupDragHandleControlID="newissuetitle" />

    <cc1:ModalPopupExtender ID="mpeNewProjectLog" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewprojectlog"
        PopupControlID="pnlNewProjectLog"
        CancelControlID="btnCancelProjectLog"
        DropShadow="true"
        PopupDragHandleControlID="newprojectlogtitle" />

    <cc1:ModalPopupExtender ID="mprNewProjectattachment" runat="server"
        BackgroundCssClass="modalbackground"
        TargetControlID="imbnewprojectattachment"
        PopupControlID="pnlNewProjectattachment"
        CancelControlID="btnCancelNewProjectAttachment"
        DropShadow="true"
        PopupDragHandleControlID="newprojectattachemnttitle" />

    <asp:ValidationSummary ID="vsTasks" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newtask" />

    <asp:ValidationSummary ID="vsIssues" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newissue" />

    <asp:ValidationSummary ID="vsProjectLog" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newprojectlog" />

    <asp:ValidationSummary ID="vsProjectAttachment" runat="server"
		ShowMessageBox="true"
		ShowSummary="false"
		HeaderText="You must enter a value in the following fields:"
		EnableClientScript="true" ValidationGroup="newprojectattachment" />
</asp:Content>

