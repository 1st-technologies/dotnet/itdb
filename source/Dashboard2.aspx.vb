﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Data

Partial Class Dashboard2
	Inherits System.Web.UI.Page

	Protected Sub imbhome_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbhome.Click
		Response.Redirect("/", False)
	End Sub

	Protected Sub imbMyView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbMyView.Click
        Dim intUserID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & Request.ServerVariables("LOGON_USER") & "'")
        Response.Redirect("/MyView.aspx?PersonID=" & intUserID, False)
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            With ddlCharts
                .DataSource = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT * FROM Charts ORDER BY Title")
                .DataTextField = "Title"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, "Select a Chart")
            End With
        End If
    End Sub

    Protected Sub ddlCharts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCharts.SelectedIndexChanged
        UpdateChart()
    End Sub


    Protected Sub ddlChartTypes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlChartTypes.SelectedIndexChanged
        UpdateChart()
    End Sub

    Protected Sub UpdateChart()
        Dim ds1 As New DataSet
        ds1 = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT * FROM Charts WHERE ID = " + ddlCharts.SelectedValue)
        If ds1.Tables(0).Rows.Count > 0 Then
            chart1_data.SelectCommand = ds1.Tables(0).Rows(0).Item(2).ToString

            Dim ser1 As Series = New Series
            ser1.ChartType = CInt(ddlChartTypes.SelectedValue)
            ser1.XValueMember = ds1.Tables(0).Rows(0).Item(3).ToString
            ser1.YValueMembers = ds1.Tables(0).Rows(0).Item(4).ToString
            ser1.CustomProperties = "DrawingStyle=Cylinder"
            ser1.ShadowOffset = 2
            Chart1.Series.Add(ser1)

            Dim ca1 As New ChartArea
            Dim xAxis As New Axis(ca1, AxisName.X)
            Dim yAxis As New Axis(ca1, AxisName.Y)
            ca1.AxisX = xAxis
            ca1.AxisY = yAxis
            Chart1.ChartAreas.Add(ca1)
        End If
    End Sub
End Class
