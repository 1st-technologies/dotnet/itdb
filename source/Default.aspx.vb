﻿Imports System.Data

Partial Class Projects
    Inherits System.Web.UI.Page

    Protected Sub Projects_Grid_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles Projects_Grid.RowCommand
		'ResetFilters()
        Dim ProjectID As Integer
        If e.CommandName = "viewproject" Then
            ProjectID = Convert.ToInt32(e.CommandArgument)
            Response.Redirect("ProjectDetails.aspx?ProjectID=" & ProjectID, False)
        ElseIf e.CommandName = "archive" Then
            ProjectID = Convert.ToInt32(e.CommandArgument)
            sql.ExecuteCommand(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "UPDATE Projects SET archive = 1 WHERE ID = " & ProjectID)

            common.fLogActivity( _
                ProjectID, _
                Convert.ToInt32(ConfigurationManager.AppSettings("PROJECTOBJECTID")), _
                Convert.ToInt32(ConfigurationManager.AppSettings("ARCHIVEACTIONID")), _
                "" _
            )

            upProjects.Update()
            Projects_Grid.DataBind()
        End If
    End Sub

    Protected Sub imbsettings_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbsettings.Click
        Response.Redirect("/admin", False)
    End Sub

    Protected Sub imbHelp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbHelp.Click
        Response.Redirect("/help", False)
    End Sub

    Protected Sub imbMyView_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbMyView.Click
        If Len(Request.ServerVariables("LOGON_USER")) > 0 Then
            Dim intUserID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & Request.ServerVariables("LOGON_USER") & "'")
            Response.Redirect("/MyView.aspx?PersonID=" & intUserID, False)
        End If
    End Sub

    Protected Sub imbReports_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbReports.Click
        Response.Redirect("/Reports.aspx", False)
    End Sub

    Protected Sub imbDashboard_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbDashboard.Click
        Response.Redirect("/Dashboard.aspx", False)
    End Sub

    Protected Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearch.Click
        'Response.Write("<div>" & imbSearch.ImageUrl & "</div>")
        If imbSearch.ImageUrl = "~/images/AddFilter_16x.png" Then
            upSearch.Visible = True
            imbSearch.ImageUrl = "~/images/DeleteFilter_16x.png"
            imbSearch.ToolTip = "Clear filter"
            imbSearch.AlternateText = "Clear filter"
        ElseIf imbSearch.ImageUrl = "~/images/DeleteFilter_16x.png" Then
            upSearch.Visible = False
            imbSearch.ImageUrl = "~/images/AddFilter_16x.png"
            imbSearch.ToolTip = "Apply filter"
            imbSearch.AlternateText = "Apply filter"
            txtSearchBox.Text = ""
            Projects_Grid.DataBind()
        End If
    End Sub

    Protected Sub Projects_Grid_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles Projects_Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim bragvalue As String = e.Row.Cells(7).Text.ToLower()
            Select Case bragvalue
                Case "green"
                    e.Row.Cells(7).CssClass = "bragTextGreen"
                Case "amber"
                    e.Row.Cells(7).CssClass = "bragTextAmber"
                Case "red"
                    e.Row.Cells(7).CssClass = "bragTextRed"
                Case "blue"
                    e.Row.Cells(7).CssClass = "bragTextBlue"
            End Select
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ' Populate dropdownlists 
        If Not Me.Page.IsPostBack Then

            With ddlOwners
                .DataSource = common.fGetManagersEnabled()
                .DataTextField = "full_name"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, ConfigurationManager.AppSettings("PROJECTOWNERMENUDEFAULT"))
            End With

            With ddlOwnerFilter
                .DataSource = common.fGetManagers()
                .DataTextField = "full_name"
                .DataValueField = "full_name"
                .DataBind()
                .Items.Insert(0, New ListItem("All", "%"))
            End With

            With ddlPlatforms
                .DataSource = common.fGetPlatforms()
                .DataTextField = "Platform"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, ConfigurationManager.AppSettings("PLATFORMMENUDEFAULT"))
            End With

            With ddlPlatformsFilter
                .DataSource = common.fGetPlatforms()
                .DataTextField = "Platform"
                .DataValueField = "Platform"
                .DataBind()
                .Items.Insert(0, New ListItem("All", "%"))
            End With

            With ddlCategories
                .DataSource = common.fGetCategories()
                .DataTextField = "Category"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, ConfigurationManager.AppSettings("CATEGORYMENUDEFAULT"))
            End With

            With ddlCategoriesFilter
                .DataSource = common.fGetCategories()
                .DataTextField = "Category"
                .DataValueField = "Category"
                .DataBind()
                .Items.Insert(0, New ListItem("All", "%"))
            End With

            With ddlRegions
                .DataSource = common.fGetRegions()
                .DataTextField = "Region"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, ConfigurationManager.AppSettings("REGIONMENUDEFAULT"))
            End With

            With ddlRegionsFilter
                .DataSource = common.fGetRegions()
                .DataTextField = "Region"
                .DataValueField = "Region"
                .DataBind()
                .Items.Insert(0, New ListItem("All", "%"))
            End With

            If Not Request.Cookies("PAGESIZE") Is Nothing Then
                Projects_Grid.PageSize = Convert.ToInt32(Request.Cookies("PAGESIZE").Value)
                ddlPageSize.SelectedValue = Request.Cookies("PAGESIZE").Value
            End If

            If Not Request.Cookies("OWNERFILTER") Is Nothing Then
                ddlOwnerFilter.SelectedValue = Request.Cookies("OWNERFILTER").Value
            End If

            If Not Request.Cookies("BRAGFILTER") Is Nothing Then
                ddlBragFilter.SelectedValue = Request.Cookies("BRAGFILTER").Value
            End If

            If Not Request.Cookies("PLATFORMSFILTER") Is Nothing Then
                ddlPlatformsFilter.SelectedValue = Request.Cookies("PLATFORMSFILTER").Value
            End If

            If Not Request.Cookies("REGIONSFILTER") Is Nothing Then
                ddlPlatformsFilter.SelectedValue = Request.Cookies("REGIONSFILTER").Value
            End If

            If Not Request.Cookies("SHOWCOMPLETED") Is Nothing Then
                chkShowComplete.Checked = Request.Cookies("SHOWCOMPLETED").Value
            End If

            Dim intUserID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT ID FROM People WHERE Login = '" & Request.ServerVariables("LOGON_USER") & "'")
            Dim intRoleID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT r.ID FROM Roles r INNER JOIN People p ON r.ID = p.Role_ID WHERE p.ID = '" & intUserID & "'")

            If intRoleID = ConfigurationManager.AppSettings("MANAGERROLEINDEX").ToString Then
                Projects_Grid.Columns(5).Visible = True
            Else
                Projects_Grid.Columns(5).Visible = False
            End If

        End If
    End Sub

    Protected Sub btnSaveProject_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveProject.Click
        If Not Page.IsValid Then Exit Sub

        Dim intID As Int32 = sql.ExecuteScalar(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, _
            "INSERT INTO Projects (Project, Description, Person_ID, Platform_ID, Category_ID, BaU, Region_ID) VALUES ('" & txtProjectName.Text.Replace("'", "''").Trim() & "', '" & txtProjectDescription.Text.Replace("'", "''").Trim() & "', '" & ddlOwners.SelectedItem.Value & "', '" & ddlPlatforms.SelectedItem.Value & "', '" & ddlCategories.SelectedItem.Value & "', '" & chkBaU.Checked & "', '" & ddlRegions.SelectedItem.Value & "'); SELECT SCOPE_IDENTITY();")

        common.fLogActivity( _
            intID, _
            Convert.ToInt32(ConfigurationManager.AppSettings("PROJECTOBJECTID")), _
            Convert.ToInt32(ConfigurationManager.AppSettings("INSERTACTIONID")), _
            txtProjectName.Text.Replace("'", "''").Trim() _
        )

        upProjects.Update()
        Projects_Grid.DataBind()
        txtProjectName.Text = ""
        txtProjectDescription.Text = ""
        ddlOwners.SelectedIndex = 0
        ddlPlatforms.SelectedIndex = 0
    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlPageSize.SelectedIndexChanged
        Response.Cookies("PAGESIZE").Value = ddlPageSize.SelectedValue
        Response.Cookies("PAGESIZE").Expires = DateTime.Now.AddMonths(12)
        Projects_Grid.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue)
        Projects_Grid.DataBind()
    End Sub

    Protected Sub imbexport_Click(sender As Object, e As ImageClickEventArgs) Handles imbexport.Click
        Dim sw As New IO.StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        Dim dg As New DataGrid

        dg.DataSource = sql.GetDataSet(ConfigurationManager.ConnectionStrings("ITDBConnectionString").ConnectionString, "SELECT * FROM ProjectSummary WHERE archive = 0 AND Brag LIKE '" & ddlBragFilter.SelectedValue & "' AND full_name LIKE '" & ddlOwnerFilter.SelectedValue & "' AND Platform LIKE '" & ddlPlatformsFilter.SelectedValue & "' AND Region LIKE '" & ddlRegionsFilter.SelectedValue & "' AND Category LIKE '" & ddlCategoriesFilter.SelectedValue & "'")
        dg.DataBind()

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=Projects.xls")
        Response.Charset = ""
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/ms-excel"
        dg.RenderControl(hw)
        Response.Write(sw.ToString())
        Response.End()

        dg.Dispose()
    End Sub

    Protected Sub ResetFilters()
        ddlOwnerFilter.SelectedIndex = 0
        ddlBragFilter.SelectedIndex = 0
        ddlPlatformsFilter.SelectedIndex = 0
    End Sub

    Protected Sub btnHidden1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHidden1.Click
        upSearch.Visible = False
        Projects_Data.SelectCommand = "SELECT DISTINCT ps.* FROM ProjectSummary ps INNER JOIN Projects p ON ps.ID = p.ID LEFT OUTER JOIN Tasks t ON ps.ID = t.Project_ID LEFT OUTER JOIN Issues i ON ps.ID = i.Project_ID WHERE ps.archive = 0 AND (p.Project LIKE '%" & txtSearchBox.Text & "%' OR p.Description LIKE '%" & txtSearchBox.Text & "%' OR t.Task LIKE '%" & txtSearchBox.Text & "%' OR t.Description LIKE '%" & txtSearchBox.Text & "%' OR i.Issue LIKE '%" & txtSearchBox.Text & "%' OR i.Description LIKE '%" & txtSearchBox.Text & "%') ORDER BY ID"
    End Sub

	Protected Sub ddlOwnerFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOwnerFilter.SelectedIndexChanged
		Projects_Grid.PageIndex = 0
		Response.Cookies("OWNERFILTER").Value = ddlOwnerFilter.SelectedValue
		Response.Cookies("OWNERFILTER").Expires = DateTime.Now.AddMonths(12)
	End Sub

	Protected Sub ddlPlatformsFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPlatformsFilter.SelectedIndexChanged
		Projects_Grid.PageIndex = 0
        Response.Cookies("PLATFORMSFILTER").Value = ddlPlatformsFilter.SelectedValue
		Response.Cookies("PLATFORMSFILTER").Expires = DateTime.Now.AddMonths(12)
	End Sub

    Protected Sub ddlCategoriesFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategoriesFilter.SelectedIndexChanged
        Projects_Grid.PageIndex = 0
        Response.Cookies("CATEGORIESFILTER").Value = ddlCategoriesFilter.SelectedValue
        Response.Cookies("CATEGORIESFILTER").Expires = DateTime.Now.AddMonths(12)
    End Sub

	Protected Sub ddlBragFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBragFilter.SelectedIndexChanged
		Projects_Grid.PageIndex = 0
		Response.Cookies("BRAGFILTER").Value = ddlBragFilter.SelectedValue
		Response.Cookies("BRAGFILTER").Expires = DateTime.Now.AddMonths(12)
	End Sub

	Protected Sub chkShowComplete_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowComplete.CheckedChanged
		Projects_Grid.PageIndex = 0
		Response.Cookies("SHOWCOMPLETED").Value = chkShowComplete.Checked
		Response.Cookies("SHOWCOMPLETED").Expires = DateTime.Now.AddMonths(12)
	End Sub

	Protected Sub ddlRegionsFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRegionsFilter.SelectedIndexChanged
		Projects_Grid.PageIndex = 0
		Response.Cookies("REGIONSFILTER").Value = ddlRegionsFilter.SelectedValue
		Response.Cookies("REGIONSFILTER").Expires = DateTime.Now.AddMonths(12)
	End Sub
End Class
