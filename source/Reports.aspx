﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Reports.aspx.vb" Inherits="Reports" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="maintoolbar">
        <div style="float:left;">
            <span class="menubarbutton">
                <asp:ImageButton ID="imbhome" runat="server" ImageAlign="absMiddle"
                    ImageUrl="~/images/Home_16x.png" AlternateText="Navigate Home" CausesValidation="false"/>
                <asp:Label ID="lblHome" runat="server" Text="Home" AssociatedControlID="imbhome" CssClass="menutext"
                    ToolTip="Navigate Home" />
            </span>
		    <span class="menubarbutton">
			    <asp:ImageButton ID="imbexport" runat="server" ImageAlign="absMiddle" Enabled="true"
				    ImageUrl="~/images/ExcelWorksheetView_16x.png" AlternateText="Export to Excel" ToolTip="Export to Excel"
                    CausesValidation="false" />
			    <asp:Label ID="lblsettings" runat="server" Text="Export" AssociatedControlID="imbexport"  CssClass="menutext"
                    ToolTip="Export to Excel" />
		    </span>
			<span class="menubarbutton">
				<asp:ImageButton ID="imbMyView" runat="server" ImageAlign="absMiddle"
					ImageUrl="~/images/TaskList_16x.png" AlternateText="View all your tasks and issues" ToolTip="View all your tasks and issues"
					CausesValidation="false" />
				<asp:Label ID="lblMyView" runat="server" Text="My Tasks" AssociatedControlID="imbMyView" CssClass="menutext"
					ToolTip="View all your tasks and issues" />
			</span>
        </div>
    </div>
    <div class="subTitle" style="clear: both;">Reports</div>
    <div style="float:left; height: 100%; width: 250px; overflow: auto; text-wrap:none; min-height: 200px;">
        <asp:TreeView ID="tvReports" runat="server" ExpandDepth="0" NodeWrap="false" />
    </div>
    <div style="float: left; width: 1348px; overflow: auto; text-wrap: none; margin: 0px 2px 0px 0px;">
        <asp:UpdatePanel ID="upReport" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel id="pnlReport" runat="server">
                    <asp:GridView ID="Reports_Grid" runat="server" HeaderStyle-Wrap="true"
                        DataSourceID="Reports_Data" Caption="No Data" AllowSorting="false"
                        AllowPaging="false" CssClass="gridView" EmptyDataText="No Report Selected"
                        PageSize="20" PagerSettings-Mode="Numeric">
                        <HeaderStyle CssClass="gridView_header" />
                        <RowStyle CssClass="gridView_text" />
                        <AlternatingRowStyle CssClass="gridView_text_alternate" />
                    </asp:GridView>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="clear: both;">
    </div>

    <asp:SqlDataSource ID="Reports_Data" runat="server"
        ConnectionString="<%$ ConnectionStrings:ITDBConnectionString %>"
        SelectCommand="SELECT * FROM Projects WHERE ID = 0">
    </asp:SqlDataSource>
</asp:Content>

